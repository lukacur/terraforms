DROP TABLE IF EXISTS LoginProvider CASCADE;
DROP TABLE IF EXISTS Status CASCADE;
DROP TABLE IF EXISTS AppUser CASCADE;
DROP TABLE IF EXISTS RestrictionType CASCADE;
DROP TABLE IF EXISTS AccessType CASCADE;
DROP TABLE IF EXISTS AnswerType CASCADE;
DROP TABLE IF EXISTS Form CASCADE;
DROP TABLE IF EXISTS Question CASCADE;
DROP TABLE IF EXISTS FormChange CASCADE;
DROP TABLE IF EXISTS AccessRequest CASCADE;
DROP TABLE IF EXISTS Answer CASCADE;
DROP TABLE IF EXISTS QuestionChange CASCADE;
DROP TABLE IF EXISTS PossibleAnswer CASCADE;

CREATE TABLE LoginProvider
(
  providerId INT NOT NULL,
  providerName VARCHAR(64) NOT NULL,
  PRIMARY KEY (providerId),
  CONSTRAINT chk_default_entries_only CHECK (providerId IN (0, 1))
);

CREATE TABLE Status
(
  statusId INT NOT NULL,
  statusName VARCHAR(64) NOT NULL,
  PRIMARY KEY (statusId),
  CONSTRAINT chk_default_entries_only CHECK (statusId IN (0, 1, 2, 3, 4))
);

CREATE TABLE AppUser
(
  UID BIGSERIAL NOT NULL,
  username VARCHAR(64) NOT NULL,
  password VARCHAR(512),
  email VARCHAR(320) NOT NULL,
  tsCreated TIMESTAMP NOT NULL,
  loginProvider INT NOT NULL DEFAULT 0,
  status INT NOT NULL DEFAULT 0, /* 1 for default disable */
  expiryDate DATE,
  tsModified TIMESTAMP,
  PRIMARY KEY (UID),
  FOREIGN KEY (loginProvider) REFERENCES LoginProvider(providerId),
  FOREIGN KEY (status) REFERENCES Status(statusId),
  UNIQUE (username),
  UNIQUE (email)
);

CREATE TABLE RestrictionType
(
  rtypeId INT NOT NULL,
  restrictionName VARCHAR(64) NOT NULL,
  PRIMARY KEY (rtypeId),
  CONSTRAINT chk_default_entries_only CHECK (rtypeId IN (0, 1, 2))
);

CREATE TABLE AccessType
(
  accTypeId INT NOT NULL,
  typeName VARCHAR(64) NOT NULL,
  PRIMARY KEY (accTypeId),
  CONSTRAINT chk_default_entries_only CHECK (accTypeId IN (0, 1, 2))
);

CREATE TABLE AnswerType
(
  answTypeId INT NOT NULL,
  answerTypeName VARCHAR(64) NOT NULL,
  PRIMARY KEY (answTypeId),
  CONSTRAINT chk_default_entries_only CHECK (answTypeId IN (0, 1, 2, 3, 4, 5, 6, 7))
);

CREATE TABLE Form
(
  formID BIGSERIAL NOT NULL,
  title VARCHAR(128) NOT NULL,
  tsCreated TIMESTAMP NOT NULL,
  description VARCHAR(512) NOT NULL,
  viewLink VARCHAR(256) NOT NULL,
  statsLink VARCHAR(256) NOT NULL,
  tsModified TIMESTAMP,
  owner BIGINT NOT NULL,
  restrictedTo INT NOT NULL DEFAULT 0,
  userModified BIGINT,
  PRIMARY KEY (formID),
  UNIQUE (viewLink),
  UNIQUE (statsLink),
  FOREIGN KEY (owner) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (restrictedTo) REFERENCES RestrictionType(rtypeId),
  FOREIGN KEY (userModified) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE Question
(
  questionID BIGSERIAL NOT NULL,
  questionNumber INT NOT NULL,
  page INT NOT NULL,
  required BOOLEAN NOT NULL,
  tsCreated TIMESTAMP NOT NULL,
  tsModified TIMESTAMP,
  question VARCHAR(256) NOT NULL,
  formID BIGINT NOT NULL,
  userCreated BIGINT NOT NULL,
  userModified BIGINT,
  answTypeId INT NOT NULL,
  previousVersion BIGINT,
  PRIMARY KEY (questionID),
  FOREIGN KEY (formID) REFERENCES Form(formID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (userCreated) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (userModified) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (answTypeId) REFERENCES AnswerType(answTypeId),
  FOREIGN KEY (previousVersion) REFERENCES Question(questionID)
      ON DELETE SET NULL
      ON UPDATE CASCADE
);

CREATE TABLE FormChange
(
  changeID BIGSERIAL NOT NULL,
  oldTitle VARCHAR(128) NOT NULL,
  newTitle VARCHAR(128) NOT NULL,
  oldDescription VARCHAR(512) NOT NULL,
  newDescription VARCHAR(512) NOT NULL,
  oldRestrictedTo INT NOT NULL,
  newRestrictedTo INT NOT NULL,
  tsChange TIMESTAMP NOT NULL,
  userResponsible BIGINT NOT NULL,
  formChanged BIGINT NOT NULL,
  PRIMARY KEY (changeID),
  FOREIGN KEY (userResponsible) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (formChanged) REFERENCES Form(formID)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE AccessRequest
(
  entryId BIGSERIAL NOT NULL,
  grantedOn TIMESTAMP,
  expiresOn DATE,
  requestedOn TIMESTAMP NOT NULL,
  userRequested BIGINT NOT NULL,
  formID BIGINT NOT NULL,
  accessType INT NOT NULL,
  PRIMARY KEY (entryId),
  FOREIGN KEY (userRequested) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (formID) REFERENCES Form(formID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (accessType) REFERENCES AccessType(accTypeId)
);

CREATE TABLE Answer
(
  answerID BIGSERIAL NOT NULL,
  value TEXT NOT NULL,
  tsCreated TIMESTAMP NOT NULL,
  tsModified TIMESTAMP,
  userCreated BIGINT,
  userModified BIGINT,
  questionID BIGINT NOT NULL,
  PRIMARY KEY (answerID),
  FOREIGN KEY (userCreated) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (userModified) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (questionID) REFERENCES Question(questionID)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE QuestionChange
(
  changeID BIGSERIAL NOT NULL,
  oldQuestionNumber INT NOT NULL,
  newQuestionNumber INT NOT NULL,
  oldPage INT NOT NULL,
  newPage INT NOT NULL,
  oldRequired BOOLEAN NOT NULL,
  newRequired BOOLEAN NOT NULL,
  tsChange TIMESTAMP NOT NULL,
  oldQuestion VARCHAR(256) NOT NULL,
  newQuestion VARCHAR(256) NOT NULL,
  oldAnswerType INT NOT NULL,
  newAnswerType INT NOT NULL,
  questionChanged BIGINT NOT NULL,
  userResponsible BIGINT NOT NULL,
  PRIMARY KEY (changeID),
  FOREIGN KEY (questionChanged) REFERENCES Question(questionID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (userResponsible) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE PossibleAnswer
(
  possibleAnswerId BIGSERIAL NOT NULL,
  name VARCHAR(128) NOT NULL,
  value VARCHAR(256) NOT NULL,
  tsCreated TIMESTAMP NOT NULL,
  tsModified TIMESTAMP,
  userModified BIGINT,
  userCreated BIGINT NOT NULL,
  questionID BIGINT NOT NULL,
  PRIMARY KEY (possibleAnswerId),
  FOREIGN KEY (userModified) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (userCreated) REFERENCES AppUser(UID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY (questionID) REFERENCES Question(questionID)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  UNIQUE (value, questionID)
);

/* --------------------------------- DEFAULT TABLE VALUES --------------------------------- */
INSERT INTO LoginProvider (providerId, providerName) VALUES
    (0, 'LOCAL'),
    (1, 'GOOGLE');

INSERT INTO Status (statusId, statusName) VALUES
    (0, 'ENABLED'),
    (1, 'ACTIVATION_PENDING'),
    (2, 'DISABLED'),
    (3, 'DELETION_PENDING'),
    (4, 'DELETED');

INSERT INTO RestrictionType (rtypeId, restrictionName) VALUES
    (0, 'NONE'),
    (1, 'LOGGED_IN'),
    (2, 'SELECTED');

INSERT INTO AccessType (accTypeId, typeName) VALUES
    (0, 'VIEW'),
    (1, 'EDIT'),
    (2, 'STAT');

INSERT INTO AnswerType (answTypeId, answerTypeName) VALUES
    (0, 'SHORTTEXT'),
    (1, 'LONGTEXT'),
    (2, 'NUMBER'),
    (3, 'SCALE'),
    (4, 'RADIO'),
    (5, 'CHECKBOX'),
    (6, 'TRUEFALSE'),
    (7, 'SCALEPERCENT');

/* -------------------------------- TRIGGERS AND FUNCTIONS -------------------------------- */
/* Before insert triggers and functions */
CREATE OR REPLACE FUNCTION tgf_set_created_timestamp() RETURNS trigger AS
$$
BEGIN
    NEW.tsCreated = NOW()::TIMESTAMP(0);
    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_set_created_timestamp() RETURNS trigger AS
$$
BEGIN
    NEW.tsCreated = NOW()::TIMESTAMP(0);
    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_insert_accrequest() RETURNS trigger AS
$$
BEGIN
    NEW.requestedOn = NOW()::TIMESTAMP(0);
    NEW.expiresOn = CURRENT_DATE + '7 days'::INTERVAL;
    RETURN NEW;
END
$$ language plpgsql;

DROP TRIGGER IF EXISTS tg_appuser_bi ON AppUser CASCADE;
DROP TRIGGER IF EXISTS tg_form_bi ON Form CASCADE;
DROP TRIGGER IF EXISTS tg_answer_bi ON Answer CASCADE;
DROP TRIGGER IF EXISTS tg_question_bi ON Question CASCADE;
DROP TRIGGER IF EXISTS tg_possibleanswer_bi ON PossibleAnswer CASCADE;
DROP TRIGGER IF EXISTS tg_accessrequest_bi ON AccessRequest CASCADE;

CREATE TRIGGER tg_appuser_bi
    BEFORE INSERT ON AppUser
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_form_bi
    BEFORE INSERT ON Form
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_answer_bi
    BEFORE INSERT ON Answer
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_question_bi
    BEFORE INSERT ON Question
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_possibleanswer_bi
    BEFORE INSERT ON PossibleAnswer
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_accessrequest_bi
    BEFORE INSERT ON AccessRequest
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_insert_accrequest();

/* Before update triggers and functions */
CREATE OR REPLACE FUNCTION tgf_set_modified_timestamp() RETURNS trigger AS
$$
BEGIN
    NEW.tsModified = NOW()::TIMESTAMP(0);
    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_update_form_entry() RETURNS trigger AS
$$
DECLARE callTs CONSTANT TIMESTAMP(0) := NOW()::TIMESTAMP(0);
BEGIN
    INSERT INTO FormChange (formChanged, userResponsible, oldTitle, newTitle, oldDescription, newDescription, oldRestrictedTo, newRestrictedTo, tsChange)
        VALUES (NEW.formID, NEW.userModified, OLD.title, NEW.title, OLD.description, NEW.description, OLD.restrictedTo, NEW.restrictedTo, callTs);
    
    NEW.tsModified = callTs;

    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_update_question_entry() RETURNS trigger AS
$$
DECLARE callTs CONSTANT TIMESTAMP(0) := NOW()::TIMESTAMP(0);
BEGIN
    INSERT INTO QuestionChange (userResponsible, questionChanged, oldQuestionNumber, newQuestionNumber, oldPage, newPage, oldRequired, newRequired, oldQuestion, newQuestion, oldAnswerType, newAnswerType, tsChange)
        VALUES (NEW.userModified, NEW.questionID, OLD.questionNumber, NEW.questionNumber, OLD.page, NEW.page, OLD.required, NEW.required, OLD.question, NEW.question, OLD.answTypeId, NEW.answTypeId, callTs);

    NEW.tsModified = callTs;

    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_update_accrequest() RETURNS trigger AS
$$
DECLARE callTs CONSTANT TIMESTAMP(0) := NOW()::TIMESTAMP(0);
BEGIN
    IF (NEW.grantedOn IS NOT NULL) THEN
        NEW.expiresOn = NULL;
    END IF;
    RETURN NEW;
END
$$ language plpgsql;

DROP TRIGGER IF EXISTS tg_form_bu ON Form CASCADE;
DROP TRIGGER IF EXISTS tg_question_bu ON Question CASCADE;
DROP TRIGGER IF EXISTS tg_accessrequest_bu ON AccessRequest CASCADE;

CREATE TRIGGER tg_form_bu
    BEFORE UPDATE ON Form
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_update_form_entry();

CREATE TRIGGER tg_question_bu
    BEFORE UPDATE ON Question
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_update_question_entry();

CREATE TRIGGER tg_accessrequest_bu
    BEFORE UPDATE ON AccessRequest
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_update_accrequest();

/* After insert triggers and functions */

/* After update triggers and functions */