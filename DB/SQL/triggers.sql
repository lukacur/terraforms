/* Before insert triggers and functions */
CREATE OR REPLACE FUNCTION tgf_set_created_timestamp() RETURNS trigger AS
$$
BEGIN
    NEW.tsCreated = NOW()::TIMESTAMP(0);
    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_set_created_timestamp() RETURNS trigger AS
$$
BEGIN
    NEW.tsCreated = NOW()::TIMESTAMP(0);
    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_insert_accrequest() RETURNS trigger AS
$$
BEGIN
    NEW.requestedOn = NOW()::TIMESTAMP(0);
    NEW.expiresOn = CURRENT_DATE + '7 days'::INTERVAL;
    RETURN NEW;
END
$$ language plpgsql;

DROP TRIGGER IF EXISTS tg_appuser_bi ON AppUser CASCADE;
DROP TRIGGER IF EXISTS tg_form_bi ON Form CASCADE;
DROP TRIGGER IF EXISTS tg_answer_bi ON Answer CASCADE;
DROP TRIGGER IF EXISTS tg_question_bi ON Question CASCADE;
DROP TRIGGER IF EXISTS tg_possibleanswer_bi ON PossibleAnswer CASCADE;
DROP TRIGGER IF EXISTS tg_accessrequest_bi ON AccessRequest CASCADE;

CREATE TRIGGER tg_appuser_bi
    BEFORE INSERT ON AppUser
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_form_bi
    BEFORE INSERT ON Form
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_answer_bi
    BEFORE INSERT ON Answer
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_question_bi
    BEFORE INSERT ON Question
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_possibleanswer_bi
    BEFORE INSERT ON PossibleAnswer
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_set_created_timestamp();

CREATE TRIGGER tg_accessrequest_bi
    BEFORE INSERT ON AccessRequest
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_insert_accrequest();

/* Before update triggers and functions */
CREATE OR REPLACE FUNCTION tgf_set_modified_timestamp() RETURNS trigger AS
$$
BEGIN
    NEW.tsModified = NOW()::TIMESTAMP(0);
    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_update_form_entry() RETURNS trigger AS
$$
DECLARE callTs CONSTANT TIMESTAMP(0) := NOW()::TIMESTAMP(0);
BEGIN
    INSERT INTO FormChange (formChanged, userResponsible, oldTitle, newTitle, oldDescription, newDescription, oldRestrictedTo, newRestrictedTo, tsChange)
        VALUES (NEW.formID, NEW.userModified, OLD.title, NEW.title, OLD.description, NEW.description, OLD.restrictedTo, NEW.restrictedTo, callTs);
    
    NEW.tsModified = callTs;

    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_update_question_entry() RETURNS trigger AS
$$
DECLARE callTs CONSTANT TIMESTAMP(0) := NOW()::TIMESTAMP(0);
BEGIN
    INSERT INTO QuestionChange (userResponsible, questionChanged, oldQuestionNumber, newQuestionNumber, oldPage, newPage, oldRequired, newRequired, oldQuestion, newQuestion, oldAnswerType, newAnswerType, tsChange)
        VALUES (NEW.userModified, NEW.questionID, OLD.questionNumber, NEW.questionNumber, OLD.page, NEW.page, OLD.required, NEW.required, OLD.question, NEW.question, OLD.answTypeId, NEW.answTypeId, callTs);

    NEW.tsModified = callTs;

    RETURN NEW;
END
$$ language plpgsql;

CREATE OR REPLACE FUNCTION tgf_update_accrequest() RETURNS trigger AS
$$
DECLARE callTs CONSTANT TIMESTAMP(0) := NOW()::TIMESTAMP(0);
BEGIN
    IF (NEW.grantedOn IS NOT NULL) THEN
        NEW.expiresOn = NULL;
    END IF;
    RETURN NEW;
END
$$ language plpgsql;

DROP TRIGGER IF EXISTS tg_form_bu ON Form CASCADE;
DROP TRIGGER IF EXISTS tg_question_bu ON Question CASCADE;
DROP TRIGGER IF EXISTS tg_accessrequest_bu ON AccessRequest CASCADE;

CREATE TRIGGER tg_form_bu
    BEFORE UPDATE ON Form
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_update_form_entry();

CREATE TRIGGER tg_question_bu
    BEFORE UPDATE ON Question
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_update_question_entry();

CREATE TRIGGER tg_accessrequest_bu
    BEFORE UPDATE ON AccessRequest
    FOR EACH ROW
        EXECUTE PROCEDURE tgf_update_accrequest();

/* After insert triggers and functions */

/* After update triggers and functions */