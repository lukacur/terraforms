INSERT INTO LoginProvider (providerId, providerName) VALUES
    (0, 'LOCAL'),
    (1, 'GOOGLE');

INSERT INTO Status (statusId, statusName) VALUES
    (0, 'ENABLED'),
    (1, 'ACTIVATION_PENDING'),
    (2, 'DISABLED'),
    (3, 'DELETION_PENDING'),
    (4, 'DELETED');

INSERT INTO RestrictionType (rtypeId, restrictionName) VALUES
    (0, 'NONE'),
    (1, 'LOGGED_IN'),
    (2, 'SELECTED');

INSERT INTO AccessType (accTypeId, typeName) VALUES
    (0, 'VIEW'),
    (1, 'EDIT'),
    (2, 'STAT');

INSERT INTO AnswerType (answTypeId, answerTypeName) VALUES
    (0, 'SHORTTEXT'),
    (1, 'LONGTEXT'),
    (2, 'NUMBER'),
    (3, 'SCALE'),
    (4, 'RADIO'),
    (5, 'CHECKBOX'),
    (6, 'TRUEFALSE'),
    (7, 'SCALEPERCENT');