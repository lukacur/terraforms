# Opis aplikacije (HR)

## TerraForms

Aplikacija za izradu izradu upitnika (anketa)

## Upute za pokretanje

1. potrebno je klonirati repozitorij projekta
2. potrebno se je pozicionirati na granu master (ako to već nije slučaj)
3. potrebno je uspostaviti strukturu baze podataka. Za to postoji SQL skripta *db_build_full.sql* koja se nalazi u direktoriju /DB/SQL. Ovu skriptu potrebno je pokrenuti u upravljaču baze podataka kako bi se stvorile sve relacije, popratni okidači i pretpostavljeni podatci. Skripta je napravljena za PostgreSQL upravitelj bazama podataka
4. potrebno je postaviti korisnika i URI za bazu podataka na poslužitelju u datoteci /backend/src/main/resources/application.properties. Dodatno je u istoj datoteci potrebno je izmjeniti podatke za OAuth2 Google klijenta

### Upute za pokretanje aplikacije (produkcija)

5. izgradnja poslužitelja moguća je pozicioniranjem u direktorij /backend i pokretanjem naredbe `mvn package` u naredbenom retku
6. nakon toga poslužitelj se pokreće naredbom `mvn exec:java -Dexec.mainClass="hr.fer.lc.terraforms.TerraformsFinal"`. Za ovaj i prethodni korak potrebno je imati instaliran [Apache Maven](https://maven.apache.org) i u varijablama okruženja imati postavljen JAVA_HOME na virtualni stroj Jave 17
7. aplikaciji se sada može pristupiti na adresi http://localhost:8080

### Upute za pokretanje aplikacije (razvoj)

5. potrebno se je pozicionirati u direktorij poslužitelja (/backend) i tamo otvoriti bilo koji IDE. Kôd poslužitelja se nakon izmjene može izgraditi i pokrenuti na način opisan u prethodnom poglavlju ili kroz IDE koji se koristi za izmjenu koda
6. potrebno se je pozicionirati u direktorij korisničkog sučelja (/frontend) i tamo otvoriti bilo koji IDE
7. pokretanjem naredbe `npm install` u naredbenom retku potrebno je instalirati ovisnosti
8. pokretanje poslužitelja korisničkog sučelja obavlja se naredbom `npm run start`
9. poslužitelju korisničkog sučelja sada se može pristupiti na adresi http://localhost:3000
10. izmjene kôda korisničkog sučelja automatski se reflektiraju na svim povezanim klijentima (nije potrebno ponovno pokretati naredbu `npm run start`)


---


# Application description (EN)

## TerraForms

Application for survey administration

## Startup instructions

1. clone the project repository
2. checkout to the master branch (if that already isn't the case)
3. setup the database structure. There is an SQL script *db_build_full.sql* in /DB/SQL. This script should be run in the database management system's interface in order to create all relations, triggers and default relation entries. The script was made for the PostgreSQL database management system
4. set the username and password for the database user and the database JDBC URI in the /backend/src/main/resources/application.properties file. Also, change the credentials for the OAuth2 Google client

### Application startup instructions (production)

5. building the server from source is possible by positioning in /backend directory and running `mvn package` in the command line
6. starting the server is possible by running `mvn exec:java -Dexec.mainClass="hr.fer.lc.terraforms.TerraformsFinal"`. For the previous two steps the Apache Maven should be installed [Apache Maven](https://maven.apache.org) and the JAVA_HOME environment variable should be set to a java 17 virtual machine
7. the application can now be accessed at http://localhost:8080

### Application startup instructions (development)

5. change the working directory to /backend and open any IDE. The server can be built from source at any time by following instructions in the previous section or by building it through the IDE
6. change the working directory to /frontend and open any IDE
7. running the command `npm install` in the command line installs all the dependencies of the frontend UI
8. start the frontend server by running `npm run start` in the command line
9. the frontend server can now be accessed at http://localhost:3000
10. frontend code changes are automatically reflected on all connected clients (no need to run `npm run start` again)
