package hr.fer.lc.terraforms.dto;

import java.util.List;

public class AnswerDTO {
    private Long formId;
    private List<AnswerPartDTO> answers;

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public List<AnswerPartDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerPartDTO> answers) {
        this.answers = answers;
    }
}
