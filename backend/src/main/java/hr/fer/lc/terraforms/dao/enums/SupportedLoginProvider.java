package hr.fer.lc.terraforms.dao.enums;

import hr.fer.lc.terraforms.dao.LoginProvider;

/**
 * @author Luka Ćurić
 */
public enum SupportedLoginProvider {
    LOCAL(0),
    GOOGLE(1),
    FACEBOOK(2);

    private final int loginProviderId;

    private static final LoginProvider PROVIDER_LOCAL;
    private static final LoginProvider PROVIDER_GOOGLE;
    private static final LoginProvider PROVIDER_FACEBOOK;

    static {
        PROVIDER_LOCAL = new LoginProvider();
        {
            PROVIDER_LOCAL.setId(0);
            PROVIDER_LOCAL.setProvidername("LOCAL");
        }

        PROVIDER_GOOGLE = new LoginProvider();
        {
            PROVIDER_GOOGLE.setId(1);
            PROVIDER_GOOGLE.setProvidername("GOOGLE");
        }

        PROVIDER_FACEBOOK = new LoginProvider();
        {
            PROVIDER_FACEBOOK.setId(2);
            PROVIDER_FACEBOOK.setProvidername("FACEBOOK");
        }
    }

    SupportedLoginProvider(int loginProviderId) {
        this.loginProviderId = loginProviderId;
    }

    public int getLoginProviderId() {
        return loginProviderId;
    }

    public LoginProvider getLoginProvider() {
        return switch (this) {
            case LOCAL -> PROVIDER_LOCAL;
            case GOOGLE -> PROVIDER_GOOGLE;
            case FACEBOOK -> PROVIDER_FACEBOOK;
        };
    }

    public static SupportedLoginProvider withValue(int value) {
        return switch (value) {
            case 0 -> LOCAL;
            case 1 -> GOOGLE;
            case 2 -> FACEBOOK;
            default -> throw new IllegalArgumentException("Invalid login provider id");
        };
    }
}
