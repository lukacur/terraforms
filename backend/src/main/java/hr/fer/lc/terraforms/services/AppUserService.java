package hr.fer.lc.terraforms.services;

import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.LoginProvider;
import hr.fer.lc.terraforms.dto.UserInfoDTO;
import hr.fer.lc.terraforms.dto.UserInfoUpdateDTO;
import hr.fer.lc.terraforms.dto.UserRegisterDTO;

import java.util.List;
import java.util.Optional;

/**
 * @author Luka Ćurić
 */
public interface AppUserService {
    boolean existsByEmail(String email);
    Optional<AppUser> getUserByEmail(String email);
    Optional<AppUser> getUserByUsername(String username);
    Optional<AppUser> getUserById(Long id);

    void registerUser(UserRegisterDTO data);
    AppUser registerOAuthUser(String username, String email, LoginProvider loginProvider);
    void handleOAuthLoginEmail(String email);
    void handleOAuthLoginUsername(String username);

    UserInfoDTO getUserInfo(AppUser user);
    void updateUserInfo(AppUser user, UserInfoUpdateDTO newUserInfo);

    List<String> userNamesContaining(String pattern);
}
