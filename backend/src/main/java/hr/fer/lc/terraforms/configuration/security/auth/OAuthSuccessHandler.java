package hr.fer.lc.terraforms.configuration.security.auth;

import hr.fer.lc.terraforms.constants.SessionAttribute;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.enums.SupportedLoginProvider;
import hr.fer.lc.terraforms.services.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Luka Ćurić
 */
@Component
public class OAuthSuccessHandler implements AuthenticationSuccessHandler {
    private final boolean HAS_CORS = false;
    private final String PREFIX = (HAS_CORS) ? "http://localhost:3000" : "";

    @Autowired
    private AppUserService appUserService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        ProprietaryOidcUser loggedInUser = (ProprietaryOidcUser) authentication.getPrincipal();

        String userEmail = loggedInUser.getEmail();
        boolean userExists = appUserService.existsByEmail(userEmail);

        if (userExists) {
            AppUser user = appUserService.getUserByEmail(userEmail).orElse(null);
            assert user != null;

            if (user.getLoginprovider().getId() == SupportedLoginProvider.LOCAL.getLoginProviderId()) {
                authentication.setAuthenticated(false);
                request.getSession().invalidate();
                response.sendRedirect(PREFIX + "/login");
                return;
            }

            request.getSession().setAttribute(SessionAttribute.CURRENT_USER, user);

            response.sendRedirect(PREFIX + "/");
        }

        request.getSession().setAttribute("email", userEmail);

        if (!userExists) {
            request.getSession().setAttribute(SessionAttribute.USERNAME_REQUIRED, true);
            authentication.setAuthenticated(true);
            request.getSession().setAttribute(
                    SessionAttribute.OAUTH_PROVIDER,
                    SupportedLoginProvider.GOOGLE.getLoginProvider()
            );
            response.sendRedirect(PREFIX + "/setupUsername");
        }
    }
}
