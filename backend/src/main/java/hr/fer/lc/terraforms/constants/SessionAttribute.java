package hr.fer.lc.terraforms.constants;

public class SessionAttribute {
    public static final String CURRENT_USER = "currentUser";
    public static final String SELECTED_FORM = "selectedForm";
    public static final String USERNAME_REQUIRED = "usernameRequired";
    public static final String OAUTH_PROVIDER = "OAuthProvider";
}
