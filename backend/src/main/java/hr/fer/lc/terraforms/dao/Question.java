package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "questionid", nullable = false)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usercreated")
    private AppUser usercreated;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "formid")
    private Form formid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "previousversion")
    private Question previousversion;

    @Column(name = "questionnumber", nullable = false)
    private Integer questionnumber;

    @Column(name = "page", nullable = false)
    private Integer page;

    @Column(name = "required", nullable = false)
    private Boolean required = false;

    @Column(name = "tscreated", insertable = false, updatable = false)
    private LocalDateTime tscreated;

    @Column(name = "tsmodified")
    private LocalDateTime tsmodified;

    @Column(name = "question", nullable = false, length = 256)
    private String question;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usermodified")
    private AppUser usermodified;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "answtypeid", nullable = false)
    private AnswerType answtypeid;

    @OneToMany(mappedBy = "questionid")
    private Set<Answer> answers = new LinkedHashSet<>();

    @OneToMany(mappedBy = "questionchanged")
    private Set<QuestionChange> questionchanges = new LinkedHashSet<>();

    @OneToMany(mappedBy = "previousversion")
    private Set<Question> questions = new LinkedHashSet<>();

    @OneToMany(mappedBy = "questionid")
    private Set<PossibleAnswer> possibleanswers = new LinkedHashSet<>();

    public Set<PossibleAnswer> getPossibleanswers() {
        return possibleanswers;
    }

    public void setPossibleanswers(Set<PossibleAnswer> possibleanswers) {
        this.possibleanswers = possibleanswers;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public Set<QuestionChange> getQuestionchanges() {
        return questionchanges;
    }

    public void setQuestionchanges(Set<QuestionChange> questionchanges) {
        this.questionchanges = questionchanges;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    public Question getPreviousversion() {
        return previousversion;
    }

    public void setPreviousversion(Question previousversion) {
        this.previousversion = previousversion;
    }

    public AnswerType getAnswtypeid() {
        return answtypeid;
    }

    public void setAnswtypeid(AnswerType answtypeid) {
        this.answtypeid = answtypeid;
    }

    public AppUser getUsermodified() {
        return usermodified;
    }

    public void setUsermodified(AppUser usermodified) {
        this.usermodified = usermodified;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public LocalDateTime getTsmodified() {
        return tsmodified;
    }

    public void setTsmodified(LocalDateTime tsmodified) {
        this.tsmodified = tsmodified;
    }

    public LocalDateTime getTscreated() {
        return tscreated;
    }

    public void setTscreated(LocalDateTime tscreated) {
        this.tscreated = tscreated;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getQuestionnumber() {
        return questionnumber;
    }

    public void setQuestionnumber(Integer questionnumber) {
        this.questionnumber = questionnumber;
    }

    public Form getFormid() {
        return formid;
    }

    public void setFormid(Form formid) {
        this.formid = formid;
    }

    public AppUser getUsercreated() {
        return usercreated;
    }

    public void setUsercreated(AppUser usercreated) {
        this.usercreated = usercreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}