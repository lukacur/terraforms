package hr.fer.lc.terraforms.exceptions.data;

/**
 * @author Luka Ćurić
 */
public class UserAlreadyExistsException extends RuntimeException {
    public UserAlreadyExistsException() {
    }

    public UserAlreadyExistsException(String message) {
        super(message);
    }
}
