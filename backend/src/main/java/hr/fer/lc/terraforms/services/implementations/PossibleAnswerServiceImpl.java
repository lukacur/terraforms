package hr.fer.lc.terraforms.services.implementations;

import hr.fer.lc.terraforms.dao.PossibleAnswer;
import hr.fer.lc.terraforms.dao.Question;
import hr.fer.lc.terraforms.dto.PossibleAnswerDTO;
import hr.fer.lc.terraforms.services.PossibleAnswerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class PossibleAnswerServiceImpl implements PossibleAnswerService {
    @Override
    @Transactional
    public List<PossibleAnswerDTO> getDtosForQuestion(Question question) {
        List<PossibleAnswerDTO> possibleAnswers = new LinkedList<>();

        for (PossibleAnswer possibleAnswer : question.getPossibleanswers()) {
            possibleAnswers.add(new PossibleAnswerDTO(
                    possibleAnswer.getName(),
                    possibleAnswer.getValue()
            ));
        }

        return possibleAnswers;
    }
}
