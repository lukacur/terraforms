package hr.fer.lc.terraforms.services;

import hr.fer.lc.terraforms.dao.AppUser;

/**
 * @author Luka Ćurić
 */
public interface StoreService {
    String getItem(AppUser user, String itemKey);
    void setItem(AppUser user, String itemKey, String itemValue);
}
