package hr.fer.lc.terraforms.services;

import hr.fer.lc.terraforms.dao.Question;
import hr.fer.lc.terraforms.dto.PossibleAnswerDTO;

import java.util.List;

public interface PossibleAnswerService {
    List<PossibleAnswerDTO> getDtosForQuestion(Question question);
}
