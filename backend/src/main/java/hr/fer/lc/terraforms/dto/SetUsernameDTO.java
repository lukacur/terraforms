package hr.fer.lc.terraforms.dto;

/**
 * @author Luka Ćurić
 */
public class SetUsernameDTO {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
