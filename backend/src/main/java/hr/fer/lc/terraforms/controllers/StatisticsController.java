package hr.fer.lc.terraforms.controllers;

import hr.fer.lc.terraforms.constants.SessionAttribute;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dto.FormDTO;
import hr.fer.lc.terraforms.dto.FormStatisticsDTO;
import hr.fer.lc.terraforms.services.AuthorityService;
import hr.fer.lc.terraforms.services.FormService;
import hr.fer.lc.terraforms.services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Luka Ćurić
 */
@Controller
@RequestMapping("/api/data/statistics")
public class StatisticsController {
    private FormService formService;
    @Autowired
    public void setFormService(FormService formService) {
        this.formService = formService;
    }

    private StatisticsService statisticsService;
    @Autowired
    public void setStatisticsService(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    private AuthorityService authorityService;
    @Autowired
    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    @PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getResponsesForm(HttpServletRequest req, @RequestBody FormDTO formInfo) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        FormStatisticsDTO statistics = statisticsService.buildStatistics(formInfo.getFormId(), currentUser);

        return new ResponseEntity<>(statistics, HttpStatus.OK);
    }

    @GetMapping(path = "/{statsLink}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getResponsesLink(HttpServletRequest req, @PathVariable("statsLink") String statsLink) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        Long formId = formService.getFormByStatsLink(statsLink).getId();
        FormStatisticsDTO statistics = statisticsService.buildStatistics(formId, currentUser);

        return new ResponseEntity<>(statistics, HttpStatus.OK);
    }

    @PostMapping(path = "/pdf", produces = { MediaType.APPLICATION_PDF_VALUE })
    public void getPdfStatistics(HttpServletResponse resp, @RequestBody FormStatisticsDTO statistics)
            throws IOException {
        byte[] pdf = statisticsService.getPDFStatistics(statistics);
        resp.setContentType("application/pdf");
        resp.setContentLengthLong(pdf.length);
        resp.setHeader("Content-Disposition", "attachment; filename=\"statistics.pdf\"");

        resp.getOutputStream().write(pdf);
    }

    @PostMapping(path = "/csv", produces = { MediaType.TEXT_PLAIN_VALUE })
    public void getCsvStatistics(HttpServletResponse resp, @RequestBody FormStatisticsDTO statistics)
            throws IOException {
        byte[] csv = statisticsService.getCSVStatistics(statistics);
        resp.setContentType("text/plain");
        resp.setContentLengthLong(csv.length);
        resp.setHeader("Content-Disposition", "attachment; filename=\"statistics.csv\"");

        resp.getOutputStream().write(csv);
    }

    @PostMapping(path = "/txt", produces = { MediaType.TEXT_PLAIN_VALUE })
    public void getTxtStatistics(HttpServletResponse resp, @RequestBody FormStatisticsDTO statistics)
            throws IOException {
        byte[] txt = statisticsService.getTXTStatistics(statistics);
        resp.setContentType("text/plain");
        resp.setContentLengthLong(txt.length);
        resp.setHeader("Content-Disposition", "attachment; filename=\"statistics.txt\"");

        resp.getOutputStream().write(txt);
    }
}
