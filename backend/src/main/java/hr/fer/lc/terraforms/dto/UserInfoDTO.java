package hr.fer.lc.terraforms.dto;

/**
 * @author Luka Ćurić
 */
public class UserInfoDTO {
    private String username;
    private String email;
    private boolean isLocalUser;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getIsLocalUser() {
        return isLocalUser;
    }

    public void setIsLocalUser(boolean localUser) {
        isLocalUser = localUser;
    }
}
