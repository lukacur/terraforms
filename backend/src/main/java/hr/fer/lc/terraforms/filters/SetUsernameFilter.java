package hr.fer.lc.terraforms.filters;

import hr.fer.lc.terraforms.configuration.ConfigVars;
import hr.fer.lc.terraforms.constants.SessionAttribute;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @author Luka Ćurić
 */
@Component
@Order
public class SetUsernameFilter implements Filter {
    private final boolean HAS_CORS = false;
    private final String PREFIX = (HAS_CORS) ? "http://localhost:3000" : "";
    private static final Pattern RESOURCE_PATTERN = Pattern.compile(
            "(.*?\\.css)|(.*?\\.html)|(.*?\\.js)|(.*?\\.json)|(.*?\\.ico)|(.*?\\.png)|(.*?\\.txt)|(.*?\\.map)"
    );

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        HttpSession session = req.getSession();

        // CORS rule
        if ((!req.getRequestURI().startsWith(ConfigVars.REST_API_ROOT)
                && session.getAttribute(SessionAttribute.USERNAME_REQUIRED) == null
            ) || HttpMethod.OPTIONS.matches(req.getMethod())
                || session.getAttribute(SessionAttribute.USERNAME_REQUIRED) == null
                || req.getRequestURI().endsWith("/isLoggedIn")
                || req.getRequestURI().equals(PREFIX + "/setupUsername")
                || RESOURCE_PATTERN.matcher(req.getRequestURI()).matches()) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else if (!req.getRequestURI().equals("/api/user/setusername")) {
            resp.sendRedirect(PREFIX + "/setupUsername");
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {}

    @Override
    public void destroy() {}
}
