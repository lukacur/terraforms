package hr.fer.lc.terraforms.dto;

import java.util.List;

public class AccessDataDTO {
    private Long formId;
    private List<AccessRequestDTO> requests;

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public List<AccessRequestDTO> getRequests() {
        return requests;
    }

    public void setRequests(List<AccessRequestDTO> requests) {
        this.requests = requests;
    }
}
