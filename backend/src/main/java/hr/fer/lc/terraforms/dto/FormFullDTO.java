package hr.fer.lc.terraforms.dto;

public class FormFullDTO {
    private FormDTO formData;
    private FormBodyDTO bodyData;

    public FormFullDTO() {}

    public FormFullDTO(FormDTO formData, FormBodyDTO bodyData) {
        this.formData = formData;
        this.bodyData = bodyData;
    }

    public FormDTO getFormData() {
        return formData;
    }

    public void setFormData(FormDTO formData) {
        this.formData = formData;
    }

    public FormBodyDTO getBodyData() {
        return bodyData;
    }

    public void setBodyData(FormBodyDTO bodyData) {
        this.bodyData = bodyData;
    }
}
