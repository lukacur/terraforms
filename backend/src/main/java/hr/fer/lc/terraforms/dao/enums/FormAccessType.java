package hr.fer.lc.terraforms.dao.enums;

import hr.fer.lc.terraforms.dao.AccessType;

/**
 * @author Luka Ćurić
 */
public enum FormAccessType {
    VIEW(0),
    EDIT(1),
    STAT(2);

    private final int accessTypeId;

    private static final AccessType ACCESS_VIEW;
    private static final AccessType ACCESS_EDIT;
    private static final AccessType ACCESS_STAT;

    static {
        ACCESS_VIEW = new AccessType();
        {
            ACCESS_VIEW.setTypename("VIEW");
            ACCESS_VIEW.setId(0);
        }

        ACCESS_EDIT = new AccessType();
        {
            ACCESS_EDIT.setTypename("EDIT");
            ACCESS_EDIT.setId(1);
        }

        ACCESS_STAT = new AccessType();
        {
            ACCESS_STAT.setTypename("STAT");
            ACCESS_STAT.setId(2);
        }
    }

    FormAccessType(int accessTypeId) {
        this.accessTypeId = accessTypeId;
    }

    public int getAccessTypeId() {
        return accessTypeId;
    }

    public AccessType getAccessType() {
        return switch (this) {
            case VIEW -> ACCESS_VIEW;
            case EDIT -> ACCESS_EDIT;
            case STAT -> ACCESS_STAT;
        };
    }

    public static FormAccessType withValue(int value) {
        return switch (value) {
            case 0 -> VIEW;
            case 1 -> EDIT;
            case 2 -> STAT;
            default -> throw new IllegalArgumentException("Invalid access type id");
        };
    }
}
