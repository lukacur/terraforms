package hr.fer.lc.terraforms.exceptions.action;

public class IllegalUserActionException extends RuntimeException {
    public IllegalUserActionException() {
    }

    public IllegalUserActionException(String message) {
        super(message);
    }

    public IllegalUserActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalUserActionException(Throwable cause) {
        super(cause);
    }
}
