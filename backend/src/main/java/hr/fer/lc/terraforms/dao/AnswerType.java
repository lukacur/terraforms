package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "answertype")
public class AnswerType {
    @Id
    @Column(name = "answtypeid", nullable = false)
    private Integer id;

    @Column(name = "answertypename", nullable = false, length = 64)
    private String answertypename;

    @OneToMany(mappedBy = "answtypeid")
    private Set<Question> questions = new LinkedHashSet<>();

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public String getAnswertypename() {
        return answertypename;
    }

    public void setAnswertypename(String answertypename) {
        this.answertypename = answertypename;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}