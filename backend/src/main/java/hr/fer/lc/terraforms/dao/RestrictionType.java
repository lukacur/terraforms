package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "restrictiontype")
public class RestrictionType {
    @Id
    @Column(name = "rtypeid", nullable = false)
    private Integer id;

    @Column(name = "restrictionname", nullable = false, length = 64)
    private String restrictionname;

    @OneToMany(mappedBy = "restrictedto")
    private Set<Form> forms = new LinkedHashSet<>();

    public Set<Form> getForms() {
        return forms;
    }

    public void setForms(Set<Form> forms) {
        this.forms = forms;
    }

    public String getRestrictionname() {
        return restrictionname;
    }

    public void setRestrictionname(String restrictionname) {
        this.restrictionname = restrictionname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}