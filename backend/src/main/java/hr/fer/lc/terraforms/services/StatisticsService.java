package hr.fer.lc.terraforms.services;

import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dto.FormStatisticsDTO;

/**
 * @author Luka Ćurić
 */
public interface StatisticsService {
    FormStatisticsDTO buildStatistics(Long formId, AppUser userRequesting);
    byte[] getPDFStatistics(FormStatisticsDTO formStatistics);
    byte[] getCSVStatistics(FormStatisticsDTO formStatistics);
    byte[] getTXTStatistics(FormStatisticsDTO formStatistics);
}
