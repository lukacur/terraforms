package hr.fer.lc.terraforms.controllers;

import hr.fer.lc.terraforms.constants.SessionAttribute;
import hr.fer.lc.terraforms.dto.UserRegisterDTO;
import hr.fer.lc.terraforms.services.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Luka Ćurić
 */
@RestController
@RequestMapping("/api")
public class LoginRegisterController {
    @Autowired
    private AppUserService appUserService;

    @PostMapping(path = "/register")
    public void registerUser(HttpServletRequest req, HttpServletResponse resp, @RequestBody UserRegisterDTO data)
            throws IOException {
        if (req.getSession().getAttribute(SessionAttribute.CURRENT_USER) != null)
            resp.sendRedirect("/");

        appUserService.registerUser(data);
        resp.setStatus(HttpStatus.OK.value());
    }

    @GetMapping("/isLoggedIn")
    public ResponseEntity<?> getLoginStatus(HttpServletRequest req, Authentication auth) {
        return new ResponseEntity<>(auth != null && auth.isAuthenticated(), HttpStatus.OK);
    }
}
