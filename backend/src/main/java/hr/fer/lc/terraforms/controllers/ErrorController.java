package hr.fer.lc.terraforms.controllers;

import hr.fer.lc.terraforms.exceptions.action.IllegalUserActionException;
import hr.fer.lc.terraforms.exceptions.action.UnauthorizedUserActionException;
import hr.fer.lc.terraforms.exceptions.data.EntryDoesNotExistException;
import hr.fer.lc.terraforms.exceptions.data.UserDoesNotExistException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ErrorController {
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleIllegalArgumentException(HttpServletRequest req, HttpServletResponse resp,
                                                            Throwable ex) {
        return new ResponseEntity<>("An error occured: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ IllegalUserActionException.class, UnauthorizedUserActionException.class })
    public ResponseEntity<?> handleIllegalUserActionException(HttpServletRequest req, HttpServletResponse resp,
                                                              Throwable ex) {
        return new ResponseEntity<>("An error occured: " + ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({ EntryDoesNotExistException.class, UserDoesNotExistException.class })
    public ResponseEntity<?> handleNonExistantResourcesExceptions(HttpServletRequest req, HttpServletResponse resp,
                                                                  Throwable ex) {
        return new ResponseEntity<>("An error occured: " + ex.getMessage(), HttpStatus.NOT_FOUND);
    }
}
