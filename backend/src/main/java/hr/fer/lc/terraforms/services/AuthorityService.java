package hr.fer.lc.terraforms.services;

import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;

public interface AuthorityService {
    boolean hasReadAuthority(AppUser user, Form form);
    boolean hasEditAuthority(AppUser user, Form form);
    boolean hasStatisticsAuthority(AppUser user, Form form);
}
