package hr.fer.lc.terraforms.exceptions.data;

import java.io.Serial;

/**
 * @author Luka Ćurić
 */
public class UserDoesNotExistException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 65476546516516L;

    public UserDoesNotExistException() {
    }

    public UserDoesNotExistException(String message) {
        super(message);
    }
}
