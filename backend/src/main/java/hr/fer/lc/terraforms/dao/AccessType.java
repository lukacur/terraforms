package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "accesstype")
public class AccessType {
    @Id
    @Column(name = "acctypeid", nullable = false)
    private Integer id;

    @Column(name = "typename", nullable = false, length = 64)
    private String typename;

    @OneToMany(mappedBy = "accesstype")
    private Set<AccessRequest> accessrequests = new LinkedHashSet<>();

    public Set<AccessRequest> getAccessrequests() {
        return accessrequests;
    }

    public void setAccessrequests(Set<AccessRequest> accessrequests) {
        this.accessrequests = accessrequests;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}