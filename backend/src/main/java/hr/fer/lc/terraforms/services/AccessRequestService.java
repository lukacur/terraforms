package hr.fer.lc.terraforms.services;

import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dao.enums.FormAccessType;
import hr.fer.lc.terraforms.dto.AccessRequestDTO;

import java.util.List;

public interface AccessRequestService {
    void updateRequests(Form form, List<AccessRequestDTO> request, AppUser userUpdating);

    void grantPermission(AccessRequestDTO request, AppUser userGranting);
    void denyPermission(AccessRequestDTO request, AppUser userRevoking);

    void addRequestFor(Form form, FormAccessType accessType, AppUser userRequesting);

    void cancelPermission(AppUser userCanceling, Form form);
}
