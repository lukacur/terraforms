package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "answer")
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "answerid", nullable = false)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usercreated")
    private AppUser usercreated;

    @Lob
    @Column(name = "value", nullable = false)
    private String value;

    @Column(name = "tscreated", insertable = false)
    private LocalDateTime tscreated;

    @Column(name = "tsmodified")
    private LocalDateTime tsmodified;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usermodified")
    private AppUser usermodified;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "questionid", nullable = false)
    private Question questionid;

    public Question getQuestionid() {
        return questionid;
    }

    public void setQuestionid(Question questionid) {
        this.questionid = questionid;
    }

    public AppUser getUsermodified() {
        return usermodified;
    }

    public void setUsermodified(AppUser usermodified) {
        this.usermodified = usermodified;
    }

    public LocalDateTime getTsmodified() {
        return tsmodified;
    }

    public void setTsmodified(LocalDateTime tsmodified) {
        this.tsmodified = tsmodified;
    }

    public LocalDateTime getTscreated() {
        return tscreated;
    }

    public void setTscreated(LocalDateTime tscreated) {
        this.tscreated = tscreated;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public AppUser getUsercreated() {
        return usercreated;
    }

    public void setUsercreated(AppUser usercreated) {
        this.usercreated = usercreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}