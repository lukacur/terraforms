package hr.fer.lc.terraforms.services.implementations;

import hr.fer.lc.terraforms.dao.*;
import hr.fer.lc.terraforms.dao.enums.QuestionAnswerType;
import hr.fer.lc.terraforms.dto.AnswerPartDTO;
import hr.fer.lc.terraforms.exceptions.action.UnauthorizedUserActionException;
import hr.fer.lc.terraforms.repositories.AnswerRepository;
import hr.fer.lc.terraforms.repositories.FormRepository;
import hr.fer.lc.terraforms.services.AnswerService;
import hr.fer.lc.terraforms.services.AuthorityService;
import hr.fer.lc.terraforms.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AnswerServiceImpl implements AnswerService {
    private AuthorityService authorityService;
    @Autowired
    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    private AnswerRepository answerRepository;
    @Autowired
    public void setAnswerRepository(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    private QuestionService questionService;
    @Autowired
    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    private FormRepository formRepository;
    @Autowired
    public void setFormRepository(FormRepository formRepository) {
        this.formRepository = formRepository;
    }

    private boolean isValidAnswer(AnswerPartDTO answer, Question question) {
        final List<String> trueFalseValues =  List.of("true", "false");
        boolean isEmptyOrNull = answer == null || answer.getAnswer() == null || answer.getAnswer().strip().isEmpty();

        if (question.getRequired() && isEmptyOrNull)
            throw new IllegalArgumentException("Question answer is required.");
        else if (isEmptyOrNull)
            return true;

        return switch (QuestionAnswerType.withValue(question.getAnswtypeid().getId())) {
            case NUMBER -> {
                try {
                    Long.parseLong(answer.getAnswer());
                    yield true;
                } catch (Exception e) {
                    yield false;
                }
            }

            case CHECKBOX -> {
                Set<String> possibleValues = question.getPossibleanswers().stream()
                        .map(PossibleAnswer::getValue)
                        .collect(Collectors.toSet());

                yield Arrays.stream(answer.getAnswer().split(";"))
                        .allMatch(possibleValues::contains);
            }

            case SCALEPERCENT -> {
                try {
                    int scaleValue = Integer.parseInt(answer.getAnswer());

                    yield scaleValue >= 0 && scaleValue <= 100;
                } catch (Exception e) {
                    yield false;
                }
            }

            case SCALE, RADIO -> {
                Set<String> possibleValues = question.getPossibleanswers().stream()
                        .map(PossibleAnswer::getValue)
                        .collect(Collectors.toSet());

                yield possibleValues.contains(answer.getAnswer());
            }

            case TRUEFALSE -> trueFalseValues.contains(answer.getAnswer());

            default -> true;
        };
    }

    @Override
    @Transactional(rollbackFor = { UnauthorizedUserActionException.class, IllegalArgumentException.class })
    public void saveAnswers(Form form, List<AnswerPartDTO> answers, AppUser userAnswering) {
        if (!authorityService.hasReadAuthority(userAnswering, form))
            throw new UnauthorizedUserActionException("User doesn't have access to the form specified.");

        List<Question> formQuestions = formRepository.getCurrentQuestionsFor(form.getId()).stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        for (Question question : formQuestions) {
            AnswerPartDTO answerPart = answers.stream()
                    .filter(ap -> ap.getQuestionId().equals(question.getId()))
                    .findFirst()
                    .orElse(null);

            if (!isValidAnswer(answerPart, question))
                throw new IllegalArgumentException("Invalid answer for question.");

            Answer answer = new Answer();
            {
                answer.setUsercreated(userAnswering);

                if (answerPart == null || answerPart.getAnswer() == null || answerPart.getAnswer().strip().isEmpty())
                    answer.setValue("");
                else
                    answer.setValue(answerPart.getAnswer());

                answer.setQuestionid(question);
            }

            answerRepository.save(answer);
        }
    }
}
