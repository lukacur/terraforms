package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "status")
public class Status {
    @Id
    @Column(name = "statusid", nullable = false)
    private Integer id;

    @Column(name = "statusname", length = 64)
    private String statusname;

    @OneToMany(mappedBy = "status")
    private Set<AppUser> appusers = new LinkedHashSet<>();

    public Set<AppUser> getAppusers() {
        return appusers;
    }

    public void setAppusers(Set<AppUser> appusers) {
        this.appusers = appusers;
    }

    public String getStatusname() {
        return statusname;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}