package hr.fer.lc.terraforms.controllers;

import hr.fer.lc.terraforms.constants.SessionAttribute;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dto.FormBodyDTO;
import hr.fer.lc.terraforms.dto.FormDTO;
import hr.fer.lc.terraforms.dto.FormFullDTO;
import hr.fer.lc.terraforms.services.AuthorityService;
import hr.fer.lc.terraforms.services.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Luka Ćurić
 */
@Controller
@RequestMapping(path = "/api/form", consumes = { MediaType.APPLICATION_JSON_VALUE })
public class FormBuildController {
    @Autowired
    private FormService formService;

    private AuthorityService authorityService;
    @Autowired
    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    @PostMapping("/createForm")
    public ResponseEntity<?> createNewForm(HttpServletRequest req, HttpServletResponse resp,
                                           @RequestBody FormDTO incomingForm) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        Form formCreated = formService.createForm(incomingForm, currentUser);
        incomingForm.setFormId(formCreated.getId());
        incomingForm.setFormLink(formCreated.getViewlink());

        if (authorityService.hasStatisticsAuthority(currentUser, formCreated))
            incomingForm.setStatsLink(formCreated.getStatslink());


        return new ResponseEntity<>(incomingForm, HttpStatus.OK);
    }

    @PostMapping("/updateFormMetadata")
    public ResponseEntity<?> updateForm(HttpServletRequest req, HttpServletResponse resp,
                                        @RequestBody FormDTO incomingForm) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        formService.updateFormMetadata(incomingForm, currentUser);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @DeleteMapping("/deleteForm")
    public ResponseEntity<?> deleteForm(HttpServletRequest req, @RequestBody FormFullDTO incomingForm) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        formService.deleteForm(incomingForm.getFormData(), currentUser);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PutMapping("/saveOver")
    public ResponseEntity<?> saveOverForm(HttpServletRequest req, HttpServletResponse resp,
                                         @RequestBody FormBodyDTO formBody) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        FormBodyDTO modifiedBody = formService.saveOver(formBody, currentUser);

        return new ResponseEntity<>(modifiedBody, HttpStatus.OK);
    }

    @PatchMapping("/saveInto")
    public ResponseEntity<?> saveIntoForm(HttpServletRequest req, HttpServletResponse resp,
                                         @RequestBody FormBodyDTO formBody) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        formService.saveInto(formBody, currentUser);

        return new ResponseEntity<>("", HttpStatus.OK);
    }
}
