package hr.fer.lc.terraforms.configuration.security.authorities;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Luka Ćurić
 */
public class CustomGrantedAuthority implements GrantedAuthority {
    private final Permission permission;
    private final Long formId;

    public CustomGrantedAuthority(Permission permission, Long formId) {
        this.permission = permission;
        this.formId = formId;
    }

    @Override
    public String getAuthority() {
        return String.format("%s_%d", this.permission.getPermission(), formId);
    }
}
