package hr.fer.lc.terraforms.controllers;

import hr.fer.lc.terraforms.constants.SessionAttribute;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.LoginProvider;
import hr.fer.lc.terraforms.dto.SetUsernameDTO;
import hr.fer.lc.terraforms.dto.UserInfoDTO;
import hr.fer.lc.terraforms.dto.UserInfoUpdateDTO;
import hr.fer.lc.terraforms.services.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Luka Ćurić
 */
@RestController
@RequestMapping("/api/user")
public class UserController {
    private final boolean HAS_CORS = false;
    private final String PREFIX = (HAS_CORS) ? "http://localhost:3000" : "";

    private AppUserService appUserService;
    @Autowired
    public void setAppUserService(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @GetMapping("/info")
    public ResponseEntity<?> getUserInfo(HttpServletRequest req) {
        AppUser requestingUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        UserInfoDTO userInfo = appUserService.getUserInfo(requestingUser);

        return new ResponseEntity<>(userInfo, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateUserInfo(HttpServletRequest req, @RequestBody UserInfoUpdateDTO userInfoUpdate) {
        AppUser requestingUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        appUserService.updateUserInfo(requestingUser, userInfoUpdate);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping(path = "/setusername", consumes = { MediaType.APPLICATION_JSON_VALUE })
    public void setUsernameForNewUser(HttpServletRequest req, HttpServletResponse resp,
                                      @RequestBody SetUsernameDTO username)
            throws IOException {
        if (req.getSession().getAttribute(SessionAttribute.USERNAME_REQUIRED) == null)
            resp.sendRedirect(PREFIX + "/");

        LoginProvider provider = (LoginProvider) req.getSession().getAttribute(SessionAttribute.OAUTH_PROVIDER);
        String userEmail = (String) req.getSession().getAttribute("email");

        if (username.getUsername().strip().length() < 5)
            throw new IllegalArgumentException("Username must be at least 5 characters long.");

        AppUser registeredUser = appUserService.registerOAuthUser(username.getUsername(), userEmail, provider);
        req.getSession().setAttribute(SessionAttribute.CURRENT_USER, registeredUser);
        req.getSession().removeAttribute(SessionAttribute.USERNAME_REQUIRED);

        resp.sendRedirect(PREFIX + "/");
    }

    @GetMapping("/usernamesLike/{usernamePart}")
    public ResponseEntity<?> getUsernamesLike(HttpServletRequest req, @PathVariable("usernamePart") String pattern) {
        if (pattern.strip().length() < 3)
            throw new IllegalArgumentException("Pattern should be at least 3 characters long");

        List<String> users = appUserService.userNamesContaining(pattern);

        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}
