package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "questionchange")
public class QuestionChange {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "changeid", nullable = false)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userresponsible")
    private AppUser userresponsible;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "questionchanged")
    private Question questionchanged;

    @Column(name = "oldquestionnumber", nullable = false)
    private Integer oldquestionnumber;

    @Column(name = "newquestionnumber", nullable = false)
    private Integer newquestionnumber;

    @Column(name = "oldpage", nullable = false)
    private Integer oldpage;

    @Column(name = "newpage", nullable = false)
    private Integer newpage;

    @Column(name = "oldrequired", nullable = false)
    private Boolean oldrequired = false;

    @Column(name = "newrequired", nullable = false)
    private Boolean newrequired = false;

    @Column(name = "tschange", nullable = false)
    private LocalDateTime tschange;

    @Column(name = "oldquestion", nullable = false, length = 256)
    private String oldquestion;

    @Column(name = "newquestion", nullable = false, length = 256)
    private String newquestion;

    @Column(name = "oldanswertype", nullable = false)
    private Integer oldanswertype;

    @Column(name = "newanswertype", nullable = false)
    private Integer newanswertype;

    public Integer getNewanswertype() {
        return newanswertype;
    }

    public void setNewanswertype(Integer newanswertype) {
        this.newanswertype = newanswertype;
    }

    public Integer getOldanswertype() {
        return oldanswertype;
    }

    public void setOldanswertype(Integer oldanswertype) {
        this.oldanswertype = oldanswertype;
    }

    public String getNewquestion() {
        return newquestion;
    }

    public void setNewquestion(String newquestion) {
        this.newquestion = newquestion;
    }

    public String getOldquestion() {
        return oldquestion;
    }

    public void setOldquestion(String oldquestion) {
        this.oldquestion = oldquestion;
    }

    public LocalDateTime getTschange() {
        return tschange;
    }

    public void setTschange(LocalDateTime tschange) {
        this.tschange = tschange;
    }

    public Boolean getNewrequired() {
        return newrequired;
    }

    public void setNewrequired(Boolean newrequired) {
        this.newrequired = newrequired;
    }

    public Boolean getOldrequired() {
        return oldrequired;
    }

    public void setOldrequired(Boolean oldrequired) {
        this.oldrequired = oldrequired;
    }

    public Integer getNewpage() {
        return newpage;
    }

    public void setNewpage(Integer newpage) {
        this.newpage = newpage;
    }

    public Integer getOldpage() {
        return oldpage;
    }

    public void setOldpage(Integer oldpage) {
        this.oldpage = oldpage;
    }

    public Integer getNewquestionnumber() {
        return newquestionnumber;
    }

    public void setNewquestionnumber(Integer newquestionnumber) {
        this.newquestionnumber = newquestionnumber;
    }

    public Integer getOldquestionnumber() {
        return oldquestionnumber;
    }

    public void setOldquestionnumber(Integer oldquestionnumber) {
        this.oldquestionnumber = oldquestionnumber;
    }

    public Question getQuestionchanged() {
        return questionchanged;
    }

    public void setQuestionchanged(Question questionchanged) {
        this.questionchanged = questionchanged;
    }

    public AppUser getUserresponsible() {
        return userresponsible;
    }

    public void setUserresponsible(AppUser userresponsible) {
        this.userresponsible = userresponsible;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}