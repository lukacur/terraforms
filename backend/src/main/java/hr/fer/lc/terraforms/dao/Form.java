package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "form")
public class Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "formid", nullable = false)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner")
    private AppUser owner;

    @Column(name = "title", nullable = false, length = 128)
    private String title;

    @Column(name = "tscreated", insertable = false, updatable = false)
    private LocalDateTime tscreated;

    @Column(name = "description", nullable = false, length = 512)
    private String description;

    @Column(name = "viewlink", nullable = false, length = 256)
    private String viewlink;

    @Column(name = "statslink", nullable = false, length = 256)
    private String statslink;

    @Column(name = "tsmodified")
    private LocalDateTime tsmodified;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "restrictedto", nullable = false)
    private RestrictionType restrictedto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usermodified")
    private AppUser usermodified;

    @OneToMany(mappedBy = "formchanged")
    private Set<FormChange> formchanges = new LinkedHashSet<>();

    @OneToMany(mappedBy = "formid")
    private Set<Question> questions = new LinkedHashSet<>();

    @OneToMany(mappedBy = "formid")
    private Set<AccessRequest> accessrequests = new LinkedHashSet<>();

    public Set<AccessRequest> getAccessrequests() {
        return accessrequests;
    }

    public void setAccessrequests(Set<AccessRequest> accessrequests) {
        this.accessrequests = accessrequests;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public Set<FormChange> getFormchanges() {
        return formchanges;
    }

    public void setFormchanges(Set<FormChange> formchanges) {
        this.formchanges = formchanges;
    }

    public AppUser getUsermodified() {
        return usermodified;
    }

    public void setUsermodified(AppUser usermodified) {
        this.usermodified = usermodified;
    }

    public RestrictionType getRestrictedto() {
        return restrictedto;
    }

    public void setRestrictedto(RestrictionType restrictedto) {
        this.restrictedto = restrictedto;
    }

    public LocalDateTime getTsmodified() {
        return tsmodified;
    }

    public void setTsmodified(LocalDateTime tsmodified) {
        this.tsmodified = tsmodified;
    }

    public String getStatslink() {
        return statslink;
    }

    public void setStatslink(String statslink) {
        this.statslink = statslink;
    }

    public String getViewlink() {
        return viewlink;
    }

    public void setViewlink(String viewlink) {
        this.viewlink = viewlink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getTscreated() {
        return tscreated;
    }

    public void setTscreated(LocalDateTime tscreated) {
        this.tscreated = tscreated;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AppUser getOwner() {
        return owner;
    }

    public void setOwner(AppUser owner) {
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}