package hr.fer.lc.terraforms.dto;

import java.util.List;

public class FormBodyDTO {
    private Long parentFormId;
    private List<QuestionDTO> questions;

    public FormBodyDTO() {}

    public FormBodyDTO(Long parentFormId, List<QuestionDTO> questions) {
        this.parentFormId = parentFormId;
        this.questions = questions;
    }

    public Long getParentFormId() {
        return parentFormId;
    }

    public void setParentFormId(Long parentFormId) {
        this.parentFormId = parentFormId;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }
}
