package hr.fer.lc.terraforms.configuration;

/**
 * @author Luka Ćurić
 */
public class ConfigVars {
    public static final String STATIC_CONTENT_ROOT = "/static";
    public static final String REST_API_ROOT = "/api";
}
