package hr.fer.lc.terraforms.configuration.web;

import hr.fer.lc.terraforms.configuration.ConfigVars;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.resource.PathResourceResolver;

/**
 * @author Luka Ćurić
 */
@Configuration
public class WebConfiguration extends WebMvcConfigurationSupport {
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**/*.css", "/**/*.html", "/**/*.js", "/**/*.json", "/**/*.ico",
                        "/**/*.png", "/**/*.txt", "/**/*.map")
                .setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/");

        registry.addResourceHandler("", "/", "/**")
                .setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/index.html")
                .resourceChain(true)
                .addResolver(new PathResourceResolver() {
                    @Override
                    public Resource getResource(String resourcePath, Resource location) {
                        if (resourcePath.startsWith(ConfigVars.REST_API_ROOT)
                                || resourcePath.startsWith(ConfigVars.REST_API_ROOT.substring(1))) {
                            return null;
                        }

                        return location.exists() && location.isReadable() ? location : null;
                    }
                });
    }
}
