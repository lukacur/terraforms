package hr.fer.lc.terraforms.services.implementations;

import hr.fer.lc.terraforms.dao.*;
import hr.fer.lc.terraforms.dao.enums.FormRestrictionType;
import hr.fer.lc.terraforms.dao.enums.QuestionAnswerType;
import hr.fer.lc.terraforms.dto.*;
import hr.fer.lc.terraforms.exceptions.action.IllegalUserActionException;
import hr.fer.lc.terraforms.exceptions.action.UnauthorizedUserActionException;
import hr.fer.lc.terraforms.exceptions.data.EntryDoesNotExistException;
import hr.fer.lc.terraforms.repositories.AppUserRepository;
import hr.fer.lc.terraforms.repositories.FormRepository;
import hr.fer.lc.terraforms.repositories.PossibleAnswerRepository;
import hr.fer.lc.terraforms.services.AuthorityService;
import hr.fer.lc.terraforms.services.FormService;
import hr.fer.lc.terraforms.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Luka Ćurić
 */
@Service
@Transactional
public class FormServiceImpl implements FormService {
    private AppUserRepository appUserRepository;
    @Autowired
    public void setAppUserRepository(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    private FormRepository formRepository;
    @Autowired
    public void setFormRepository(FormRepository formRepository) { this.formRepository = formRepository; }

    private QuestionService questionService;
    @Autowired
    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    private AuthorityService authorityService;
    @Autowired
    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    private PossibleAnswerRepository possibleAnswerRepository;
    @Autowired
    public void setPossibleAnswerRepository(PossibleAnswerRepository possibleAnswerRepository) {
        this.possibleAnswerRepository = possibleAnswerRepository;
    }

    @Override
    public Form createForm(FormDTO formData, AppUser owner) {
        SecureRandom secRand = new SecureRandom();

        byte[] viewLinkBytes = new byte[64];
        secRand.nextBytes(viewLinkBytes);
        byte[] statsLinkBytes = new byte[64];
        secRand.nextBytes(statsLinkBytes);

        Base64.Encoder b64e = Base64.getUrlEncoder();

        Form form = new Form();
        {
            form.setOwner(owner);
            form.setTitle(formData.getTitle());
            form.setDescription(formData.getDescription());
            form.setViewlink(b64e.encodeToString(viewLinkBytes));
            form.setStatslink(b64e.encodeToString(statsLinkBytes));
            form.setRestrictedto(FormRestrictionType.withValue(formData.getRestrictedTo()).getRestType());
        }

        formRepository.save(form);

        return form;
    }

    @Override
    @Transactional(readOnly = true)
    public FormFullDTO responseForLink(String viewLink, AppUser userRequesting) {
        Form form = formRepository.getByViewlink(viewLink)
                .orElseThrow(() -> new IllegalArgumentException("No form with given link."));

        if (!authorityService.hasReadAuthority(userRequesting, form)
                && !authorityService.hasEditAuthority(userRequesting, form))
            throw new UnauthorizedUserActionException("User not allowed to view form.");

        FormDTO formData = new FormDTO(
                form.getId(),
                form.getTitle(),
                form.getDescription(),
                form.getRestrictedto().getId()
        );

        List<QuestionDTO> formQuestions = formRepository.getCurrentQuestionsFor(form.getId()).stream()
                .filter(Objects::nonNull)
                .filter(q -> q.getQuestionnumber() >= 0)
                .map(q -> {
                    QuestionDTO questionDTO = new QuestionDTO();
                    {
                        questionDTO.setQuestion(q.getQuestion());
                        questionDTO.setQuestionId(q.getId());
                        questionDTO.setQuestionPage(q.getPage());
                        questionDTO.setQuestionNumber(q.getQuestionnumber());
                        questionDTO.setIsRequired(q.getRequired());
                        questionDTO.setAnswerType(q.getAnswtypeid().getId());

                        Question prevVersion = q.getPreviousversion();
                        questionDTO.setPreviousVersion((prevVersion == null) ? null : prevVersion.getId());

                        List<PossibleAnswerDTO> possibleAnswers = q.getPossibleanswers().stream()
                                .map(pa -> {
                                    PossibleAnswerDTO answerDTO = new PossibleAnswerDTO();
                                    {
                                        answerDTO.setName(pa.getName());
                                        answerDTO.setValue(pa.getValue());
                                    }

                                    return answerDTO;
                                })
                                .collect(Collectors.toList());

                        questionDTO.setPossibleAnswers(possibleAnswers);
                    }

                    return questionDTO;
                })
                .collect(Collectors.toList());

        FormBodyDTO formBody = new FormBodyDTO(
                form.getId(),
                formQuestions
        );

        return new FormFullDTO(formData, formBody);
    }

    @Override
    public Optional<Form> getFormById(Long formId) {
        return formRepository.findById(formId);
    }

    @Override
    public Form getFormByLink(String formLink) {
        return formRepository.getByViewlink(formLink)
                .orElseThrow(() -> new EntryDoesNotExistException("Form doesn't exist."));
    }

    @Override
    public Form getFormByStatsLink(String statsLink) {
        return formRepository.getByStatslink(statsLink)
                .orElseThrow(() -> new EntryDoesNotExistException("Form doesn't exist."));
    }

    @Override
    @Transactional(readOnly = true)
    public List<AccessRequest> getPendingRequestsFor(FormDTO formInfo, AppUser userFetching) {
        Form form = formRepository.findById(formInfo.getFormId())
                .orElseThrow(() -> new EntryDoesNotExistException("Form doesn't exist"));

        if (!form.getOwner().getId().equals(userFetching.getId()))
            throw new UnauthorizedUserActionException("User is not authorized to view form access requests.");

        return form.getAccessrequests().stream()
                .filter(accReq -> accReq.getGrantedOn() == null)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<AccessRequest> getActivePermissionsFor(FormDTO formInfo, AppUser userFetching) {
        Form form = formRepository.findById(formInfo.getFormId())
                .orElseThrow(() -> new EntryDoesNotExistException("Form doesn't exist"));

        if (!form.getOwner().getId().equals(userFetching.getId()))
            throw new UnauthorizedUserActionException("User is not authorized to view form access requests.");

        return form.getAccessrequests().stream()
                .filter(accReq -> accReq.getGrantedOn() != null)
                .collect(Collectors.toList());
    }

    @Override
    public void updateFormMetadata(FormDTO formData, AppUser userChanging) {
        Form form = formRepository.getById(formData.getFormId());

        if (!authorityService.hasEditAuthority(userChanging, form))
            throw new UnauthorizedUserActionException("User not allowed to edit form.");

        form.setDescription(formData.getDescription());
        form.setRestrictedto(FormRestrictionType.withValue(formData.getRestrictedTo()).getRestType());
        form.setTitle(formData.getTitle());
        form.setUsermodified(userChanging);

        formRepository.save(form);
    }

    @Override
    public void deleteForm(FormDTO formData, AppUser userChanging) {
        Form form = formRepository.findById(formData.getFormId()).orElse(null);

        if (form != null) {
            if (form.getOwner().getId().equals(userChanging.getId())) {
                formRepository.delete(form);
            } else {
                throw new IllegalUserActionException("User attempted to delete form they don't own.");
            }
        } else {
            throw new IllegalUserActionException("User attempted to delete non-existant form.");
        }
    }

    private static void validateQuestion(QuestionDTO question) {
        if (!QuestionAnswerType.getSettable().contains(QuestionAnswerType.withValue(question.getAnswerType()))
                && (question.getPossibleAnswers() != null && !question.getPossibleAnswers().isEmpty()))
            throw new IllegalArgumentException("Question can't accept preset answers");
    }

    @Override
    @Transactional(rollbackFor = { IllegalArgumentException.class, UnauthorizedUserActionException.class })
    public void saveInto(FormBodyDTO bodyData, AppUser userChanging) {
        Form form = formRepository.getById(bodyData.getParentFormId());

        Set<Long> formQuestionIds = formRepository.getCurrentQuestionsFor(form.getId()).stream()
                .filter(Objects::nonNull)
                .map(Question::getId)
                .collect(Collectors.toSet());

        if (!bodyData.getQuestions().stream().map(QuestionDTO::getQuestionId).allMatch(formQuestionIds::contains)
                || bodyData.getQuestions().size() != formQuestionIds.size())
            throw new IllegalUserActionException("Question deletion not allowed while in the 'saveInto' mode.");

        if (!authorityService.hasEditAuthority(userChanging, form))
            throw new UnauthorizedUserActionException("User not allowed to edit form.");

        for (QuestionDTO question : bodyData.getQuestions()) {
            validateQuestion(question);

            if (question.getQuestionId() == null)
                throw new IllegalArgumentException("Question versioning incorrect. ");

            Question qst;

            qst = questionService.getQuestionById(question.getQuestionId());

            qst.getPossibleanswers().stream()
                    .filter(pa -> question.getPossibleAnswers().stream()
                            .map(PossibleAnswerDTO::getName)
                            .noneMatch(answName -> pa.getName().equals(answName))
                    )
                    .forEach(pa -> possibleAnswerRepository.delete(pa));

            if (!qst.getAnswtypeid().getId().equals(question.getAnswerType()))
                throw new IllegalArgumentException("New question creation not allowed while in the 'saveInto' mode.");

            if (!qst.getFormid().getId().equals(bodyData.getParentFormId()))
                throw new IllegalArgumentException("Question doesn't belong to the form specified.");

            qst.setUsermodified(userChanging);

            qst.setQuestionnumber(question.getQuestionNumber());
            qst.setPage(question.getQuestionPage());
            qst.setRequired(question.getIsRequired());
            qst.setQuestion(question.getQuestion());
            qst.setAnswtypeid(QuestionAnswerType.withValue(question.getAnswerType()).getAnswType());

            questionService.saveOrUpdateQuestion(qst);

            for (PossibleAnswerDTO possibleAnswer : question.getPossibleAnswers()) {
                PossibleAnswer answer;

                answer = possibleAnswerRepository.getByQuestionidAndName(qst, possibleAnswer.getName())
                        .orElse(null);

                if (answer == null) {
                    answer = new PossibleAnswer();
                    answer.setUsercreated(userChanging);
                } else {
                    answer.setUsermodified(userChanging);
                    answer.setTsmodified(LocalDateTime.now());
                }

                answer.setQuestionid(qst);
                answer.setName(possibleAnswer.getName());
                answer.setValue(possibleAnswer.getValue());

                possibleAnswerRepository.save(answer);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = { UnauthorizedUserActionException.class, IllegalUserActionException.class,
            IllegalArgumentException.class })
    public FormBodyDTO saveOver(FormBodyDTO bodyData, AppUser userChanging) {
        Form form = formRepository.getById(bodyData.getParentFormId());

        Set<Long> keepIds = bodyData.getQuestions().stream()
                        .map(QuestionDTO::getQuestionId)
                        .collect(Collectors.toSet());

        formRepository.getCurrentQuestionsFor(form.getId()).stream()
                .filter(q -> q != null && !keepIds.contains(q.getId()))
                .forEach(qst -> questionService.deleteQuestion(qst));

        if (!authorityService.hasEditAuthority(userChanging, form))
            throw new UnauthorizedUserActionException("User not allowed to edit form.");

        for (QuestionDTO question : bodyData.getQuestions()) {
            validateQuestion(question);

            Question qst = new Question();
            qst.setFormid(form);

            if (question.getQuestionId() != null) {
                Question prevQuestion = questionService.getQuestionById(question.getQuestionId());

                if (!prevQuestion.getFormid().getId().equals(form.getId()))
                    throw new IllegalUserActionException("Question doesn't belong to the form specified.");

                qst.setPreviousversion(prevQuestion);
                qst.setUsermodified(userChanging);

                prevQuestion.setQuestionnumber(-prevQuestion.getQuestionnumber());
                prevQuestion.setUsermodified(userChanging);
                questionService.saveOrUpdateQuestion(prevQuestion);
            }

            qst.setQuestionnumber(question.getQuestionNumber());
            qst.setPage(question.getQuestionPage());
            qst.setRequired(question.getIsRequired());
            qst.setAnswtypeid(QuestionAnswerType.withValue(question.getAnswerType()).getAnswType());
            qst.setQuestion(question.getQuestion());
            qst.setUsercreated(userChanging);

            questionService.saveOrUpdateQuestion(qst);
            question.setQuestionId(qst.getId());

            for (PossibleAnswerDTO possibleAnswer : question.getPossibleAnswers()) {
                PossibleAnswer answer = new PossibleAnswer();

                answer.setUsercreated(userChanging);
                answer.setName(possibleAnswer.getName());
                answer.setValue(possibleAnswer.getValue());
                answer.setQuestionid(qst);

                possibleAnswerRepository.save(answer);
            }
        }

        return bodyData;
    }

    @Override
    @Transactional(readOnly = true)
    public List<FormFullDTO> getOwnedFormsFor(AppUser user) {
        AppUser tmpUser = appUserRepository.findById(user.getId()).orElse(null);

        if (tmpUser == null)
            return null;

        var owned = tmpUser.getFormsOwned();

        return owned.stream()
                .map(form -> {
                    FormFullDTO dto = new FormFullDTO();
                    {
                        FormDTO formMeta = new FormDTO();
                        formMeta.setFormId(form.getId());
                        formMeta.setDescription(form.getDescription());
                        formMeta.setTitle(form.getTitle());
                        formMeta.setRestrictedTo(form.getRestrictedto().getId());
                        formMeta.setFormLink(form.getViewlink());
                        formMeta.setStatsLink(form.getStatslink());

                        FormBodyDTO formBody = new FormBodyDTO();
                        formBody.setParentFormId(form.getId());
                        formBody.setQuestions(new ArrayList<>());

                        List<Question> newestQuestions = formRepository.getCurrentQuestionsFor(form.getId()).stream()
                                .filter(Objects::nonNull)
                                .filter(q -> q.getQuestionnumber() > 0)
                                .collect(Collectors.toList());

                        for (Question question : newestQuestions) {
                            QuestionDTO qDto = new QuestionDTO();
                            {
                                qDto.setQuestionId(question.getId());
                                if (question.getPreviousversion() != null)
                                    qDto.setPreviousVersion(question.getPreviousversion().getId());
                                qDto.setQuestionNumber(question.getQuestionnumber());
                                qDto.setQuestionPage(question.getPage());
                                qDto.setIsRequired(question.getRequired());
                                qDto.setAnswerType(question.getAnswtypeid().getId());
                                qDto.setQuestion(question.getQuestion());
                                qDto.setPossibleAnswers(new ArrayList<>());
                            }

                            for (PossibleAnswer possibleAnswer : question.getPossibleanswers()) {
                                PossibleAnswerDTO posAnswDto = new PossibleAnswerDTO();
                                {
                                    posAnswDto.setName(possibleAnswer.getName());
                                    posAnswDto.setValue(possibleAnswer.getValue());
                                }

                                qDto.getPossibleAnswers().add(posAnswDto);
                            }

                            formBody.getQuestions().add(qDto);
                        }

                        dto.setFormData(formMeta);
                        dto.setBodyData(formBody);
                    }

                    return dto;
                })
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<FormFullDTO> getSharedWithUser(AppUser user) {
        AppUser tmpUser = appUserRepository.findById(user.getId()).orElse(null);

        if (tmpUser == null)
            return null;

        return formRepository.findAll().stream()
                .filter(form -> authorityService.hasEditAuthority(tmpUser, form) &&
                        !form.getOwner().getId().equals(user.getId())
                )
                .map(form -> {
                    FormFullDTO dto = new FormFullDTO();
                    {
                        FormDTO formMeta = new FormDTO();
                        formMeta.setFormId(form.getId());
                        formMeta.setDescription(form.getDescription());
                        formMeta.setTitle(form.getTitle());
                        formMeta.setRestrictedTo(form.getRestrictedto().getId());
                        formMeta.setFormLink(form.getViewlink());
                        if (authorityService.hasStatisticsAuthority(tmpUser, form))
                            formMeta.setStatsLink(form.getStatslink());

                        FormBodyDTO formBody = new FormBodyDTO();
                        formBody.setParentFormId(form.getId());
                        formBody.setQuestions(new ArrayList<>());

                        List<Question> newestQuestions = formRepository.getCurrentQuestionsFor(form.getId())
                                .stream().filter(Objects::nonNull)
                                .collect(Collectors.toList());

                        for (Question question : newestQuestions) {
                            QuestionDTO qDto = new QuestionDTO();
                            {
                                qDto.setQuestionId(question.getId());
                                if (question.getPreviousversion() != null)
                                    qDto.setPreviousVersion(question.getPreviousversion().getId());
                                qDto.setQuestionNumber(question.getQuestionnumber());
                                qDto.setQuestionPage(question.getPage());
                                qDto.setIsRequired(question.getRequired());
                                qDto.setAnswerType(question.getAnswtypeid().getId());
                                qDto.setQuestion(question.getQuestion());
                                qDto.setPossibleAnswers(new ArrayList<>());
                            }

                            for (PossibleAnswer possibleAnswer : question.getPossibleanswers()) {
                                PossibleAnswerDTO posAnswDto = new PossibleAnswerDTO();
                                {
                                    posAnswDto.setName(possibleAnswer.getName());
                                    posAnswDto.setValue(possibleAnswer.getValue());
                                }

                                qDto.getPossibleAnswers().add(posAnswDto);
                            }

                            formBody.getQuestions().add(qDto);
                        }

                        dto.setFormData(formMeta);
                        dto.setBodyData(formBody);
                    }

                    return dto;
                })
                .collect(Collectors.toList());
    }
}
