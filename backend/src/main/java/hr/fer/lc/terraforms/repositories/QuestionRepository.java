package hr.fer.lc.terraforms.repositories;

import hr.fer.lc.terraforms.dao.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Long> {

}
