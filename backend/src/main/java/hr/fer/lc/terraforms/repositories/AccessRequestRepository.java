package hr.fer.lc.terraforms.repositories;

import hr.fer.lc.terraforms.dao.AccessRequest;
import hr.fer.lc.terraforms.dao.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface AccessRequestRepository extends JpaRepository<AccessRequest, Long> {
    Set<AccessRequest> findByUserrequested(AppUser userRequested);
}
