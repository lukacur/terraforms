package hr.fer.lc.terraforms.repositories;

import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dao.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * @author Luka Ćurić
 */
public interface FormRepository extends JpaRepository<Form, Long> {
    List<Form> getByOwner(AppUser owner);
    Optional<Form> getByViewlink(String viewlink);
    Optional<Form> getByStatslink(String statsLink);

    @Query(value =
        """
        SELECT q
        FROM Form f
            LEFT JOIN Question q
                ON q.formid = f
        WHERE NOT EXISTS (
            SELECT qst.previousversion
            FROM Question qst
            WHERE qst.previousversion = q
        ) AND
        f.id = ?1
        """
    )
    List<Question> getCurrentQuestionsFor(Long formId);
}
