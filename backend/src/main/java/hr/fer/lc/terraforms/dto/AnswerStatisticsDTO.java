package hr.fer.lc.terraforms.dto;

public class AnswerStatisticsDTO {
    private long answerCount;
    private double answerPercentage;

    public long getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(long answerCount) {
        this.answerCount = answerCount;
    }

    public double getAnswerPercentage() {
        return answerPercentage;
    }

    public void setAnswerPercentage(double answerPercentage) {
        this.answerPercentage = answerPercentage;
    }
}
