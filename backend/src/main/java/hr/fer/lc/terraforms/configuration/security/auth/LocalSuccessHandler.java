package hr.fer.lc.terraforms.configuration.security.auth;

import hr.fer.lc.terraforms.constants.SessionAttribute;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.services.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LocalSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    private AppUserService userService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        UserDetails details = (UserDetails) authentication.getPrincipal();
        AppUser user = userService.getUserByEmail(details.getUsername()).orElse(null);

        request.getSession().setAttribute(SessionAttribute.CURRENT_USER, user);
        System.out.println("User " + details.getUsername() + " logged in");

        response.setStatus(HttpStatus.OK.value());
        response.getWriter().close();
    }
}
