package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "formchange")
public class FormChange {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "changeid", nullable = false)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userresponsible")
    private AppUser userresponsible;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "formchanged")
    private Form formchanged;

    @Column(name = "oldtitle", nullable = false, length = 128)
    private String oldtitle;

    @Column(name = "newtitle", nullable = false, length = 128)
    private String newtitle;

    @Column(name = "olddescription", nullable = false, length = 512)
    private String olddescription;

    @Column(name = "newdescription", nullable = false, length = 512)
    private String newdescription;

    @Column(name = "oldrestrictedto", nullable = false)
    private Integer oldrestrictedto;

    @Column(name = "newrestrictedto", nullable = false)
    private Integer newrestrictedto;

    @Column(name = "tschange", nullable = false)
    private LocalDateTime tschange;

    public LocalDateTime getTschange() {
        return tschange;
    }

    public void setTschange(LocalDateTime tschange) {
        this.tschange = tschange;
    }

    public Integer getNewrestrictedto() {
        return newrestrictedto;
    }

    public void setNewrestrictedto(Integer newrestrictedto) {
        this.newrestrictedto = newrestrictedto;
    }

    public Integer getOldrestrictedto() {
        return oldrestrictedto;
    }

    public void setOldrestrictedto(Integer oldrestrictedto) {
        this.oldrestrictedto = oldrestrictedto;
    }

    public String getNewdescription() {
        return newdescription;
    }

    public void setNewdescription(String newdescription) {
        this.newdescription = newdescription;
    }

    public String getOlddescription() {
        return olddescription;
    }

    public void setOlddescription(String olddescription) {
        this.olddescription = olddescription;
    }

    public String getNewtitle() {
        return newtitle;
    }

    public void setNewtitle(String newtitle) {
        this.newtitle = newtitle;
    }

    public String getOldtitle() {
        return oldtitle;
    }

    public void setOldtitle(String oldtitle) {
        this.oldtitle = oldtitle;
    }

    public Form getFormchanged() {
        return formchanged;
    }

    public void setFormchanged(Form formchanged) {
        this.formchanged = formchanged;
    }

    public AppUser getUserresponsible() {
        return userresponsible;
    }

    public void setUserresponsible(AppUser userresponsible) {
        this.userresponsible = userresponsible;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}