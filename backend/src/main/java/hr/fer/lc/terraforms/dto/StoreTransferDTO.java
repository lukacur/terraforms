package hr.fer.lc.terraforms.dto;

/**
 * @author Luka Ćurić
 */
public class StoreTransferDTO {
    private String itemKey;
    private String itemValue;

    public String getItemKey() {
        return itemKey;
    }

    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }
}
