package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "appuser")
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "uid", nullable = false)
    private Long id;

    @Column(name = "username", nullable = false, length = 64)
    private String username;

    @Column(name = "password", length = 512)
    private String password;

    @Column(name = "email", nullable = false, length = 320)
    private String email;

    @Column(name = "tscreated", insertable = false, updatable = false)
    private LocalDateTime tscreated;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "loginprovider", nullable = false)
    private LoginProvider loginprovider;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "status", nullable = false)
    private Status status;

    @Column(name = "expirydate")
    private LocalDate expirydate;

    @Column(name = "tsmodified")
    private LocalDateTime tsmodified;

    @OneToMany(mappedBy = "usercreated")
    private Set<Answer> answersCreated = new LinkedHashSet<>();

    @OneToMany(mappedBy = "usermodified")
    private Set<Answer> answersModified = new LinkedHashSet<>();

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private Set<Form> formsOwned = new LinkedHashSet<>();

    @OneToMany(mappedBy = "usermodified")
    private Set<Form> formsModified = new LinkedHashSet<>();

    @OneToMany(mappedBy = "userresponsible")
    private Set<FormChange> formsChanged = new LinkedHashSet<>();

    @OneToMany(mappedBy = "userresponsible")
    private Set<QuestionChange> questionsChanged = new LinkedHashSet<>();

    @OneToMany(mappedBy = "usercreated")
    private Set<Question> questionsCreated = new LinkedHashSet<>();

    @OneToMany(mappedBy = "usermodified")
    private Set<Question> questionsModified = new LinkedHashSet<>();

    @OneToMany(mappedBy = "usercreated")
    private Set<PossibleAnswer> possibleanswersCreated = new LinkedHashSet<>();

    @OneToMany(mappedBy = "usermodified")
    private Set<PossibleAnswer> possibleanswersModified = new LinkedHashSet<>();

    @OneToMany(mappedBy = "userrequested", fetch = FetchType.LAZY)
    private Set<AccessRequest> accessRequests = new LinkedHashSet<>();

    public Set<AccessRequest> getAccessRequests() {
        return accessRequests;
    }

    public void setAccessrequests(Set<AccessRequest> accessRequests) {
        this.accessRequests = accessRequests;
    }

    public Set<PossibleAnswer> getPossibleanswersModified() {
        return possibleanswersModified;
    }

    public void setPossibleanswersModified(Set<PossibleAnswer> possibleanswersModified) {
        this.possibleanswersModified = possibleanswersModified;
    }

    public Set<PossibleAnswer> getPossibleanswersCreated() {
        return possibleanswersCreated;
    }

    public void setPossibleanswersCreated(Set<PossibleAnswer> possibleanswersCreated) {
        this.possibleanswersCreated = possibleanswersCreated;
    }

    public Set<Question> getQuestionsModified() {
        return questionsModified;
    }

    public void setQuestionsModified(Set<Question> questionsModified) {
        this.questionsModified = questionsModified;
    }

    public Set<Question> getQuestionsCreated() {
        return questionsCreated;
    }

    public void setQuestionsCreated(Set<Question> questionsCreated) {
        this.questionsCreated = questionsCreated;
    }

    public Set<QuestionChange> getQuestionsChanged() {
        return questionsChanged;
    }

    public void setQuestionsChanged(Set<QuestionChange> questionsChanged) {
        this.questionsChanged = questionsChanged;
    }

    public Set<FormChange> getFormsChanged() {
        return formsChanged;
    }

    public void setFormsChanged(Set<FormChange> formsChanged) {
        this.formsChanged = formsChanged;
    }

    public Set<Form> getFormsModified() {
        return formsModified;
    }

    public void setFormsModified(Set<Form> formsModified) {
        this.formsModified = formsModified;
    }

    public Set<Form> getFormsOwned() {
        return formsOwned;
    }

    public void setFormsOwned(Set<Form> formsOwned) {
        this.formsOwned = formsOwned;
    }

    public Set<Answer> getAnswersModified() {
        return answersModified;
    }

    public void setAnswersModified(Set<Answer> answersModified) {
        this.answersModified = answersModified;
    }

    public Set<Answer> getAnswersCreated() {
        return answersCreated;
    }

    public void setAnswersCreated(Set<Answer> answersCreated) {
        this.answersCreated = answersCreated;
    }

    public LocalDateTime getTsmodified() {
        return tsmodified;
    }

    public void setTsmodified(LocalDateTime tsmodified) {
        this.tsmodified = tsmodified;
    }

    public LocalDate getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(LocalDate expirydate) {
        this.expirydate = expirydate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LoginProvider getLoginprovider() {
        return loginprovider;
    }

    public void setLoginprovider(LoginProvider loginprovider) {
        this.loginprovider = loginprovider;
    }

    public LocalDateTime getTscreated() {
        return tscreated;
    }

    public void setTscreated(LocalDateTime tscreated) {
        this.tscreated = tscreated;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppUser appUser = (AppUser) o;
        return id.equals(appUser.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}