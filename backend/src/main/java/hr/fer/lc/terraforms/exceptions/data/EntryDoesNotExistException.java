package hr.fer.lc.terraforms.exceptions.data;

public class EntryDoesNotExistException extends RuntimeException {
    public EntryDoesNotExistException() {
    }

    public EntryDoesNotExistException(String message) {
        super(message);
    }
}
