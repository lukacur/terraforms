package hr.fer.lc.terraforms.services.implementations;

import hr.fer.lc.terraforms.dao.*;
import hr.fer.lc.terraforms.dao.enums.QuestionAnswerType;
import hr.fer.lc.terraforms.dto.AnswerStatisticsDTO;
import hr.fer.lc.terraforms.dto.FormStatisticsDTO;
import hr.fer.lc.terraforms.dto.StatisticsEntryDTO;
import hr.fer.lc.terraforms.exceptions.action.UnauthorizedUserActionException;
import hr.fer.lc.terraforms.exceptions.data.EntryDoesNotExistException;
import hr.fer.lc.terraforms.repositories.FormRepository;
import hr.fer.lc.terraforms.services.AuthorityService;
import hr.fer.lc.terraforms.services.FormService;
import hr.fer.lc.terraforms.services.StatisticsService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Luka Ćurić
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {
    private AuthorityService authorityService;
    @Autowired
    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    private FormService formService;
    @Autowired
    public void setFormService(FormService formService) {
        this.formService = formService;
    }

    private FormRepository formRepository;
    @Autowired
    public void setFormRepository(FormRepository formRepository) {
        this.formRepository = formRepository;
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = { UnauthorizedUserActionException.class })
    public FormStatisticsDTO buildStatistics(Long formId, AppUser userRequesting) {
        Form form = formService.getFormById(formId)
                .orElseThrow(() -> new EntryDoesNotExistException("Form doesn't exist."));

        if (!authorityService.hasStatisticsAuthority(userRequesting, form))
            throw new UnauthorizedUserActionException("User doesn't have statistics access");

        List<StatisticsEntryDTO> statistics = new LinkedList<>();

        List<Question> formQuestions = formRepository.getCurrentQuestionsFor(form.getId()).stream()
                .filter(Objects::nonNull)
                .sorted(Comparator.comparing(Question::getQuestionnumber))
                .collect(Collectors.toList());

        FormStatisticsDTO formStatistics = new FormStatisticsDTO();
        formStatistics.setFormTitle(form.getTitle());
        formStatistics.setFormDescription(form.getDescription());

        for (Question q : formQuestions) {
            StatisticsEntryDTO statEntry = new StatisticsEntryDTO();
            statEntry.setQuestion(q.getQuestion());
            statEntry.setAnswerType(q.getAnswtypeid().getId());
            QuestionAnswerType answerType = QuestionAnswerType.withValue(q.getAnswtypeid().getId());

            List<Answer> answerList = q.getAnswers().stream().filter(Objects::nonNull).collect(Collectors.toList());
            List<Answer> filledAnswers = answerList.stream()
                    .filter(answ -> !answ.getValue().equals(""))
                    .collect(Collectors.toList());
            List<PossibleAnswer> possibleAnswers = q.getPossibleanswers().stream()
                    .filter(Objects::nonNull)
                    .sorted(Comparator.comparing(PossibleAnswer::getValue))
                    .collect(Collectors.toList());

            int totalAnswers = answerList.size();
            int answered = filledAnswers.size();

            if (answerType == QuestionAnswerType.TRUEFALSE) {
                PossibleAnswer trueAnswer = new PossibleAnswer();
                trueAnswer.setValue("true");
                PossibleAnswer falseAnswer = new PossibleAnswer();
                falseAnswer.setValue("false");

                possibleAnswers = List.of(trueAnswer, falseAnswer);
            }

            if (!possibleAnswers.isEmpty()) {
                Map<String, AnswerStatisticsDTO> answerToPartMap = new HashMap<>();
                statEntry.setAnswerToPartMap(answerToPartMap);

                for (PossibleAnswer possibleAnswer : possibleAnswers) {
                    AnswerStatisticsDTO answerStatistics = new AnswerStatisticsDTO();
                    String answerName = possibleAnswer.getName();

                    if (answerType == QuestionAnswerType.TRUEFALSE)
                        answerName = possibleAnswer.getValue().equals("true") ? "True" : "False";

                    long answerCount = filledAnswers.stream()
                            .filter(fa -> {
                                if (answerType != QuestionAnswerType.CHECKBOX)
                                    return fa.getValue().equals(possibleAnswer.getValue());
                                else
                                    return List.of(fa.getValue().split(";"))
                                            .contains(possibleAnswer.getValue());
                            })
                            .count();

                    double answerPart = (answered == 0) ? 0. : ((double) answerCount / answered);

                    answerStatistics.setAnswerCount(answerCount);
                    answerStatistics.setAnswerPercentage(answerPart);

                    answerToPartMap.put(answerName, answerStatistics);
                }
            } else {
                statEntry.setAnswers(new LinkedList<>());

                q.getAnswers().stream()
                        .map(Answer::getValue)
                        .forEach(answer -> statEntry.getAnswers().add(answer));
            }

            statEntry.setTotalAnswers(totalAnswers);
            statEntry.setAnswered(answered);
            statistics.add(statEntry);
        }

        formStatistics.setStatistics(statistics);

        return formStatistics;
    }

    @Override
    public byte[] getPDFStatistics(FormStatisticsDTO formStatistics) {
        List<StatisticsEntryDTO> statistics = formStatistics.getStatistics();

        final String questionTableHtml =
            """
            <table>
                <thead>
                    <th colspan="2">%s</th>
                </thead>
                <tbody>
                    %s
                </tbody>
            </table>
            <br/>
            <br/>
            """;

        final String answerMultichoiceTemplate =
            """
            <tr>
                <td>%s</td>
                <td>%s</td>
            </tr>
            
            """;

        final String answerSingleTemplate =
            """
            <tr>
                <td colspan="2">%s</td>
            </tr>
            
            """;

        StringBuilder questionTables = new StringBuilder();
        for (StatisticsEntryDTO entry : statistics) {
            StringBuilder questionInfoHtml = new StringBuilder();

            int totalAnswers = entry.getTotalAnswers();
            int answered = entry.getAnswered();

            if (entry.getAnswers() == null && entry.getAnswerToPartMap() == null) {
                questionInfoHtml.append(String.format(answerSingleTemplate, "No answers for this question"));
            } else if (entry.getAnswerToPartMap() != null) {
                for (var answerEntry : entry.getAnswerToPartMap().entrySet()) {
                    String answerName = answerEntry.getKey();
                    long answerCount = answerEntry.getValue().getAnswerCount();
                    double percentage = answerEntry.getValue().getAnswerPercentage();

                    String answerTitle = String.format("%s (%d)", answerName, answerCount);
                    String answerPercentage = String.format(Locale.ROOT, "%.2f%%", percentage * 100.);
                    questionInfoHtml.append(String.format(answerMultichoiceTemplate, answerTitle, answerPercentage));
                }
            } else {
                for (String answer : entry.getAnswers())
                    questionInfoHtml.append(String.format(answerSingleTemplate, answer));
            }

            questionInfoHtml.append(String.format(answerSingleTemplate, "Overall statistics"));
            questionInfoHtml.append(String.format(answerMultichoiceTemplate, "Answered", answered));
            questionInfoHtml.append(String.format(answerMultichoiceTemplate, "Unanswered", totalAnswers - answered));
            double answeredPart = (double) answered / totalAnswers;
            String answeredPercentage = String.format(Locale.ROOT, "%% answered: %.2f%%", answeredPart * 100.);
            questionInfoHtml.append(String.format(answerSingleTemplate, answeredPercentage));

            questionTables.append(String.format(questionTableHtml, entry.getQuestion(), questionInfoHtml));
        }

        String html =
           """
             <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8"/>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Terraforms - statistics report</title>
                <style>
                    body {
                        font-family: "Arial Unicode MS", Arial, Helvetica, sans-serif;
                    }
                    
                    table {
                        width: 100%%;
                        text-align: center;
                        border: 1px solid black;
                        font-size: 0.75rem;
                        border-collapse: collapse;
                        margin-bottom: 2rem;
                    }
            
                    h1 {
                        font: bold;
                    }
            
                    .container{
                        position: relative;
                        width: 100%%;
                    }
            
                    .child {
                        width: 100%%;
                        text-align: center;
                    }
            
                    hr {
                        border: 1px solid black;
                        width: 80%%; margin: 4rem;
                    }
            
                    table * {
                        border: 1px solid black;
                    }
                </style>
            </head>
            <body>
                <div>
                    <h1 class="child"> Statistics report for "%s"</h1>
            
                    <hr class="" />
            
                    %s
                </div>
            </body>
            </html>
            """;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ByteArrayInputStream bais = new ByteArrayInputStream(
                String.format(html, formStatistics.getFormTitle(), questionTables).getBytes(StandardCharsets.UTF_8)
        );

        Document doc;
        try {
            doc = Jsoup.parse(bais, "UTF-8", "");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        doc.outputSettings().charset(StandardCharsets.UTF_8);
        doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        doc.outputSettings().syntax(Document.OutputSettings.Syntax.xml);

        ITextRenderer renderer = new ITextRenderer();
        SharedContext sharedContext = renderer.getSharedContext();
        sharedContext.setPrint(true);
        sharedContext.setInteractive(false);
        renderer.setDocumentFromString(doc.html());
        renderer.layout();
        renderer.createPDF(baos);

        return baos.toByteArray();
    }

    @Override
    public byte[] getCSVStatistics(FormStatisticsDTO formStatistics) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        List<StatisticsEntryDTO> statistics = formStatistics.getStatistics();
        try {
            baos.write((";" + formStatistics.getFormTitle().replace(";", ">semi<") + ";\n")
                    .getBytes(StandardCharsets.UTF_8));

            for (StatisticsEntryDTO entry : statistics) {
                baos.write((";" + entry.getQuestion().replace(";", ">semi<") + ";\n").getBytes(StandardCharsets.UTF_8));

                int totalAnswers = entry.getTotalAnswers();
                int answered = entry.getAnswered();

                if (entry.getAnswers() == null && entry.getAnswerToPartMap() == null) {
                    baos.write((";;\n").getBytes(StandardCharsets.UTF_8));
                } else if (entry.getAnswerToPartMap() != null) {
                    StringBuilder csvRow = new StringBuilder();

                    for (var answerEntry : entry.getAnswerToPartMap().entrySet()) {
                        csvRow.append(answerEntry.getKey().replace(";", ">semi<")).append(";");
                        csvRow.append(answerEntry.getValue().getAnswerCount()).append(";");
                        double percentage = answerEntry.getValue().getAnswerPercentage() * 100.;

                        csvRow.append(String.format(Locale.ROOT, "%.2f", percentage)).append("\n");
                    }

                    baos.write(csvRow.toString().getBytes(StandardCharsets.UTF_8));
                } else {
                    for (String answer : entry.getAnswers())
                        baos.write(String.format(";%s;\n", answer).getBytes(StandardCharsets.UTF_8));
                }

                baos.write((";;\n").getBytes(StandardCharsets.UTF_8));

                baos.write(String.format("Total;%d;\n", totalAnswers).getBytes(StandardCharsets.UTF_8));
                baos.write(String.format("Answered;%d;\n", answered).getBytes(StandardCharsets.UTF_8));
                baos.write(
                        String.format(Locale.ROOT,
                        "%% answered;%.2f;\n", ((double) answered / totalAnswers) * 100.)
                        .getBytes(StandardCharsets.UTF_8));
            }
        } catch (IOException ioe) {
            throw new IllegalStateException(ioe);
        }

        return baos.toByteArray();
    }

    @Override
    public byte[] getTXTStatistics(FormStatisticsDTO formStatistics) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        List<StatisticsEntryDTO> statistics = formStatistics.getStatistics();
        try {
            baos.write(
                    ("- ".repeat(5) + formStatistics.getFormTitle() + " -".repeat(5) + "\n")
                            .getBytes(StandardCharsets.UTF_8)
            );

            baos.write("question\tnumber_of_answers\tpercentage\n".getBytes(StandardCharsets.UTF_8));
            baos.write(
                    ("- ".repeat(10 + Math.max(formStatistics.getFormTitle().length(), 18)) + "\n")
                            .getBytes(StandardCharsets.UTF_8)
            );

            for (StatisticsEntryDTO entry : statistics) {
                baos.write(
                        ("- ".repeat(5) + entry.getQuestion() + " -".repeat(5) + "\n").getBytes(StandardCharsets.UTF_8)
                );

                int totalAnswers = entry.getTotalAnswers();
                int answered = entry.getAnswered();

                if (entry.getAnswers() == null && entry.getAnswerToPartMap() == null) {
                    baos.write(("\n").getBytes(StandardCharsets.UTF_8));
                } else if (entry.getAnswerToPartMap() != null) {
                    StringBuilder txtRow = new StringBuilder();

                    for (var answerEntry : entry.getAnswerToPartMap().entrySet()) {
                        txtRow.append(answerEntry.getKey()).append("\t");
                        txtRow.append(answerEntry.getValue().getAnswerCount()).append("\t");
                        double percentage = answerEntry.getValue().getAnswerPercentage() * 100.;

                        txtRow.append(String.format(Locale.ROOT, "%.2f", percentage)).append("\n");
                    }

                    baos.write(txtRow.toString().getBytes(StandardCharsets.UTF_8));
                } else {
                    for (String answer : entry.getAnswers())
                        baos.write(String.format("\t%s\n", answer).getBytes(StandardCharsets.UTF_8));
                }

                baos.write(
                        ("- ".repeat(10 + Math.max(formStatistics.getFormTitle().length(), 18)) + "\n")
                                .getBytes(StandardCharsets.UTF_8)
                );

                baos.write(String.format("Total\t%d\n", totalAnswers).getBytes(StandardCharsets.UTF_8));
                baos.write(String.format("Answered\t%d\n", answered).getBytes(StandardCharsets.UTF_8));
                baos.write(
                        String.format(Locale.ROOT,
                        "%% answered\t%.2f\n", ((double) answered / totalAnswers) * 100.)
                        .getBytes(StandardCharsets.UTF_8));

                baos.write(
                        ("- ".repeat(10 + Math.max(formStatistics.getFormTitle().length(), 18)) + "\n")
                                .getBytes(StandardCharsets.UTF_8)
                );
            }
        } catch (IOException ioe) {
            throw new IllegalStateException(ioe);
        }

        return baos.toByteArray();
    }
}
