package hr.fer.lc.terraforms.repositories;

import hr.fer.lc.terraforms.dao.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
