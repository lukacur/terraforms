package hr.fer.lc.terraforms.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Luka Ćurić
 */
@RestController
@RequestMapping("/")
public class IndexController {
    @GetMapping
    public void redirectToHomepage(HttpServletResponse resp) throws IOException {
        resp.sendRedirect("/home");
    }
}
