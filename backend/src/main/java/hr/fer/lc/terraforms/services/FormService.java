package hr.fer.lc.terraforms.services;

import hr.fer.lc.terraforms.dao.AccessRequest;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dto.FormBodyDTO;
import hr.fer.lc.terraforms.dto.FormDTO;
import hr.fer.lc.terraforms.dto.FormFullDTO;

import java.util.List;
import java.util.Optional;

/**
 * @author Luka Ćurić
 */
public interface FormService {
    Form createForm(FormDTO formData, AppUser owner);
    void updateFormMetadata(FormDTO formData, AppUser userChanging);
    void deleteForm(FormDTO formData, AppUser userChanging);

    void saveInto(FormBodyDTO bodyData, AppUser userChanging);
    FormBodyDTO saveOver(FormBodyDTO bodyData, AppUser userChanging);

    Optional<Form> getFormById(Long formId);
    Form getFormByLink(String formLink);
    Form getFormByStatsLink(String statsLink);

    List<AccessRequest> getPendingRequestsFor(FormDTO formInfo, AppUser userFetching);
    List<AccessRequest> getActivePermissionsFor(FormDTO formInfo, AppUser userFetching);

    FormFullDTO responseForLink(String viewLink, AppUser userRequesting);
    List<FormFullDTO> getOwnedFormsFor(AppUser user);
    List<FormFullDTO> getSharedWithUser(AppUser user);
}
