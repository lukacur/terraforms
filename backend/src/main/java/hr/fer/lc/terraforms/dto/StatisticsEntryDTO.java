package hr.fer.lc.terraforms.dto;

import java.util.List;
import java.util.Map;

public class StatisticsEntryDTO {
    private String question;
    private int answerType;
    private int totalAnswers;
    private int answered;
    private List<String> answers;
    private Map<String, AnswerStatisticsDTO> answerToPartMap;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getAnswerType() {
        return answerType;
    }

    public void setAnswerType(int answerType) {
        this.answerType = answerType;
    }

    public int getTotalAnswers() {
        return totalAnswers;
    }

    public void setTotalAnswers(int totalAnswers) {
        this.totalAnswers = totalAnswers;
    }

    public int getAnswered() {
        return answered;
    }

    public void setAnswered(int answered) {
        this.answered = answered;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public Map<String, AnswerStatisticsDTO> getAnswerToPartMap() {
        return answerToPartMap;
    }

    public void setAnswerToPartMap(Map<String, AnswerStatisticsDTO> answerToPartMap) {
        this.answerToPartMap = answerToPartMap;
    }
}
