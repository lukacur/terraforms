package hr.fer.lc.terraforms.services.implementations;

import hr.fer.lc.terraforms.dao.AccessRequest;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dao.enums.FormAccessType;
import hr.fer.lc.terraforms.dto.AccessRequestDTO;
import hr.fer.lc.terraforms.exceptions.action.IllegalUserActionException;
import hr.fer.lc.terraforms.exceptions.action.UnauthorizedUserActionException;
import hr.fer.lc.terraforms.exceptions.data.EntryDoesNotExistException;
import hr.fer.lc.terraforms.repositories.AccessRequestRepository;
import hr.fer.lc.terraforms.services.AccessRequestService;
import hr.fer.lc.terraforms.services.AppUserService;
import hr.fer.lc.terraforms.services.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AccessRequestServiceImpl implements AccessRequestService {
    private AccessRequestRepository accessRequestRepository;
    @Autowired
    public void setAccessRequestRepository(AccessRequestRepository accessRequestRepository) {
        this.accessRequestRepository = accessRequestRepository;
    }

    private AuthorityService authorityService;
    @Autowired
    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    private AppUserService appUserService;
    @Autowired
    public void setAppUserService(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @Override
    @Transactional(rollbackFor = { EntryDoesNotExistException.class })
    public void updateRequests(Form form, List<AccessRequestDTO> requests, AppUser userUpdating) {
        if (!form.getOwner().getId().equals(userUpdating.getId()))
            throw new UnauthorizedUserActionException("User can't change permissions on a form they don't own.");

        Set<Long> keptPermissionIds = requests.stream()
                        .map(AccessRequestDTO::getEntryId)
                                .filter(Objects::nonNull)
                                        .collect(Collectors.toSet());

        form.getAccessrequests().stream()
                .filter(ar -> !keptPermissionIds.contains(ar.getId()))
                .forEach(ar -> accessRequestRepository.delete(ar));

        for (AccessRequestDTO request : requests) {
            AccessRequest req;

            if (request.getEntryId() == null) {
                req = new AccessRequest();

                AppUser userRequested = appUserService.getUserByUsername(request.getUsernameRequested())
                        .orElse(null);

                if (userRequested == null)
                    continue;

                req.setUserrequested(userRequested);
            } else {
                req = accessRequestRepository.findById(request.getEntryId())
                        .orElseThrow(() -> new EntryDoesNotExistException("Access request does not exist."));
            }

            LocalDateTime now = LocalDateTime.now();

            req.setFormid(form);
            req.setRequestedon(now);
            req.setGrantedon(now);
            req.setAccesstype(FormAccessType.withValue(request.getAccessType()).getAccessType());

            accessRequestRepository.save(req);
            req.setExpireson(null);
            accessRequestRepository.save(req);
        }
    }

    @Override
    public void denyPermission(AccessRequestDTO request, AppUser userRevoking) {
        AccessRequest req = accessRequestRepository.findById(request.getEntryId())
                        .orElseThrow(() -> new EntryDoesNotExistException("Request doesn't exist"));

        accessRequestRepository.delete(req);
    }

    @Override
    public void grantPermission(AccessRequestDTO request, AppUser userGranting) {
        AccessRequest req = accessRequestRepository.findById(request.getEntryId())
                .orElseThrow(() -> new EntryDoesNotExistException("Request doesn't exist"));

        req.setGrantedon(LocalDateTime.now());
        req.setAccesstype(FormAccessType.withValue(request.getAccessType()).getAccessType());
        req.setExpireson(null);

        accessRequestRepository.save(req);
    }

    @Override
    public void addRequestFor(Form form, FormAccessType accessType, AppUser userRequesting) {
        if (
                !accessRequestRepository.findByUserrequested(userRequesting).stream()
                    .filter(entry -> entry.getAccesstype().getId().equals(accessType.getAccessTypeId()))
                    .filter(entry -> entry.getForm().getId().equals(form.getId()))
                    .collect(Collectors.toSet()).isEmpty()
        ) {
            throw new IllegalUserActionException("Access already requested");
        }

        AccessRequest req = new AccessRequest();
        {
            req.setFormid(form);
            req.setUserrequested(userRequesting);
            req.setAccesstype(accessType.getAccessType());
        }

        accessRequestRepository.save(req);
    }

    @Override
    public void cancelPermission(AppUser userCanceling, Form form) {
        if (!authorityService.hasEditAuthority(userCanceling, form))
            throw new IllegalUserActionException("User can't cancel permission they don't have.");

        form.getAccessrequests().stream()
                .filter(ar -> ar.getUserrequested().getId().equals(userCanceling.getId())
                                && FormAccessType.withValue(ar.getAccesstype().getId()) == FormAccessType.EDIT
                )
                .forEach(ar -> accessRequestRepository.delete(ar));
    }
}
