package hr.fer.lc.terraforms.configuration.security;

import hr.fer.lc.terraforms.configuration.security.authorities.CustomGrantedAuthority;
import hr.fer.lc.terraforms.configuration.security.authorities.Permission;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dao.enums.FormAccessType;
import hr.fer.lc.terraforms.dao.enums.SupportedLoginProvider;
import hr.fer.lc.terraforms.dao.enums.UserStatus;
import hr.fer.lc.terraforms.exceptions.data.UserDoesNotExistException;
import hr.fer.lc.terraforms.repositories.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Luka Ćurić
 */
@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return setupForEmail(username);
    }

    @Transactional(readOnly = true)
    private CustomUserDetails setupForEmail(String email) {
        AppUser user = appUserRepository.getByEmail(email)
                .orElseThrow(() -> new UserDoesNotExistException("User with email " + email + " doesn't exist"));

        if (user.getLoginprovider().getId() != SupportedLoginProvider.LOCAL.getLoginProviderId())
            throw new UsernameNotFoundException("User is not a local user.");

        var authorities = buildAuthorities(user);

        return new CustomUserDetails(
                (authorities.isEmpty()) ? Set.of(new CustomGrantedAuthority(Permission.FORM_STATS, 2L)) : authorities,
                user.getPassword(),
                user.getEmail(),
                UserStatus.withValue(user.getStatus().getId()) != UserStatus.DELETED,
                UserStatus.withValue(user.getStatus().getId()) != UserStatus.DISABLED,
                true,
                UserStatus.withValue(user.getStatus().getId()) == UserStatus.ENABLED
        );
    }

    private Set<GrantedAuthority> buildAuthorities(AppUser user) {
        Set<GrantedAuthority> authorities = new HashSet<>();

        Set<Form> viewableForms = new HashSet<>();
        Set<Form> editableForms = new HashSet<>();
        Set<Form> statisticsAccess = new HashSet<>();

        for (Form form : user.getFormsOwned()) {
            viewableForms.add(form);
            editableForms.add(form);
            statisticsAccess.add(form);
        }

        user.getAccessRequests().stream()
                .filter(form -> form.getGrantedOn() != null)
                .forEach(accessRequest -> {
                    switch (FormAccessType.withValue(accessRequest.getAccesstype().getId())) {
                        case VIEW -> viewableForms.add(accessRequest.getForm());
                        case EDIT -> editableForms.add(accessRequest.getForm());
                        case STAT -> statisticsAccess.add(accessRequest.getForm());
                    }
                });

        for (Form viewableForm : viewableForms)
            authorities.add(new CustomGrantedAuthority(Permission.FORM_READ, viewableForm.getId()));

        for (Form editableForm : editableForms)
            authorities.add(new CustomGrantedAuthority(Permission.FORM_EDIT, editableForm.getId()));

        for (Form overwievableForm : statisticsAccess)
            authorities.add(new CustomGrantedAuthority(Permission.FORM_STATS, overwievableForm.getId()));

        return authorities;
    }
}
