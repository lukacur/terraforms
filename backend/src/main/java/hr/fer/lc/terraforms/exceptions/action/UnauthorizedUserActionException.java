package hr.fer.lc.terraforms.exceptions.action;

public class UnauthorizedUserActionException extends RuntimeException {
    public UnauthorizedUserActionException() {
    }

    public UnauthorizedUserActionException(String message) {
        super(message);
    }

    public UnauthorizedUserActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedUserActionException(Throwable cause) {
        super(cause);
    }
}
