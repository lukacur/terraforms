package hr.fer.lc.terraforms.controllers;

import hr.fer.lc.terraforms.constants.SessionAttribute;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dto.StoreTransferDTO;
import hr.fer.lc.terraforms.services.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Luka Ćurić
 */
@RestController
@RequestMapping(path = "/remote/store", consumes = { MediaType.APPLICATION_JSON_VALUE })
public class RemoteStoreController {
    @Autowired
    private StoreService storeService;

    @PostMapping
    public ResponseEntity<StoreTransferDTO> getFromStore(HttpServletRequest req,
                                                         @RequestBody StoreTransferDTO readRequest) {
        AppUser currUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);
        System.out.printf("Store read request from user: %s; userId: %d%n", currUser.getUsername(), currUser.getId());

        String storeEntry =storeService.getItem(currUser, readRequest.getItemKey());
        StoreTransferDTO responseData = new StoreTransferDTO();
        responseData.setItemKey(readRequest.getItemKey());
        responseData.setItemValue(storeEntry);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> updateStoreEntry(HttpServletRequest req,
                                              @RequestBody List<StoreTransferDTO> storeRequests) {
        for (StoreTransferDTO storeRequest : storeRequests) {
            this.updateStoreEntry(req, storeRequest);
        }

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PatchMapping
    public ResponseEntity<?> updateStoreEntry(HttpServletRequest req, @RequestBody StoreTransferDTO storeRequest) {
        AppUser currUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);
        System.out.printf("Store write request from user: %s; userId: %d%n", currUser.getUsername(), currUser.getId());

        storeService.setItem(currUser, storeRequest.getItemKey(), storeRequest.getItemValue());

        return new ResponseEntity<>("", HttpStatus.OK);
    }
}
