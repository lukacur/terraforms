package hr.fer.lc.terraforms.dto;

public class PermissionRequestDTO {
    private String formLink;
    private Integer accessType;

    public String getFormLink() {
        return formLink;
    }

    public void setFormLink(String formLink) {
        this.formLink = formLink;
    }

    public Integer getAccessType() {
        return accessType;
    }

    public void setAccessType(Integer accessType) {
        this.accessType = accessType;
    }
}
