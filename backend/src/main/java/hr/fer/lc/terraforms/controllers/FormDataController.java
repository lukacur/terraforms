package hr.fer.lc.terraforms.controllers;

import hr.fer.lc.terraforms.constants.SessionAttribute;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dto.AnswerDTO;
import hr.fer.lc.terraforms.dto.FormFullDTO;
import hr.fer.lc.terraforms.exceptions.data.EntryDoesNotExistException;
import hr.fer.lc.terraforms.services.AnswerService;
import hr.fer.lc.terraforms.services.FormService;
import hr.fer.lc.terraforms.services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/api/data/form")
public class FormDataController {
    private FormService formService;
    @Autowired
    public void setFormService(FormService formService) {
        this.formService = formService;
    }

    private StatisticsService statisticsService;
    @Autowired
    public void setStatisticsService(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    private AnswerService answerService;
    @Autowired
    public void setAnswerService(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping(path = "/get/{viewLink}", consumes = { MediaType.ALL_VALUE })
    public ResponseEntity<?> getFormData(HttpServletRequest req, HttpServletResponse resp,
                                         @PathVariable("viewLink") String formViewLink) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        FormFullDTO formData = formService.responseForLink(formViewLink, currentUser);

        return new ResponseEntity<>(formData, HttpStatus.OK);
    }

    @PutMapping(path = "/answer", consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> receiveResponse(HttpServletRequest req, @RequestBody AnswerDTO answerInfo) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        Form form = formService.getFormById(answerInfo.getFormId())
                .orElseThrow(() -> new EntryDoesNotExistException("Form doesn't exist"));

        answerService.saveAnswers(form, answerInfo.getAnswers(), currentUser);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @GetMapping(path = "/ownedForms")
    public ResponseEntity<?> getOwnedForms(HttpServletRequest req, HttpServletResponse resp) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        List<FormFullDTO> ownedForms = formService.getOwnedFormsFor(currentUser);

        return new ResponseEntity<>(ownedForms, HttpStatus.OK);
    }

    @GetMapping(path = "/sharedWithMe")
    public ResponseEntity<?> getSharedWithUser(HttpServletRequest req, HttpServletResponse resp) {
        AppUser currentUser = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        List<FormFullDTO> sharedWithUser = formService.getSharedWithUser(currentUser);

        return new ResponseEntity<>(sharedWithUser, HttpStatus.OK);
    }
}
