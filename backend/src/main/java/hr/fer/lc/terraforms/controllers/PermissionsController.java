package hr.fer.lc.terraforms.controllers;

import hr.fer.lc.terraforms.constants.SessionAttribute;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dao.enums.FormAccessType;
import hr.fer.lc.terraforms.dto.AccessDataDTO;
import hr.fer.lc.terraforms.dto.AccessRequestDTO;
import hr.fer.lc.terraforms.dto.FormDTO;
import hr.fer.lc.terraforms.dto.PermissionRequestDTO;
import hr.fer.lc.terraforms.exceptions.data.EntryDoesNotExistException;
import hr.fer.lc.terraforms.services.AccessRequestService;
import hr.fer.lc.terraforms.services.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/permissions", consumes = { MediaType.APPLICATION_JSON_VALUE })
public class PermissionsController {
    private AccessRequestService accessRequestService;
    @Autowired
    public void setAccessRequestService(AccessRequestService accessRequestService) {
        this.accessRequestService = accessRequestService;
    }

    private FormService formService;
    @Autowired
    public void setFormService(FormService formService) {
        this.formService = formService;
    }

    @DeleteMapping("/cancel")
    @Transactional
    public ResponseEntity<?> cancelPermissionForSelf(HttpServletRequest req, @RequestBody FormDTO formInfo) {
        AppUser userCanceling = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        Form form = formService.getFormById(formInfo.getFormId()).orElseThrow(() -> new EntryDoesNotExistException("Invalid form. Form doesn't exist."));
        accessRequestService.cancelPermission(userCanceling, form);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/request")
    @Transactional
    public ResponseEntity<?> requestPermission(HttpServletRequest req, @RequestBody PermissionRequestDTO request) {
        AppUser userRequesting = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        Form form = formService.getFormByLink(request.getFormLink());

        accessRequestService.addRequestFor(form, FormAccessType.withValue(request.getAccessType()), userRequesting);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/pending")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getPendingRequests(HttpServletRequest req, @RequestBody FormDTO formInfo) {
        AppUser userFetching = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        List<AccessRequestDTO> response = formService.getPendingRequestsFor(formInfo, userFetching).stream()
                .map(ar -> {
                    AccessRequestDTO reqDto = new AccessRequestDTO();
                    {
                        reqDto.setAccessType(ar.getAccesstype().getId());
                        reqDto.setEntryId(ar.getId());
                        reqDto.setFormId(ar.getForm().getId());
                        reqDto.setUsernameRequested(ar.getUserrequested().getUsername());
                    }

                    return reqDto;
                })
                .collect(Collectors.toList());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/active")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getActivePermissions(HttpServletRequest req, @RequestBody FormDTO formInfo) {
        AppUser userFetching = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        List<AccessRequestDTO> response = formService.getActivePermissionsFor(formInfo, userFetching).stream()
                .map(ar -> {
                    AccessRequestDTO reqDto = new AccessRequestDTO();
                    {
                        reqDto.setAccessType(ar.getAccesstype().getId());
                        reqDto.setEntryId(ar.getId());
                        reqDto.setFormId(ar.getForm().getId());
                        reqDto.setUsernameRequested(ar.getUserrequested().getUsername());
                    }

                    return reqDto;
                })
                .collect(Collectors.toList());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/update")
    @Transactional(rollbackFor = { Exception.class })
    public ResponseEntity<?> updatePermissions(HttpServletRequest req, @RequestBody AccessDataDTO accessInfo) {
        AppUser userChanging = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        Form form = formService.getFormById(accessInfo.getFormId())
                .orElseThrow(() -> new EntryDoesNotExistException("Form doesn't exist"));

        accessRequestService.updateRequests(form, accessInfo.getRequests(), userChanging);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/grant")
    public ResponseEntity<?> grantPermission(HttpServletRequest req, @RequestBody AccessRequestDTO accessInfo) {
        AppUser userGranting = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        accessRequestService.grantPermission(accessInfo, userGranting);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/deny")
    public ResponseEntity<?> denyPermission(HttpServletRequest req, @RequestBody AccessRequestDTO accessInfo) {
        AppUser userRevoking = (AppUser) req.getSession().getAttribute(SessionAttribute.CURRENT_USER);

        accessRequestService.denyPermission(accessInfo, userRevoking);

        return new ResponseEntity<>("", HttpStatus.OK);
    }
}
