package hr.fer.lc.terraforms.services;

import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dto.AnswerPartDTO;

import java.util.List;

public interface AnswerService {
    void saveAnswers(Form form, List<AnswerPartDTO> answers, AppUser userAnswering);
}
