package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "accessrequest")
public class AccessRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "entryid", nullable = false)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userrequested")
    private AppUser userrequested;

    @Column(name = "grantedon")
    private LocalDateTime grantedon;

    @Column(name = "expireson")
    private LocalDate expireson;

    @Column(name = "requestedon", insertable = false)
    private LocalDateTime requestedon;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "formid", nullable = false)
    private Form formid;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "accesstype", nullable = false)
    private AccessType accesstype;

    public AccessType getAccesstype() {
        return accesstype;
    }

    public void setAccesstype(AccessType accesstype) {
        this.accesstype = accesstype;
    }

    public Form getForm() {
        return formid;
    }

    public void setFormid(Form formid) {
        this.formid = formid;
    }

    public LocalDateTime getRequestedon() {
        return requestedon;
    }

    public void setRequestedon(LocalDateTime requestedon) {
        this.requestedon = requestedon;
    }

    public LocalDate getExpireson() {
        return expireson;
    }

    public void setExpireson(LocalDate expireson) {
        this.expireson = expireson;
    }

    public LocalDateTime getGrantedOn() {
        return grantedon;
    }

    public void setGrantedon(LocalDateTime grantedon) {
        this.grantedon = grantedon;
    }

    public AppUser getUserrequested() {
        return userrequested;
    }

    public void setUserrequested(AppUser userrequested) {
        this.userrequested = userrequested;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}