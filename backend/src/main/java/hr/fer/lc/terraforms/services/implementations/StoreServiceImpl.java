package hr.fer.lc.terraforms.services.implementations;

import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.services.StoreService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Luka Ćurić
 */
@Service
public class StoreServiceImpl implements StoreService {
    private final Map<Long, Map<String, String>> usersData = new HashMap<>();

    @Override
    public String getItem(AppUser user, String itemKey) {
        Map<String, String> userMap = this.usersData.get(user.getId());

        if (userMap == null)
            return null;

        return this.usersData.get(user.getId()).get(itemKey);
    }

    @Override
    public void setItem(AppUser user, String itemKey, String itemValue) {
        this.usersData.compute(user.getId(), (userKey, mapValue) -> {
            if (mapValue == null) {
                mapValue = new HashMap<>();
            }

            mapValue.put(itemKey, itemValue);

            return mapValue;
        });
    }
}
