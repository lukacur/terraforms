package hr.fer.lc.terraforms.configuration.security.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Luka Ćurić
 */
public class CredentialsFilter extends AbstractAuthenticationProcessingFilter {
    private static class CredentialsWrapper {
        @JsonProperty
        private String username;
        @JsonProperty
        private String password;
    }

    public CredentialsFilter() {
        super(new AntPathRequestMatcher("/api/login", "POST"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        CredentialsWrapper creds;

        try {
            creds = (new ObjectMapper()).readValue(request.getInputStream(), CredentialsWrapper.class);
        } catch (Exception ex) {
            throw new AuthenticationServiceException(ex.getMessage(), ex);
        }

        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                creds.username,
                creds.password
        );

        return this.getAuthenticationManager().authenticate(authToken);
    }
}
