package hr.fer.lc.terraforms.dto;

import java.util.List;

/**
 * @author Luka Ćurić
 */
public class QuestionDTO {
    private Long questionId;
    private Long previousVersion;
    private Integer questionNumber;
    private Integer questionPage;
    private Boolean isRequired;
    private Integer answerType;
    private String question;
    private List<PossibleAnswerDTO> possibleAnswers;

    public QuestionDTO() {}

    public QuestionDTO(Long questionId, Long previousVersion, Integer questionNumber, Integer questionPage,
                       Boolean isRequired, Integer answerType, String question,
                       List<PossibleAnswerDTO> possibleAnswers) {
        this.questionId = questionId;
        this.previousVersion = previousVersion;
        this.questionNumber = questionNumber;
        this.questionPage = questionPage;
        this.isRequired = isRequired;
        this.answerType = answerType;
        this.question = question;
        this.possibleAnswers = possibleAnswers;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getPreviousVersion() {
        return previousVersion;
    }

    public void setPreviousVersion(Long previousVersion) {
        this.previousVersion = previousVersion;
    }

    public Integer getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(Integer questionNumber) {
        this.questionNumber = questionNumber;
    }

    public Integer getQuestionPage() {
        return questionPage;
    }

    public void setQuestionPage(Integer questionPage) {
        this.questionPage = questionPage;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean required) {
        isRequired = required;
    }

    public String getQuestion() {
        return question;
    }

    public Integer getAnswerType() {
        return answerType;
    }

    public void setAnswerType(Integer answerType) {
        this.answerType = answerType;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<PossibleAnswerDTO> getPossibleAnswers() {
        return possibleAnswers;
    }

    public void setPossibleAnswers(List<PossibleAnswerDTO> possibleAnswers) {
        this.possibleAnswers = possibleAnswers;
    }
}
