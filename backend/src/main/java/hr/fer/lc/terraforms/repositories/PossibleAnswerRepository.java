package hr.fer.lc.terraforms.repositories;

import hr.fer.lc.terraforms.dao.PossibleAnswer;
import hr.fer.lc.terraforms.dao.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PossibleAnswerRepository extends JpaRepository<PossibleAnswer, Long> {
    Optional<PossibleAnswer> getByQuestionidAndName(Question questionId, String name);
}
