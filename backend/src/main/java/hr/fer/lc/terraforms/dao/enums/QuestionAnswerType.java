package hr.fer.lc.terraforms.dao.enums;

import hr.fer.lc.terraforms.dao.AnswerType;

import java.util.List;

public enum QuestionAnswerType {
    SHORTTEXT(0),
    LONGTEXT(1),
    NUMBER(2),
    SCALE(3),
    RADIO(4),
    CHECKBOX(5),
    TRUEFALSE(6),
    SCALEPERCENT(7);

    private final int questionTypeId;

    private static final List<QuestionAnswerType> settable = List.of(SCALE, RADIO, CHECKBOX);

    private static final AnswerType TYPE_SHORTTEXT;
    private static final AnswerType TYPE_LONGTEXT;
    private static final AnswerType TYPE_NUMBER;
    private static final AnswerType TYPE_SCALE;
    private static final AnswerType TYPE_RADIO;
    private static final AnswerType TYPE_CHECKBOX;
    private static final AnswerType TYPE_TRUEFALSE;
    private static final AnswerType TYPE_SCALEPERCENT;

    static {
        TYPE_SHORTTEXT = new AnswerType();
        {
            TYPE_SHORTTEXT.setAnswertypename(SHORTTEXT.name());
            TYPE_SHORTTEXT.setId(0);
        }

        TYPE_LONGTEXT = new AnswerType();
        {
            TYPE_LONGTEXT.setAnswertypename(LONGTEXT.name());
            TYPE_LONGTEXT.setId(1);
        }

        TYPE_NUMBER = new AnswerType();
        {
            TYPE_NUMBER.setAnswertypename(NUMBER.name());
            TYPE_NUMBER.setId(2);
        }

        TYPE_SCALE = new AnswerType();
        {
            TYPE_SCALE.setAnswertypename(SCALE.name());
            TYPE_SCALE.setId(3);
        }

        TYPE_RADIO = new AnswerType();
        {
            TYPE_RADIO.setAnswertypename(RADIO.name());
            TYPE_RADIO.setId(4);
        }

        TYPE_CHECKBOX = new AnswerType();
        {
            TYPE_CHECKBOX.setAnswertypename(CHECKBOX.name());
            TYPE_CHECKBOX.setId(5);
        }

        TYPE_TRUEFALSE = new AnswerType();
        {
            TYPE_TRUEFALSE.setAnswertypename(TRUEFALSE.name());
            TYPE_TRUEFALSE.setId(6);
        }

        TYPE_SCALEPERCENT = new AnswerType();
        {
            TYPE_SCALEPERCENT.setAnswertypename(SCALEPERCENT.name());
            TYPE_SCALEPERCENT.setId(7);
        }
    }

    QuestionAnswerType(int questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public int getQuestionTypeId() {
        return questionTypeId;
    }

    public AnswerType getAnswType() {
        return switch (this) {
            case SHORTTEXT -> TYPE_SHORTTEXT;
            case LONGTEXT -> TYPE_LONGTEXT;
            case NUMBER -> TYPE_NUMBER;
            case SCALE -> TYPE_SCALE;
            case RADIO -> TYPE_RADIO;
            case CHECKBOX -> TYPE_CHECKBOX;
            case TRUEFALSE -> TYPE_TRUEFALSE;
            case SCALEPERCENT -> TYPE_SCALEPERCENT;
        };
    }

    public static QuestionAnswerType withValue(int value) {
        return switch (value) {
            case 0 -> SHORTTEXT;
            case 1 -> LONGTEXT;
            case 2 -> NUMBER;
            case 3 -> SCALE;
            case 4 -> RADIO;
            case 5 -> CHECKBOX;
            case 6 -> TRUEFALSE;
            case 7 -> SCALEPERCENT;
            default -> throw new IllegalArgumentException("Invalid restriction type id");
        };
    }

    public static List<QuestionAnswerType> getSettable() {
        return settable;
    }
}
