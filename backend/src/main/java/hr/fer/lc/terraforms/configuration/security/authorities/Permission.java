package hr.fer.lc.terraforms.configuration.security.authorities;

/**
 * @author Luka Ćurić
 */
public enum Permission {
    FORM_READ("form:read"),
    FORM_EDIT("form:edit"),
    FORM_STATS("form:stats");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
