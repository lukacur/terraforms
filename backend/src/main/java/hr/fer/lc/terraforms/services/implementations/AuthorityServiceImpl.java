package hr.fer.lc.terraforms.services.implementations;

import hr.fer.lc.terraforms.dao.AccessRequest;
import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.Form;
import hr.fer.lc.terraforms.dao.enums.FormAccessType;
import hr.fer.lc.terraforms.dao.enums.FormRestrictionType;
import hr.fer.lc.terraforms.services.AuthorityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    private static boolean isGranted(AccessRequest request) {
        return request.getGrantedOn() != null;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean hasReadAuthority(AppUser user, Form form) {
        FormRestrictionType restrictionType = FormRestrictionType.withValue(form.getRestrictedto().getId());

        return (
            restrictionType == FormRestrictionType.NONE ||
            user != null && form.getOwner().getId().equals(user.getId()) ||
            user != null && (
                restrictionType == FormRestrictionType.LOGGED_IN ||
                form.getAccessrequests().stream()
                .filter(request -> isGranted(request) &&
                        request.getUserrequested().getId().equals(user.getId())
                )
                .mapToInt(req -> req.getAccesstype().getId())
                .anyMatch(aTypeId -> FormAccessType.withValue(aTypeId) == FormAccessType.VIEW)
            )
        );
    }

    @Override
    @Transactional(readOnly = true)
    public boolean hasEditAuthority(AppUser user, Form form) {
        return user != null && (
            form.getOwner().getId().equals(user.getId()) ||
            form.getAccessrequests().stream()
                    .filter(request -> isGranted(request) &&
                            request.getUserrequested().getId().equals(user.getId())
                    )
                    .mapToInt(req -> req.getAccesstype().getId())
                    .anyMatch(aTypeId -> FormAccessType.withValue(aTypeId) == FormAccessType.EDIT)
        );
    }

    @Override
    @Transactional(readOnly = true)
    public boolean hasStatisticsAuthority(AppUser user, Form form) {
        return FormRestrictionType.withValue(form.getRestrictedto().getId()) == FormRestrictionType.NONE ||
               user != null && (
                form.getOwner().getId().equals(user.getId()) ||
                        form.getAccessrequests().stream()
                                .filter(request -> isGranted(request) &&
                                        request.getUserrequested().getId().equals(user.getId())
                                )
                                .mapToInt(req -> req.getAccesstype().getId())
                                .anyMatch(aTypeId -> FormAccessType.withValue(aTypeId) == FormAccessType.STAT)
                );
    }
}
