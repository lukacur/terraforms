package hr.fer.lc.terraforms.dto;

/**
 * @author Luka Ćurić
 */
public class FormDTO {
    private Long formId;
    private String title;
    private String description;
    private String formLink;
    private String statsLink;
    private Integer restrictedTo;

    public FormDTO() {}

    public FormDTO(Long formId, String title, String description, Integer restrictedTo) {
        this.formId = formId;
        this.title = title;
        this.description = description;
        this.restrictedTo = restrictedTo;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRestrictedTo() {
        return restrictedTo;
    }

    public String getFormLink() {
        return formLink;
    }

    public void setFormLink(String formLink) {
        this.formLink = formLink;
    }

    public String getStatsLink() {
        return statsLink;
    }

    public void setStatsLink(String statsLink) {
        this.statsLink = statsLink;
    }

    public void setRestrictedTo(Integer restrictedTo) {
        this.restrictedTo = restrictedTo;
    }

    @Override
    public String toString() {
        return "FormDTO{" +
                "formId=" + formId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", restrictedTo=" + restrictedTo +
                '}';
    }
}
