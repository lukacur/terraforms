package hr.fer.lc.terraforms.dao.enums;

import hr.fer.lc.terraforms.dao.RestrictionType;

/**
 * @author Luka Ćurić
 */
public enum FormRestrictionType {
    NONE(0),
    LOGGED_IN(1),
    SELECTED(2);

    private final int restrictionTypeId;

    private static final RestrictionType RESTRICTION_NONE;
    private static final RestrictionType RESTRICTION_LOGGED_IN;
    private static final RestrictionType RESTRICTION_SELECTED;

    static {
        RESTRICTION_NONE = new RestrictionType();
        {
            RESTRICTION_NONE.setRestrictionname("NONE");
            RESTRICTION_NONE.setId(0);
        }

        RESTRICTION_LOGGED_IN = new RestrictionType();
        {
            RESTRICTION_LOGGED_IN.setRestrictionname("LOGGED_IN");
            RESTRICTION_LOGGED_IN.setId(1);
        }

        RESTRICTION_SELECTED = new RestrictionType();
        {
            RESTRICTION_SELECTED.setRestrictionname("SELECTED");
            RESTRICTION_SELECTED.setId(2);
        }
    }

    FormRestrictionType(int restrictionTypeId) {
        this.restrictionTypeId = restrictionTypeId;
    }

    public int getRestrictionTypeId() {
        return restrictionTypeId;
    }

    public RestrictionType getRestType() {
        return switch (this) {
            case NONE -> RESTRICTION_NONE;
            case LOGGED_IN -> RESTRICTION_LOGGED_IN;
            case SELECTED -> RESTRICTION_SELECTED;
        };
    }

    public static FormRestrictionType withValue(int value) {
        return switch (value) {
            case 0 -> NONE;
            case 1 -> LOGGED_IN;
            case 2 -> SELECTED;
            default -> throw new IllegalArgumentException("Invalid restriction type id");
        };
    }
}
