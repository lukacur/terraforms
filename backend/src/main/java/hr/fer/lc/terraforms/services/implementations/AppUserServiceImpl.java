package hr.fer.lc.terraforms.services.implementations;

import hr.fer.lc.terraforms.dao.AppUser;
import hr.fer.lc.terraforms.dao.LoginProvider;
import hr.fer.lc.terraforms.dao.enums.SupportedLoginProvider;
import hr.fer.lc.terraforms.dao.enums.UserStatus;
import hr.fer.lc.terraforms.dto.UserInfoDTO;
import hr.fer.lc.terraforms.dto.UserInfoUpdateDTO;
import hr.fer.lc.terraforms.dto.UserRegisterDTO;
import hr.fer.lc.terraforms.exceptions.action.IllegalUserActionException;
import hr.fer.lc.terraforms.exceptions.data.EntryDoesNotExistException;
import hr.fer.lc.terraforms.exceptions.data.UserAlreadyExistsException;
import hr.fer.lc.terraforms.repositories.AppUserRepository;
import hr.fer.lc.terraforms.services.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Luka Ćurić
 */
@Service
public class AppUserServiceImpl implements AppUserService {
    private static final Pattern EMAIL_PATTERN = Pattern.compile(
        "[a-zA-Z0-9-_.]+@[a-zA-Z0-9](-[a-zA-Z0-9]|[a-zA-Z0-9.])+\\.[a-zA-Z]{2,10}"
    );

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public Optional<AppUser> getUserByEmail(String email) {
        return appUserRepository.getByEmail(email);
    }

    @Override
    public Optional<AppUser> getUserByUsername(String username) {
        return appUserRepository.getByUsername(username);
    }

    @Override
    public Optional<AppUser> getUserById(Long id) {
        return Optional.of(appUserRepository.getById(id));
    }

    @Override
    public void handleOAuthLoginEmail(String email) {
        System.out.println("User with " + email + " just logged in through OAuth2!");
    }

    @Override
    public void handleOAuthLoginUsername(String username) {
        throw new UnsupportedOperationException("OAuth adding with usernames not supported");
    }

    @Override
    public void registerUser(UserRegisterDTO data) {
        if (appUserRepository.existsByEmail(data.getEmail()))
            throw new UserAlreadyExistsException("User already exists");

        if (!data.getPassword().equals(data.getRepeatedPassword()))
            throw new IllegalArgumentException("Passwords don't match");

        if (data.getPassword().length() < 8)
            throw new IllegalArgumentException("Password should be at least 8 characters long");

        if (data.getUsername().length() < 5)
            throw new IllegalArgumentException("Username should be at least 5 characters long");

        if (!EMAIL_PATTERN.matcher(data.getEmail()).find())
            throw new IllegalArgumentException("Invalid email format");

        AppUser user = new AppUser();
        user.setEmail(data.getEmail());
        user.setPassword(passwordEncoder.encode(data.getPassword()));
        user.setUsername(data.getUsername());
        user.setLoginprovider(SupportedLoginProvider.LOCAL.getLoginProvider());
        user.setStatus(UserStatus.ENABLED.getStatus());

        appUserRepository.save(user);
    }

    @Override
    public boolean existsByEmail(String email) {
        return appUserRepository.existsByEmail(email);
    }

    @Override
    public AppUser registerOAuthUser(String username, String email, LoginProvider loginProvider) {
        if (appUserRepository.existsByEmail(email))
            throw new UserAlreadyExistsException("User already exists");

        AppUser user = new AppUser();
        user.setEmail(email);
        user.setLoginprovider(loginProvider);
        user.setStatus(UserStatus.ENABLED.getStatus());
        user.setPassword(null);
        user.setUsername(username);

        appUserRepository.save(user);

        return user;
    }

    @Override
    public UserInfoDTO getUserInfo(AppUser user) {
        AppUser userInfo = appUserRepository.findById(user.getId())
                .orElse(null);

        if (userInfo == null)
            return null;

        UserInfoDTO userData = new UserInfoDTO();
        {
            userData.setUsername(userInfo.getUsername());
            userData.setEmail(userInfo.getEmail());
            userData.setIsLocalUser(
                    SupportedLoginProvider.withValue(user.getLoginprovider().getId()) == SupportedLoginProvider.LOCAL
            );
        }

        return userData;
    }

    @Override
    public void updateUserInfo(AppUser user, UserInfoUpdateDTO newUserInfo) {
        AppUser updating = appUserRepository.findById(user.getId())
                .orElseThrow(() -> new EntryDoesNotExistException("User does not exist"));

        if (SupportedLoginProvider.withValue(updating.getLoginprovider().getId()) != SupportedLoginProvider.LOCAL)
            throw new IllegalUserActionException("User with non-local login provider can't change their information");

        if (newUserInfo.getEmail() != null && !EMAIL_PATTERN.matcher(newUserInfo.getEmail()).find())
            throw new IllegalArgumentException("Invalid email format");

        if (newUserInfo.getPassword() != null && newUserInfo.getRepeatedPassword() != null) {
            if (!newUserInfo.getPassword().equals(newUserInfo.getRepeatedPassword()))
                throw new IllegalArgumentException("Passwords do not match");

            updating.setPassword(passwordEncoder.encode(newUserInfo.getPassword()));
        }

        if (newUserInfo.getEmail() != null)
            updating.setEmail(newUserInfo.getEmail());

        if (newUserInfo.getEmail() != null ||
                newUserInfo.getPassword() != null && newUserInfo.getRepeatedPassword() != null)
            appUserRepository.save(updating);
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> userNamesContaining(String pattern) {
        Pattern pat = Pattern.compile(String.format("(?i)%s", pattern));
        return appUserRepository.findAll().stream()
                .filter(Objects::nonNull)
                .map(AppUser::getUsername)
                .filter(username -> pat.matcher(username).find())
                .collect(Collectors.toList());
    }
}
