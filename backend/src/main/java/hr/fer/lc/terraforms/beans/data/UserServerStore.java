package hr.fer.lc.terraforms.beans.data;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.util.HashMap;
import java.util.Map;

public class UserServerStore {
    private static final UserServerStore instance = new UserServerStore();
    private final Map<Long, Map<String, String>> dataMap = new HashMap<>();

    private UserServerStore() {}

    public Map<String, String> getUserStore(Long userId) {
        return dataMap.get(userId);
    }

    public Map<String, String> createUserStore(Long userId) {
        if (dataMap.containsKey(userId))
            throw new IllegalArgumentException("User store already exists for user " + userId);

        Map<String, String> store = new HashMap<>();
        dataMap.put(userId, store);

        return store;
    }

    @Bean
    @Scope(scopeName = "singleton")
    public static UserServerStore getInstance() {
        return instance;
    }
}
