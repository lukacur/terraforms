package hr.fer.lc.terraforms.services.implementations;

import hr.fer.lc.terraforms.dao.Question;
import hr.fer.lc.terraforms.exceptions.data.EntryDoesNotExistException;
import hr.fer.lc.terraforms.repositories.QuestionRepository;
import hr.fer.lc.terraforms.services.PossibleAnswerService;
import hr.fer.lc.terraforms.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    private PossibleAnswerService possibleAnswerService;

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public Question getQuestionById(Long questionId) {
        if (questionRepository.findById(questionId).isEmpty())
            throw new EntryDoesNotExistException("Question does not exist");

        return questionRepository.findById(questionId).get();
    }

    @Override
    public void saveOrUpdateQuestion(Question question) {
        questionRepository.save(question);
    }

    @Override
    public void deleteQuestion(Question question) {
        questionRepository.delete(question);
    }
}
