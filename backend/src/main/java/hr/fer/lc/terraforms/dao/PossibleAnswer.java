package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "possibleanswer")
public class PossibleAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "possibleanswerid", nullable = false)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usercreated")
    private AppUser usercreated;

    @Column(name = "name", nullable = false, length = 128)
    private String name;

    @Column(name = "value", nullable = false, length = 256)
    private String value;

    @Column(name = "tscreated", insertable = false)
    private LocalDateTime tscreated;

    @Column(name = "tsmodified")
    private LocalDateTime tsmodified;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usermodified")
    private AppUser usermodified;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "questionid", nullable = false)
    private Question questionid;

    public Question getQuestionid() {
        return questionid;
    }

    public void setQuestionid(Question questionid) {
        this.questionid = questionid;
    }

    public AppUser getUsermodified() {
        return usermodified;
    }

    public void setUsermodified(AppUser usermodified) {
        this.usermodified = usermodified;
    }

    public LocalDateTime getTsmodified() {
        return tsmodified;
    }

    public void setTsmodified(LocalDateTime tsmodified) {
        this.tsmodified = tsmodified;
    }

    public LocalDateTime getTscreated() {
        return tscreated;
    }

    public void setTscreated(LocalDateTime tscreated) {
        this.tscreated = tscreated;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AppUser getUsercreated() {
        return usercreated;
    }

    public void setUsercreated(AppUser usercreated) {
        this.usercreated = usercreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}