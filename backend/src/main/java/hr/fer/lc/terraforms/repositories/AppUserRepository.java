package hr.fer.lc.terraforms.repositories;

import hr.fer.lc.terraforms.dao.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    Optional<AppUser> getByEmail(String email);
    Optional<AppUser> getByUsername(String username);

    boolean existsByEmail(String email);
}