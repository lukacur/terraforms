package hr.fer.lc.terraforms.services;

import hr.fer.lc.terraforms.dao.Question;

public interface QuestionService {
    Question getQuestionById(Long questionId);
    void saveOrUpdateQuestion(Question question);
    void deleteQuestion(Question question);
}
