package hr.fer.lc.terraforms.dao.enums;

import hr.fer.lc.terraforms.dao.Status;

/**
 * @author Luka Ćurić
 */
public enum UserStatus {
    ENABLED(0),
    ACTIVATION_PENDING(1),
    DISABLED(2),
    DELETION_PENDING(3),
    DELETED(4);

    private final int statusId;

    private static final Status STATUS_ENABLED;
    private static final Status STATUS_ACTIVATION_PENDING;
    private static final Status STATUS_DISABLED;
    private static final Status STATUS_DELETION_PENDING;
    private static final Status STATUS_DELETED;

    static {
        STATUS_ENABLED = new Status();
        {
            STATUS_ENABLED.setId(0);
            STATUS_ENABLED.setStatusname("ENABLED");
        }

        STATUS_ACTIVATION_PENDING = new Status();
        {
            STATUS_ACTIVATION_PENDING.setId(1);
            STATUS_ACTIVATION_PENDING.setStatusname("ACTIVATION_PENDING");
        }

        STATUS_DISABLED = new Status();
        {
            STATUS_DISABLED.setId(2);
            STATUS_DISABLED.setStatusname("DISABLED");
        }

        STATUS_DELETION_PENDING = new Status();
        {
            STATUS_DELETION_PENDING.setId(3);
            STATUS_DELETION_PENDING.setStatusname("DELETION_PENDING");
        }

        STATUS_DELETED = new Status();
        {
            STATUS_DELETED.setId(4);
            STATUS_DELETED.setStatusname("DELETED");
        }
    }

    UserStatus(int statusId) {
        this.statusId = statusId;
    }

    public int getStatusId() {
        return statusId;
    }

    public Status getStatus() {
        return switch (this) {
            case DELETED -> STATUS_DELETED;
            case ENABLED -> STATUS_ENABLED;
            case DISABLED -> STATUS_DISABLED;
            case DELETION_PENDING -> STATUS_DELETION_PENDING;
            case ACTIVATION_PENDING -> STATUS_ACTIVATION_PENDING;
        };
    }

    public static UserStatus withValue(int value) {
        return switch (value) {
            case 0 -> ENABLED;
            case 1 -> ACTIVATION_PENDING;
            case 2 -> DISABLED;
            case 3 -> DELETION_PENDING;
            case 4 -> DELETED;
            default -> throw new IllegalArgumentException("Invalid status id");
        };
    }
}
