package hr.fer.lc.terraforms.dto;

public class AccessRequestDTO {
    private Long entryId;
    private Long formId;
    private String usernameRequested;
    private Integer accessType;

    public Long getEntryId() {
        return entryId;
    }

    public void setEntryId(Long entryId) {
        this.entryId = entryId;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public String getUsernameRequested() {
        return usernameRequested;
    }

    public void setUsernameRequested(String usernameRequested) {
        this.usernameRequested = usernameRequested;
    }

    public Integer getAccessType() {
        return accessType;
    }

    public void setAccessType(Integer accessType) {
        this.accessType = accessType;
    }
}
