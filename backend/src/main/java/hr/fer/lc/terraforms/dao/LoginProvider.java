package hr.fer.lc.terraforms.dao;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "loginprovider")
public class LoginProvider {
    @Id
    @Column(name = "providerid", nullable = false)
    private Integer id;

    @Column(name = "providername", nullable = false, length = 64)
    private String providername;

    @OneToMany(mappedBy = "loginprovider")
    private Set<AppUser> appusers = new LinkedHashSet<>();

    public Set<AppUser> getAppusers() {
        return appusers;
    }

    public void setAppusers(Set<AppUser> appusers) {
        this.appusers = appusers;
    }

    public String getProvidername() {
        return providername;
    }

    public void setProvidername(String providername) {
        this.providername = providername;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}