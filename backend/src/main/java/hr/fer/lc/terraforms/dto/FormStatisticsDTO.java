package hr.fer.lc.terraforms.dto;

import java.util.List;

public class FormStatisticsDTO {
    private String formTitle;
    private String formDescription;
    private List<StatisticsEntryDTO> statistics;

    public String getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle;
    }

    public String getFormDescription() {
        return formDescription;
    }

    public void setFormDescription(String formDescription) {
        this.formDescription = formDescription;
    }

    public List<StatisticsEntryDTO> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<StatisticsEntryDTO> statistics) {
        this.statistics = statistics;
    }
}
