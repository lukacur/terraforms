type StoreConfig = {
    readonly host: string,
    readonly port: number,
    readonly fullHost: string,
    readonly storeEndpoint: string
}

let storeConfig: StoreConfig = {
    host: "localhost",
    port: 8080,
    fullHost: "http://localhost:8080",
    storeEndpoint: "/remote/store"
};

export default storeConfig.fullHost + storeConfig.storeEndpoint;