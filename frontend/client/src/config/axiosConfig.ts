import axios from "axios";

const DEV_ENV: boolean = false;
const corsAxios = (!DEV_ENV) ? axios : axios.create(
    {
        withCredentials: true,
        headers: {
            "Content-Type": "application/json"
        }
    }
);

export default corsAxios;