type ConnConfig = {
    readonly host: string,
    readonly port: number,
    readonly fullHost: string,
    readonly oauth2Host: string
};

type ApiConfig = {
    readonly OAUTH2_GOOGLE: string,
    readonly API_FORM_CREATE: string,
    readonly API_FORM_UPDATE: string,
    readonly API_FORM_DELETE: string,
    readonly API_FORM_SAVE_OVER: string,
    readonly API_FORM_SAVE_INTO: string,
    readonly API_FORM_DATA_FETCH: string,
    readonly API_FORM_STATS_FETCH: string,
    readonly API_FORM_STATS_FETCH_LINK: string,
    readonly API_FORM_OWNED_FETCH: string,
    readonly API_FORM_SHARED_FETCH: string,
    readonly API_FORM_PERMISSION_ACTIVE_FETCH: string,
    readonly API_FORM_PERMISSION_FETCH: string,
    readonly API_FORM_PERMISSION_UPDATE: string,
    readonly API_FORM_PERMISSION_GRANT: string,
    readonly API_FORM_PERMISSION_DENY: string,
    readonly API_FORM_PERMISSION_REQUEST: string,
    readonly API_FORM_PERMISSION_CANCEL: string,
    readonly API_FORM_ANSWER: string,
    readonly REGISTER_ENDPOINT: string,
    readonly SETUSERNAME_ENDPOINT: string,
    readonly USERNAMES_FETCH_ENDPOINT: string,
    readonly LOGIN_ENDPOINT: string,
    readonly LOGOUT_ENDPOINT: string,
    readonly LOGIN_CHECK_ENDPOINT: string,
    readonly USER_INFO_ENDPOINT: string,
    readonly USER_INFO_CHANGE_ENDPOINT: string,
    readonly FORM_STATS_PDF: string,
    readonly FORM_STATS_CSV: string,
    readonly FORM_STATS_TXT: string
};

let configData: ConnConfig = {
    host: "localhost",
    port: 8080,
    fullHost: "http://localhost:8080",
    oauth2Host: "http://localhost:8080"
};

const OAUTH2_BASE: string = configData.oauth2Host + "/oauth2/authorization";
const API_BASE: string = configData.fullHost + "/api";
const API_FORM_CREATION_BASE: string = API_BASE + "/form";
const API_FORM_DATA_BASE: string = API_BASE + "/data/form";
const API_FORM_PERMISSION_BASE: string = API_BASE + "/permissions";
const API_USER_BASE: string = API_BASE + "/user";
const API_STATISTICS_DATA_BASE: string = API_BASE + "/data/statistics";

let apiConfigData: ApiConfig = {
    OAUTH2_GOOGLE: OAUTH2_BASE + "/google",
    API_FORM_CREATE: API_FORM_CREATION_BASE + "/createForm",
    API_FORM_UPDATE: API_FORM_CREATION_BASE + "/updateFormMetadata",
    API_FORM_DELETE: API_FORM_CREATION_BASE + "/deleteForm",
    API_FORM_SAVE_OVER: API_FORM_CREATION_BASE + "/saveOver",
    API_FORM_SAVE_INTO: API_FORM_CREATION_BASE + "/saveInto",
    API_FORM_DATA_FETCH: API_FORM_DATA_BASE + "/get/",
    API_FORM_STATS_FETCH: API_STATISTICS_DATA_BASE,
    API_FORM_STATS_FETCH_LINK: API_STATISTICS_DATA_BASE + "/",
    API_FORM_OWNED_FETCH: API_FORM_DATA_BASE + "/ownedForms",
    API_FORM_SHARED_FETCH: API_FORM_DATA_BASE + "/sharedWithMe",
    API_FORM_PERMISSION_ACTIVE_FETCH: API_FORM_PERMISSION_BASE + "/active",
    API_FORM_PERMISSION_FETCH: API_FORM_PERMISSION_BASE + "/pending",
    API_FORM_PERMISSION_UPDATE: API_FORM_PERMISSION_BASE + "/update",
    API_FORM_PERMISSION_GRANT: API_FORM_PERMISSION_BASE + "/grant",
    API_FORM_PERMISSION_DENY: API_FORM_PERMISSION_BASE + "/deny",
    API_FORM_PERMISSION_REQUEST: API_FORM_PERMISSION_BASE + "/request",
    API_FORM_PERMISSION_CANCEL: API_FORM_PERMISSION_BASE + "/cancel",
    API_FORM_ANSWER: API_FORM_DATA_BASE + "/answer",
    REGISTER_ENDPOINT: API_BASE + "/register",
    SETUSERNAME_ENDPOINT: API_USER_BASE + "/setusername",
    USERNAMES_FETCH_ENDPOINT: API_USER_BASE + "/usernamesLike/",
    LOGIN_ENDPOINT: API_BASE + "/login",
    LOGOUT_ENDPOINT: API_BASE + "/logout",
    LOGIN_CHECK_ENDPOINT: API_BASE + "/isLoggedIn",
    USER_INFO_ENDPOINT: API_USER_BASE + "/info",
    USER_INFO_CHANGE_ENDPOINT: API_USER_BASE + "/update",
    FORM_STATS_PDF: API_STATISTICS_DATA_BASE + "/pdf",
    FORM_STATS_CSV: API_STATISTICS_DATA_BASE + "/csv",
    FORM_STATS_TXT: API_STATISTICS_DATA_BASE + "/txt"
}

const CONN_CONFIG = { configData, ...apiConfigData };
export default CONN_CONFIG;