import { ReactElement } from "react";
import { AnswerType } from "../../util/constants/Enums";
import { StatisticsEntryDTO } from "../../util/interfaces/dtos";
import PercentageBar from "./PercentageBar";

type StatisticsCardProps = {
    questionInfo: StatisticsEntryDTO,
    questionNumber: number
}

function StatisticsCard({
    questionInfo,
    questionNumber
}: StatisticsCardProps) {
    let participationCount: number = questionInfo.totalAnswers;
    let answeredCount: number = questionInfo.answered;

    function getAnswerType(answerTypeId: number) {
        switch (answerTypeId) {
            case AnswerType.SHORTTEXT: {
                return "Short text answer";
            }
            case AnswerType.LONGTEXT: {
                return "Long text answer";
            }
            case AnswerType.RADIO: {
                return "Single select answer";
            }
            case AnswerType.CHECKBOX: {
                return "Multi select answer";
            }
            case AnswerType.TRUEFALSE: {
                return "True/false answer";
            }
            case AnswerType.SCALE: {
                return "Scale answer";
            }
            case AnswerType.SCALEPERCENT: {
                return "Scale percent answer";
            }
            case AnswerType.NUMBER: {
                return "Number answer";
            }
        }
    }

    function generateStatistics(): ReactElement {
        let statisticsInfo: [title: string, percentageFull: number][] = [];

        if (questionInfo.answerType !== AnswerType.SHORTTEXT
            && questionInfo.answerType !== AnswerType.LONGTEXT) {
            questionInfo.answerToPartMap?.forEach((questionStatInfo, answerTitle) => {
                let fullTitle: string = `${answerTitle} (${questionStatInfo.answerCount})`;
                statisticsInfo.push([fullTitle, Math.round(questionStatInfo.answerPercentage * 100.)]);
            });
        }

        return (
            <>
                <div className="w-4/5 mb-5">
                    <PercentageBar percentFull={ (participationCount === 0) ? 0 : (Math.round((answeredCount / participationCount) * 100.)) } />
                </div>

                <hr className="border-gray-600/75 w-4/5 my-5" />

                {statisticsInfo.map(
                    (info, index) => (
                        <div className="flex flex-col w-4/5 mb-5">
                            <div className="mb-3">{info[0]}</div>
                            <PercentageBar percentFull={info[1]} />
                        </div>
                    )
                )}
            </>
        );
    }

    return (
        <div className="flex flex-col items-center justify-center w-full text-xl font-serif">
            <div className="flex flex-col md:flex-row md:items-center justify-between font-bold w-4/5">
                <div className="flex items-center mb-2 md:mb-0">
                    <div>{questionNumber}.</div>
                    <div className="ml-5">{questionInfo.question}</div>
                </div>
                <div className="flex items-center justify-end text-lg font-normal">
                    <div>Answer type:</div> <div className="ml-4">{getAnswerType(questionInfo.answerType)}</div>
                </div>
            </div>

            <hr className="border-gray-600/75 w-4/5 my-5" />

            {(questionInfo.answers) ? (
                <>
                <div className="max-h-[35vh] w-4/5 overflow-y-auto mt-5">
                    <div className="flex flex-col justify-center w-full">
                        {questionInfo.answers.filter(answ => answ !== "").map(
                            (answer, index) => (
                                <div className="flex items-center mb-5">
                                    <div className="text-center basis-2/12">{index + 1}.</div>
                                    <div className="basis-10/12 break-all text-justify">{answer}{questionInfo.answerType === AnswerType.SCALEPERCENT ? "%" : ""}</div>
                                </div>
                            )
                        )}
                    </div>
                </div>
                <hr className="border-gray-600/75 w-4/5 my-5" />
                </>
            ):""}


            <div className="w-4/5">
                Statistics:
            </div>

            <hr className="border-gray-600/75 w-4/5 my-5" />

            <div className="flex justify-between w-4/5 mb-5">
                <div className="flex basis-5/12">total participated: {participationCount}</div>
                <div className="flex basis-5/12">of which unanswered: {participationCount - answeredCount}</div>
            </div>

            {generateStatistics()}
        </div>
    );
}

export default StatisticsCard;