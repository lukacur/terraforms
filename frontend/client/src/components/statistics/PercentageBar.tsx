import classNames from "classnames";

type PercentageBarProps = {
    percentFull: number;
}

function PercentageBar({ percentFull }: PercentageBarProps) {
    const containerClass = classNames(
        "overflow-hidden rounded-2xl w-full h-4",
        "border border-gray-400/60 bg-gray-200/75"
    );

    const thumbClass = classNames(
        "bg-blue-600 h-full text-white flex items-center justify-center text-sm"
    );

    return (
        <div className={containerClass}>
            <div className={thumbClass} style={{width: `${percentFull}%`}}><div>{percentFull}%</div></div>
        </div>
    );
}

export default PercentageBar;