import { CheckIcon, XIcon } from "@heroicons/react/outline";
import { MinusCircleIcon, PlusCircleIcon } from "@heroicons/react/outline";
import classNames from "classnames";
import React from "react";
import { FormAccessType } from "../../util/constants/Enums";
import DATA_SERVER from "../../util/data/DataFetch";
import { AccessRequestDTO } from "../../util/interfaces/dtos";
import { FormFullLocal } from "../../util/interfaces/StoreData";
import InfoCard from "../universal/base/InfoCard";
import Button from "../universal/form/Button";

type PermissionManagerProps = {
    formInfo: FormFullLocal
}

function PermissionManager({
    formInfo
}: PermissionManagerProps) {
    const [requests, setRequests] = React.useState<AccessRequestDTO[]>([]);
    const [softReloadState, softReload] = React.useState<boolean>(false);

    const [possibleUsernames, setPossibleUsernames] = React.useState<string[]>([]);
    const [shownUsernames, setShownUsernames] = React.useState<string[]>([]);
    const [usernameListHidden, setUsernameListHidden] = React.useState<boolean>(true);

    const [startingPermissions, setStartingPermissions] = React.useState<AccessRequestDTO[]>([]);
    const [updatedPermissions, setUpdatedPermissions] = React.useState<AccessRequestDTO[]>(startingPermissions);

    const [grantingForUser, setGrantingForUser] = React.useState<{username: string, permission: number}>({ username: "", permission: -1 });

    const [infoCardShown, setInfoCardShown] = React.useState<boolean>(false);
    const [infoCardText, setInfoCardText] = React.useState<string>("");
    const [infoCardClassName, setInfoCardClassName] = React.useState<string>("");

    const [permissionTypeInvalid, setPermissionTypeInvalid] = React.useState<boolean>(false);
    const [usernameInvalid, setUsernameInvalid] = React.useState<boolean>(false);

    function getAccessTypeName(permissionId: number): string {
        switch (permissionId) {
            case FormAccessType.VIEW: {
                return "View";
            }

            case FormAccessType.EDIT: {
                return "Edit + View";
            }

            case FormAccessType.STAT: {
                return "Statistics";
            }

            default: {
                return "-";
            }
        }
    }

    function updateGrants() {
        setInfoCardClassName("bg-green-600");
        setInfoCardText("Changes saved");
        
        if (!infoCardShown) {
            setInfoCardShown(true);
            setTimeout(() => setInfoCardShown(false), 4000);
        }

        DATA_SERVER.updatePermissions(
            {
                formId: formInfo.form.formData.formId || (() => { throw new Error("This should never happen") })(),
                requests: updatedPermissions
            }
        )
        .then(() => {
            setStartingPermissions(updatedPermissions);
        });
    }

    function updateUsernames(value: string) {
        let newUsernames = [ ...possibleUsernames ];
        let regex = new RegExp(value);
        setShownUsernames(newUsernames.filter(un => regex.test(un)));
    }

    function updateGrant(event: any) {
        const { name, value } = event.target;

        if (name === "permission") {
            setGrantingForUser({...grantingForUser, [name]: parseInt(value)});
        } else {
            if ((value as string).length >= 3) {
                if (possibleUsernames.length === 0) {
                    DATA_SERVER.getUsernamesLike(value)
                    .then(usernames => {
                        setPossibleUsernames(usernames);
                        setShownUsernames(usernames);
                    });
                } else {
                    updateUsernames(value);
                }
            } else if ((value as string).length !== 0) {
                setPossibleUsernames([]);
                setShownUsernames([]);
            }

            setGrantingForUser({...grantingForUser, [name]: value});
        }
    }

    function focusUsernameInput(event: React.FocusEvent<HTMLInputElement, HTMLInputElement>) {
        const { value } = event.target;

        updateUsernames(value);
        setUsernameListHidden(false);
    }

    function blurUsernameInput() {
        setTimeout(() => setUsernameListHidden(true), 100);
    }

    React.useEffect(() => {
        DATA_SERVER.getPendingAccessRequests(formInfo)
        .then(result => {
            setRequests(result);
        })
        .catch(error => console.log(error));

        DATA_SERVER.getActivePermissions(formInfo)
        .then(result => {
            setStartingPermissions(result);
            setUpdatedPermissions(result);
        })
        .catch(error => console.log(error));
    }, [softReloadState, formInfo]);
    
    return (
        <div className="flex flex-col items-center justify-center">
            <div className="flex flex-col items-center justify-center w-[90%] my-10">
                <h1 className="text-center font-bold text-3xl lg:text-4xl">Permission manager</h1>
                <hr className="border-black/25 w-full my-10" />
                <h2 className="w-[90%] self-start text-lg lg:text-2xl">Manage form permissions below</h2>
                <hr className="border-black/25 w-full mt-10" />
            </div>

            <div className="w-[90%] mb-5">
                <h1 className="font-bold text-2xl">Active form permissions</h1>
            </div>

            <div className="flex flex-col lg:flex-row w-[90%]">
                <div className="relative px-2 basis-6/12 mb-4 lg:mb-0">
                    <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">Username</label>
                    <input
                        className="rounded-lg w-full outline-none border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] p-2"
                        type={"text"}
                        name={"username"}
                        autoComplete={"off"}
                        value={grantingForUser.username}
                        placeholder="User to grant permission for"
                        onChange={updateGrant}
                        onFocus={focusUsernameInput}
                        onBlur={blurUsernameInput}
                    />
                    <ul className="absolute z-50 bg-white w-[95%] list-none border border-black text-xl overflow-hidden" hidden={shownUsernames.length === 0 || usernameListHidden}>
                        {shownUsernames.map((un, index) => (
                            <li className="hover:bg-sky-100 py-1 px-2 cursor-pointer last:border-none border-b border-black" onClick={() => setGrantingForUser({ ...grantingForUser, username: un })}>
                                {un}
                            </li>
                        ))}
                    </ul>
                </div>

                <div className="relative px-2 basis-2/12 mr-8 mb-4 lg:mb-0">
                    <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">Access</label>
                    <select className="outline-none w-full border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] rounded-xl p-2 cursor-pointer" name={"permission"} value={grantingForUser.permission} onChange={updateGrant}>
                        <option value={-1}>-</option>
                        <option value={FormAccessType.VIEW}>View access</option>
                        <option value={FormAccessType.EDIT}>Edit + View access</option>
                        <option value={FormAccessType.STAT}>Statistics access</option>
                    </select>
                </div>

                <Button
                    className={"text-white bg-green-600 hover:bg-green-500 basis-1/12 w-[50%] mb-2 lg:mb-0 lg:w-[auto] lg:mr-2 self-center lg:self-auto"}
                    styling={{ paddedSmall: true, roundedFull: true }}
                    text=""
                    icon={<PlusCircleIcon className="w-6" />}
                    toolTip="Grant"
                    onclick={
                        () => {
                            let tmp: AccessRequestDTO[] = updatedPermissions;

                            if (grantingForUser.permission === -1) {
                                setPermissionTypeInvalid(true);
                                setTimeout(() => setPermissionTypeInvalid(false), 2000);
                            } else if (grantingForUser.username.length < 5) {
                                setUsernameInvalid(true);
                                setTimeout(() => setUsernameInvalid(false), 2000);
                            } else if (tmp.filter(el => el.accessType === grantingForUser.permission && el.usernameRequested === grantingForUser.username).length === 0) {
                                setUpdatedPermissions([...updatedPermissions, { usernameRequested: grantingForUser.username, accessType: grantingForUser.permission, formId: formInfo.form.formData.formId??(() => { throw new Error("This should never happen")})() }]);
                            }
                        }
                    }
                />

                <Button
                    className={"text-white bg-red-600 hover:bg-red-500 basis-1/12 w-[50%] mb-2 lg:mb-0 lg:w-[auto] lg:ml-2 self-center lg:self-auto"}
                    styling={{ paddedSmall: true, roundedFull: true }}
                    text=""
                    icon={<MinusCircleIcon className="w-6" />}
                    toolTip="Revoke"
                    onclick={
                        () => {
                            let tmp: AccessRequestDTO[] = updatedPermissions;

                            setUpdatedPermissions(tmp.filter(el => el.accessType !== grantingForUser.permission || el.usernameRequested !== grantingForUser.username));
                        }
                    }
                />
            </div>

            {(permissionTypeInvalid) ? <span className="w-[90%] mt-2 text-sm text-red-600">Please select a valid access type</span>:""}
            {(usernameInvalid) ? <span className="w-[90%] mt-2 text-sm text-red-600">Please enter a valid username (at least 5 characters long)</span>:""}

            <div className="scrollable overflow-auto w-[90%] h-[35vh] my-10">
                <div className="grid grid-cols-2 items-center justify-items-start">
                    <p className="sticky top-0 bg-white w-full h-full pb-2">Username</p>
                    <p className="sticky top-0 bg-white w-full h-full pb-2">Permission</p>

                    {(updatedPermissions.length === 0) ? (
                        <>
                            <hr className="border-black/25 w-full my-4 col-start-1 col-end-4" />

                            <div className="col-start-1 col-end-3 text-center text-gray-600 mt-8 justify-self-center">
                                You haven't defined any access rights for this form yet
                            </div>
                        </>
                    ):""}

                    {updatedPermissions.map((grantedPerm, index) => {
                        const rowClasses = classNames(
                            "w-full h-full border-t border-black/25 py-4",
                            {
                                "bg-gray-200": (index % 2) === 0
                            }
                        );

                        return (
                            <>
                                <p className={classNames("pl-2", rowClasses)}>{grantedPerm.usernameRequested}</p>
                                <p className={rowClasses}>{getAccessTypeName(grantedPerm.accessType)}</p>
                            </>
                        );
                    })}
                </div>
            </div>

            <div className="flex flex-col lg:flex-row w-[90%] text-lg justify-around mb-20">
                <Button
                    className={"bg-orange-700 hover:bg-[#F18F01] text-white mb-2 lg:mb-0"}
                    styling={{paddedMedium: true, roundedPartial: true}}
                    text="Save changes"
                    onclick={updateGrants}
                />

                <Button
                    className={"bg-red-600 hover:bg-red-500 text-white"}
                    styling={{paddedMedium: true, roundedPartial: true}}
                    text="Discard changes"
                    onclick={
                        () => {
                            setInfoCardClassName("bg-red-600");
                            setInfoCardText("Changes discarded");

                            if (!infoCardShown) {
                                setInfoCardShown(true);
                                setTimeout(() => setInfoCardShown(false), 4000);
                            }

                            setUpdatedPermissions(startingPermissions);
                        }
                    }
                />
            </div>

            <div className="w-[90%] mb-5">
                <h1 className="font-bold text-2xl">Pending requests</h1>
            </div>

            <div className="scrollable overflow-auto w-[90%] h-[35vh] mb-10">
                <div className="grid grid-cols-3 items-center">
                    <p className="sticky top-0 bg-white w-full h-full pb-2">Username</p>
                    <p className="sticky top-0 bg-white w-full h-full pb-2 text-center">Permission requested</p>
                    <div className="sticky top-0 bg-white w-full h-full pb-2 text-right pr-2">Action</div>

                    {(requests.length === 0) ? (
                        <>
                            <hr className="border-black/25 w-full my-4 col-start-1 col-end-4" />

                            <div className="col-start-1 col-end-4 text-center text-gray-600 mt-8">
                                No pending requests for this form
                            </div>
                        </>
                    ):""}

                    {requests.map((req, index) => {
                        const rowClasses = classNames(
                            "w-full h-full border-t border-black/25 py-4",
                            {
                                "bg-gray-200": (index % 2) === 0
                            }
                        );

                        return (
                            <>
                                <p className={classNames("flex items-center justify-self-start pl-2", rowClasses)}>{req.usernameRequested}</p>
                                <p className={classNames("flex items-center justify-center justify-self-center text-center", rowClasses)}>{getAccessTypeName(req.accessType)}</p>
                                <div className={classNames("flex flex-col lg:flex-row justify-self-end justify-end pr-2", rowClasses)}>
                                    <Button
                                        className={"bg-green-600 hover:bg-green-500 text-white mb-1 lg:mb-0 lg:mr-1 px-2 lg:px-4 py-1"}
                                        styling={{ roundedPartial: true }}
                                        text=""
                                        icon={<CheckIcon className="w-6" />}
                                        toolTip="Accept"
                                        onclick={
                                            () => {
                                                setInfoCardClassName("bg-green-600");
                                                setInfoCardText("Request accepted");

                                                if (!infoCardShown) {
                                                    setInfoCardShown(true);
                                                    setTimeout(() => setInfoCardShown(false), 4000);
                                                }
                                                
                                                DATA_SERVER.grantPermission(req)
                                                .then(() => {
                                                    let tmp = requests;
                                                    tmp.splice(index, 1);

                                                    setRequests([...tmp]);
                                                    softReload(!softReloadState);
                                                });
                                            }
                                        }
                                    />

                                    <Button
                                        className={"bg-red-600 hover:bg-red-500 text-white lg:ml-1 px-2 lg:px-4 py-1"}
                                        styling={{ roundedPartial: true }}
                                        text=""
                                        icon={<XIcon className="w-6" />}
                                        toolTip="Deny"
                                        onclick={
                                            () => {
                                                setInfoCardClassName("bg-red-600");
                                                setInfoCardText("Request denied");

                                                if (!infoCardShown) {
                                                    setInfoCardShown(true);
                                                    setTimeout(() => setInfoCardShown(false), 4000);
                                                }

                                                DATA_SERVER.denyPermission(req)
                                                .then(() => {
                                                    let tmp = requests;
                                                    tmp.splice(index, 1);

                                                    setRequests([...tmp]);
                                                });
                                            }
                                        }
                                    />
                                </div>
                            </>
                        );
                    })}
                </div>
            </div>

            {(infoCardShown) ? (
                <InfoCard
                    text={infoCardText}
                    className={infoCardClassName}
                />
            ):""}
        </div>
    );
}

export default PermissionManager;