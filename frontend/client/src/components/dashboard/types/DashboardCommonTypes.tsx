import { FormFullLocal } from "../../../util/interfaces/StoreData"

export type FormMenuCallbacks = {
    formSettingsCallback: (formInfo: FormFullLocal) => void,
    managePermissionsCallback: (formInfo: FormFullLocal) => void,
    cloneCallback: (formInfo: FormFullLocal) => void,
    formDeleteCallback: (formInfo: FormFullLocal) =>void
}