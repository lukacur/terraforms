import { CheckCircleIcon, XCircleIcon } from "@heroicons/react/solid";
import classNames from "classnames";
import React from "react";
import { FormRestrictionType } from "../../util/constants/Enums";
import { FormFullLocal } from "../../util/interfaces/StoreData";
import Button from "../universal/form/Button";

type EditorType = {
    create?: boolean,
    clone?: boolean,
    edit?: boolean
}

type FormInfoEditorProps = {
    form: FormFullLocal,
    editorType: EditorType,
    confirmActionCallback: (finalFormData: FormFullLocal) => void,
    cancelActionCallback: () => void
}

function FormInfoEditor({
    form,
    editorType,
    confirmActionCallback,
    cancelActionCallback
}: FormInfoEditorProps) {
    const titleRef = React.useRef<HTMLInputElement>(null);
    const [titleInvalid, setTitleInvalid] = React.useState<boolean>(false);

    const descriptionRef = React.useRef<HTMLTextAreaElement>(null);
    const [descriptionInvalid, setDescriptionInvalid] = React.useState<boolean>(false);

    const accessRef = React.useRef<HTMLSelectElement>(null);
    const [accessInvalid, setAccessInvalid] = React.useState<boolean>(false);

    const [fieldState, setFieldState] = React.useState<FormFullLocal>(
        {
            localFormId: form.localFormId,
            form: {
                formData: form.form.formData,
                bodyData: form.form.bodyData
            }
        }
    );

    function updateForm(event: any): void {
        const { name, value } = event.target;

        let tmp: FormFullLocal = { ...fieldState };
        tmp.form.formData = { ...(tmp.form.formData), [name]: value };

        setFieldState(tmp);
    }

    function getTitle(): string {
        if (editorType.create) {
            return "Create a new form";
        } else if (editorType.clone) {
            return "Clone form";
        } else if (editorType.edit) {
            return "Edit form";
        }

        return "";
    }

    function getDescription(): string {
        if (editorType.create) {
            return "Enter all information below to create a new form.";
        } else if (editorType.clone) {
            return "Clone the form you selected and all it's questions to your 'Owned forms' section.";
        } else if (editorType.edit) {
            return "Edit the form information below.";
        }

        return "";
    }

    function getConfirmText(): string {
        if (editorType.create) {
            return "Create form";
        } else if (editorType.clone) {
            return "Clone form";
        } else if (editorType.edit) {
            return "Update form settings";
        }

        return "";
    }

    function runCheck(event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) {
        if (event.target === titleRef.current) {
            if (titleRef.current.value.length === 0) {
                titleRef.current.setCustomValidity("This field is required");
                setTitleInvalid(true);
            } else {
                titleRef.current?.setCustomValidity("");
                setTitleInvalid(false);
            }
        }

        if (event.target === descriptionRef.current) {
            if (descriptionRef.current.value.length === 0) {
                descriptionRef.current.setCustomValidity("This field is required");
                setDescriptionInvalid(true);
            } else {
                descriptionRef.current?.setCustomValidity("");
                setDescriptionInvalid(false);
            }
        }

        if (event.target === accessRef.current) {
            let { value } = event.target;

            if (value == "-1") {
                accessRef.current.setCustomValidity("Please select a valid access type");
                setAccessInvalid(true);
            } else {
                accessRef.current?.setCustomValidity("");
                setAccessInvalid(false);
            }
        }
    }

    const inputContainerClasses: string = classNames(
        "relative w-[90%] px-2 mb-10"
    );

    const titleInputClasses: string = classNames(
        "rounded-lg w-full outline-none border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] p-2",
        "invalid:border-red-600 invalid:focus:border-red-600  invalid:focus:shadow-md invalid:focus:shadow-red-300"
    );

    const descriptionInputClasses: string = classNames(
        "scrollable text-justify resize-none w-full outline-none border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] p-2 rounded-lg",
        "invalid:border-red-600 invalid:focus:border-red-600  invalid:focus:shadow-md invalid:focus:shadow-red-300"
    );

    const accessInputClass: string = classNames(
        "outline-none w-full border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] rounded-xl p-2 cursor-pointer",
        "invalid:border-red-600 invalid:focus:border-red-600  invalid:focus:shadow-md invalid:focus:shadow-red-300"
    );

    return (
        <div className="flex flex-col items-center justify-center">
            <div className="flex flex-col items-center justify-center w-[90%] my-10">
                <h1 className="text-center font-bold text-3xl md:text-4xl">{getTitle()}</h1>
                <hr className="border-black/25 w-full my-10" />
                <h2 className="w-[90%] self-start text-2xl">{getDescription()}</h2>
                <hr className="border-black/25 w-full mt-10" />
            </div>

            <div className={inputContainerClasses}>
                <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">Form title</label>
                <input
                    ref={titleRef}
                    className={titleInputClasses}
                    type={"text"}
                    name={"title"}
                    value={fieldState.form.formData.title}
                    placeholder="The form's title"
                    onChange={updateForm}
                    onBlur={runCheck}
                />
                {(titleInvalid) ? <span className="text-red-600 text-sm">{titleRef.current?.validationMessage}</span> :""}
            </div>

            <div className={inputContainerClasses}>
                <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">Form description</label>
                <textarea
                    ref={descriptionRef}
                    className={descriptionInputClasses}
                    name="description"
                    rows={6}
                    value={fieldState.form.formData.description}
                    placeholder="Write some text that describes and helps users determine the context of the form"
                    onChange={updateForm}
                    onBlur={runCheck}
                />
                {(descriptionInvalid) ? <span className="text-red-600 text-sm">{descriptionRef.current?.validationMessage}</span> :""}
            </div>

            <div className={inputContainerClasses}>
                <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">Form restriction</label>
                <select className={accessInputClass} name={"restrictedTo"} value={fieldState.form.formData.restrictedTo} ref={accessRef} onChange={updateForm} onBlur={runCheck}>
                    <option value={-1}>-</option>
                    <option value={FormRestrictionType.NONE}>No restriction</option>
                    <option value={FormRestrictionType.LOGGED_IN}>Only registered users</option>
                    <option value={FormRestrictionType.SELECTED}>Only selected registered users</option>
                </select>
                {(accessInvalid) ? <span className="text-red-600 text-sm">{accessRef.current?.validationMessage}</span> :""}
            </div>

            <div className="w-full flex flex-col md:flex-row items-center justify-around mb-10">
                <Button
                    className={"text-white bg-green-500 hover:bg-green-400 basis-2/5 px-8 py-2 mb-4 md:mb-0"}
                    styling={{roundedPartial: true}}
                    text={getConfirmText()}
                    icon={<CheckCircleIcon className="w-6" />}
                    onclick={
                        () => {
                            let invalid: boolean = titleInvalid || descriptionInvalid;

                            if (!invalid) {
                                confirmActionCallback(fieldState);
                            }
                        }
                    }
                />

                <Button
                    className={"text-white bg-red-500 hover:bg-red-400 basis-2/5 px-8 py-2"}
                    styling={{roundedPartial: true}}
                    text="Cancel"
                    icon={<XCircleIcon className="w-6" />}
                    onclick={
                        () => {
                            cancelActionCallback();
                        }
                    }
                />
            </div>
        </div>
    );
}

export default FormInfoEditor;