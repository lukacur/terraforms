import { CheckIcon, InformationCircleIcon } from "@heroicons/react/solid";
import React from "react";
import { FormAccessType } from "../../util/constants/Enums";
import { HttpStatus } from "../../util/constants/HttpStatus";
import DATA_SERVER from "../../util/data/DataFetch";
import { PermissionRequestDTO } from "../../util/interfaces/dtos";
import Button from "../universal/form/Button";
import TextModal from "../universal/modals/TextModal";

type FormAccessRequestProps = {
    modalCloseFunction: () => void
}

function FormAccessRequest({
    modalCloseFunction
}: FormAccessRequestProps) {
    const [infoModal, setInfoModal] = React.useState<boolean>(false);
    const [requestInfo, setRequestInfo] = React.useState<PermissionRequestDTO>({formLink: "", accessType: -1});
    const [infoText, setInfoText] = React.useState<string>("");
    const [infoTitle, setInfoTitle] = React.useState<string>("");

    function updateRequest(event: any): void {
        const { name, value } = event.target;

        setRequestInfo({...requestInfo, [name]: value});
    }

    function submitRequest() {
        DATA_SERVER.requestPermission(requestInfo)
        .then(response => {
            if (response.status !== HttpStatus.OK) {
                setInfoText("You entered an invalid form link. Check the form link and try again.");
                setInfoTitle("Access request unsuccessful");
            } else {
                setInfoText("The owner of the form has been notified. When they accept the request, the form will be visible under the 'Shared with me' section of your dashboard");
                setInfoTitle("Access request successful");
            }
            setInfoModal(true);
        })
        .catch(error => {
            setInfoTitle("Access request unsuccessful");
            setInfoText("Form doesn't exist, form link invalid or access already requested/granted.");
            setInfoModal(true);
        });
    }

    return (
        <div className="flex flex-col items-center">
            <div className="flex flex-col items-center justify-center w-[90%] my-10">
                <h1 className="text-center font-bold text-2xl md:text-3xl">Request access for a form</h1>
                <hr className="border-black/25 w-full my-10" />
                <h2 className="w-[90%] self-start text-lg md:text-xl text-justify">Enter the link of the form you want to request access to below</h2>
                <hr className="border-black/25 w-full mt-10" />
            </div>

            <div className="flex flex-col md:flex-row justify-center w-[90%]">
                <div className="relative px-2 basis-9/12">
                    <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">Form link</label>
                    <input
                        className="rounded-lg w-full outline-none border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] p-2 mb-10"
                        type={"text"}
                        name={"formLink"}
                        value={requestInfo.formLink}
                        onChange={updateRequest}
                    />
                </div>

                <div className="relative px-2 basis-3/12">
                    <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">Access</label>
                    <select className="outline-none w-full border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] rounded-xl p-2 cursor-pointer" name={"accessType"} value={requestInfo.accessType} onChange={updateRequest}>
                        <option value={-1}>-</option>
                        <option value={FormAccessType.VIEW}>View access</option>
                        <option value={FormAccessType.EDIT}>Edit + View access</option>
                        <option value={FormAccessType.STAT}>Statistics access</option>
                    </select>
                </div>
            </div>

            <div className="w-full flex flex-row items-center justify-around my-10">
                <Button
                    className={"text-white bg-green-500 hover:bg-green-400 basis-2/5"}
                    styling={{paddedMedium: true, roundedPartial: true}}
                    text="Submit request"
                    icon={<CheckIcon className="w-8 md:w-6" />}
                    onclick={submitRequest}
                />
            </div>

            {(infoModal) ? (
                <TextModal
                    modalCloseFunction={() => setInfoModal(false)}
                    modalOptions={[<Button width="[50%]" styling={{paddedSmall: true, roundedPartial: true}} text="OK" onclick={() => setInfoModal(false)} />]}
                    modalText={infoText}
                    modalTitle={infoTitle}
                    modalIcon={<InformationCircleIcon className="text-blue-300" />}
                />
            ):""}
        </div>
    );
}

export default FormAccessRequest;