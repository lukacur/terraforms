import classNames from "classnames";
import React from "react";
import Menu from "../universal/dropdown/Menu";
import "../universal/modals/Modals.css";
import { FormMenuCallbacks } from "./types/DashboardCommonTypes";
import { useNavigate } from "react-router-dom";
import STORE from "../../util/store/Store";
import { FormFullLocal } from "../../util/interfaces/StoreData";
import InfoCard from "../universal/base/InfoCard";
import DATA_SERVER from "../../util/data/DataFetch";

type FormCardProps = {
    formFullInfo: FormFullLocal,
    menuCallbacks: FormMenuCallbacks,
    formOwner: boolean,
    isTempStored: boolean
}

function FormCard({
    formFullInfo,
    menuCallbacks,
    formOwner,
    isTempStored
}: FormCardProps) {
    const navigate = useNavigate();
    const [menuHidden, setMenuHidden] = React.useState<boolean>(true);

    const [infoCardClassName, setInfoCardClassName] = React.useState<string>("");
    const [infoCardText, setInfoCardText] = React.useState<string>("");
    const [infoCardShown, setInfoCardShown] = React.useState<boolean>(false);
    
    const formCardClasses: string = classNames(
        "w-4/5",
        "rounded-3xl px-4",
        "max-h-[auto] min-h-[33vh] my-4",
        "shadow-lg shadow-gray-500",
        "flex flex-col justify-center items-center group relative cursor-default",
        {
           "h-[40%] overflow-hidden hover:overflow-visible hover:shadow-xl hover:shadow-gray-700 hover:h-[80%] hover:bg-gray-100": matchMedia("(any-pointer: fine").matches,
           "h-[80%]": !matchMedia("(any-pointer: fine").matches
        }
    );

    const menuWrapperClasses: string = classNames(
        "absolute top-4 right-4 overflow-visible",
        {
            "group-hover:visible invisible": matchMedia("(any-pointer: fine").matches
        }
    );

    const separatorClasses: string = classNames(
        "border-black/25 w-[90%]",
        {
            "group-hover:visible invisible": matchMedia("(any-pointer: fine)").matches
        }
    );

    const formDescriptionClasses: string = classNames(
        "text-justify",
        "w-[90%] m-5 px-4",
        "scrollable",
        {
           "overflow-hidden invisible group-hover:overflow-scroll group-hover:overflow-x-hidden group-hover:visible": matchMedia("(any-pointer: fine").matches,
           "overflow-scroll overflow-x-hidden": !matchMedia("(any-pointer: fine)").matches
        }
    );

    function copyLink(isStatLink: boolean) {
        let link: string | null = (isStatLink) ? formFullInfo.form.formData.statsLink : formFullInfo.form.formData.formLink;

        if (link) {
            navigator.clipboard.writeText(link);

            setInfoCardClassName("bg-green-500 border-green-600/50");
            setInfoCardText(`Form ${(isStatLink) ? "stats " : ""}link copied to clipboard!`);
            
            if (!infoCardShown) {
                setInfoCardShown(true);
                setTimeout(() => setInfoCardShown(false), 4000);
            }
        }
    }

    function openStats() {
        DATA_SERVER.fetchFormStatistics(formFullInfo.form.formData)
        .then(stats => {
            document.body.classList.add("cursor-wait");
            STORE.setCurrStatistics(stats)
            .then(() => {
                document.body.classList.remove("cursor-wait");
                navigate("/statistics");
            })
            .catch(err => console.log(err))
            .finally(() => document.body.classList.remove("cursor-wait"));
        });
    }

    function getMenuOptions(): [string, () => void, string | undefined, boolean | undefined][] {
        let opts: [string, () => void, string | undefined, boolean | undefined][] = [
            ["Clone...", () => menuCallbacks.cloneCallback({ ...formFullInfo }), "", true]
        ];

        let formLinkLast = true;
        if (formFullInfo.form.formData.statsLink && !isTempStored) {
            formLinkLast = false;
            opts.unshift(
                ["Get statistics link", () => copyLink(true), "", false],
                ["Open statistics", openStats, "", true]
            );
        }

        if (formFullInfo.form.formData.formLink && !isTempStored) {
            opts.unshift(["Get form link", () => copyLink(false), "", formLinkLast]);
        }

        if (isTempStored || (formOwner && !isTempStored)) {
            opts.push(["Discard changes", () => menuCallbacks.formDeleteCallback({ ...formFullInfo }), "hover:bg-red-500/90 hover:text-white", undefined]);
        }

        if (formOwner && !isTempStored) {
            opts.unshift(["Manage permissions", () => menuCallbacks.managePermissionsCallback({ ...formFullInfo }), undefined, true]);

            if (!isTempStored) {
                (opts[opts.length - 1])[0] = "Delete form";
            }
        } else if (!isTempStored) {
            opts.push(["Stop participating", () => menuCallbacks.formDeleteCallback({ ...formFullInfo }), "hover:bg-red-500/90 hover:text-white", undefined]);
        }

        opts.unshift(["Form settings", () => menuCallbacks.formSettingsCallback({ ...formFullInfo }), undefined, undefined]);

        return opts;
    }

    return (
        <>
            <div
                className={formCardClasses}
                onClick={
                    () => {
                        if (!menuHidden) {
                            setMenuHidden(true);
                        } else {
                            STORE.getFromEditingForms(formFullInfo.localFormId)
                            .then(localForm => {
                                STORE.setCurrEditForm(localForm || formFullInfo)
                                .then(() => navigate("/editor"));
                            });
                        }
                    }
                }
                title={"Click to edit"}
                onMouseLeave={() => setMenuHidden(true)}
                onMouseEnter={() => setMenuHidden(true)}
            >
                <div className={menuWrapperClasses}>
                    <Menu
                        horizontalAlignment={{ custom: "-left-44 md:-left-42" }}
                        menuOptions={getMenuOptions()}
                        hidden={menuHidden}
                        menuOpenCallback={() => setMenuHidden(false)}
                        menuCloseCallback={() => setMenuHidden(true)}
                    />
                </div>
                <h1 className="text-2xl group-hover:font-bold my-5">{formFullInfo.form.formData.title}</h1>
                <hr className={separatorClasses} />
                <div className={formDescriptionClasses}>{formFullInfo.form.formData.description}</div>
            </div>

            {(infoCardShown) ? (
                <InfoCard
                    className={infoCardClassName}
                    text={infoCardText}
                />
            ):""}
        </>
    );
}

export default FormCard;