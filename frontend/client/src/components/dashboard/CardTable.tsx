import FormCard from "./FormCard";
import "../universal/modals/Modals.css";
import React from "react";
import Button from "../universal/form/Button";
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/outline";
import { FormMenuCallbacks } from "./types/DashboardCommonTypes";
import { FormFullLocal } from "../../util/interfaces/StoreData";
import classNames from "classnames";

type CardTableProps = {
    forms: FormFullLocal[],
    formCardMenuCallbacks: FormMenuCallbacks,
    ownsForms: boolean,
    tempStore: boolean
}

const ITEMS_PER_PAGE: number = 12;

function CardTable({
    forms,
    formCardMenuCallbacks,
    ownsForms,
    tempStore
}: CardTableProps) {
    const [currentPage, setCurrentPage] = React.useState<number>(1);
    let pagesMap: Map<number, FormFullLocal[]> = new Map<number, FormFullLocal[]>();

    let pages: number = Math.ceil(forms.length / ITEMS_PER_PAGE);
    let formsLeft: FormFullLocal[] = forms.slice(0);

    for (let i: number = 0; i < pages; i++) {
        pagesMap.set(i + 1, (formsLeft.length <= ITEMS_PER_PAGE) ? formsLeft : formsLeft.splice(0, ITEMS_PER_PAGE));
    }

    const tableClasses: string = classNames(
        "grid md:grid-cols-3 sm:grid-cols-1 lg:grid-cols-4 justify-items-center items-center w-full"
    );

    return (
        <div className="w-full">
            {(pages > 1) ? (<div className="flex flex-row w-full justify-between px-16 mb-5">
                <Button
                    className={"w-6"}
                    styling={{}}
                    text=""
                    toolTip="Previous page"
                    icon={<ChevronLeftIcon className="w-full" />}
                    enabled={currentPage > 1}
                    onclick={
                        () => {
                            if (currentPage > 1) {
                                setCurrentPage(currentPage - 1);
                            }
                        }
                    }
                />
                <div>{currentPage} / {pages}</div>
                <Button
                    className={"w-6"}
                    styling={{}}
                    text=""
                    toolTip="Next page"
                    icon={<ChevronRightIcon className="w-full" />}
                    enabled={currentPage < pages}
                    onclick={
                        () => {
                            if (currentPage < pages) {
                                setCurrentPage(currentPage + 1);
                            }
                        }
                    }
                />
            </div>):""}

            <div className={tableClasses}>
                {pagesMap.get(currentPage)?.map((form, index) => {
                    return (
                        <FormCard
                            isTempStored={tempStore}
                            formOwner={ownsForms}
                            formFullInfo={form}
                            key={`${form.localFormId}__${index}`}
                            menuCallbacks={formCardMenuCallbacks}
                        />
                    );
                })}
            </div>

            {(pages > 1) ? (<div className="flex flex-row w-full justify-between px-16 mb-5">
                <Button
                    className={"w-6"}
                    styling={{}}
                    text=""
                    toolTip="Previous page"
                    icon={<ChevronLeftIcon className="w-full" />}
                    enabled={currentPage > 1}
                    onclick={
                        () => {
                            if (currentPage > 1) {
                                setCurrentPage(currentPage - 1);
                            }
                        }
                    }
                />
                <div>{currentPage} / {pages}</div>
                <Button
                    className={"w-6"}
                    styling={{}}
                    text=""
                    toolTip="Next page"
                    icon={<ChevronRightIcon className="w-full" />}
                    enabled={currentPage < pages}
                    onclick={
                        () => {
                            if (currentPage < pages) {
                                setCurrentPage(currentPage + 1);
                            }
                        }
                    }
                />
            </div>):""} 
        </div>
    );
}

export default CardTable;