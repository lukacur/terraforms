import classNames from "classnames";
import React, { ReactElement } from "react";
import { AnswerType } from "../../util/constants/Enums";
import { QuestionDTO } from "../../util/interfaces/dtos";
import Button from "../universal/form/Button";

type AnswerCardProps = {
    questionInfo: QuestionDTO,
    answered?: string,
    valueUpdateCallback: (newValue: string) => void
}

function AnswerCard({
    questionInfo,
    answered,
    valueUpdateCallback
}: AnswerCardProps) {
    const [storedValue, setStoredValue] = React.useState(answered);

    function updateValue(event: any) {
        const { value } = event.target;

        setStoredValue(value);
        valueUpdateCallback(value);
    }

    function yieldInputElement(): ReactElement {
        switch(questionInfo.answerType) {
            case AnswerType.LONGTEXT: {
                return (
                    <textarea
                        className="resize-none w-[75%] outline-none border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] rounded-xl p-2"
                        name={`quest_${questionInfo.questionNumber}_answer_bigtext`}
                        value={storedValue??""}
                        onChange={updateValue}
                    />
                );
            }

            case AnswerType.SHORTTEXT: {
                return (
                    <input
                        className="w-[75%] outline-none border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] rounded-xl p-2"
                        type={"text"}
                        name={`quest_${questionInfo.questionNumber}_answer_smalltext`}
                        placeholder="Enter your answer here"
                        value={storedValue??""}
                        onChange={updateValue}
                    />
                );
            }

            case AnswerType.CHECKBOX:
            case AnswerType.RADIO: {
                return (
                    <>
                        {questionInfo.possibleAnswers.map((answer, index) => {
                            return (
                                <div className="flex flex-row items-baseline content-center" key={index}>
                                    <input
                                        type={(questionInfo.answerType === AnswerType.CHECKBOX) ? "checkbox" : "radio"}
                                        name={`quest_${questionInfo.questionNumber}_answer_multi`}
                                        value={answer.value}
                                        onChange={
                                            (ev: any) => {
                                                if (questionInfo.answerType !== AnswerType.CHECKBOX || !storedValue) {
                                                    updateValue(ev);
                                                } else {
                                                    const { value } = ev.target;

                                                    if (storedValue) {
                                                        let tmp = storedValue.split(";");
                                                        
                                                        if (ev.target.checked) {
                                                            tmp.push(value);
                                                        } else {
                                                            tmp.splice(tmp.indexOf(value), 1);
                                                        }

                                                        ev.target.value = tmp.join(";");
                                                        updateValue(ev);
                                                    }
                                                }
                                            }
                                        }
                                        onClick={
                                            (ev: any) => {
                                                if (questionInfo.answerType === AnswerType.CHECKBOX) {
                                                    return;
                                                }

                                                const { value } = ev.target;

                                                if (storedValue == value) {
                                                    ev.target.value = "";
                                                    updateValue(ev);
                                                }
                                            }
                                        }
                                        className="basis-1/12 justify-self-end h-4"
                                        checked={
                                            (() => {
                                                if (storedValue) {
                                                    if (questionInfo.answerType === AnswerType.RADIO) {
                                                        return storedValue == answer.value;
                                                    }

                                                    return storedValue.split(";").indexOf(answer.value.toString()) !== -1;
                                                }

                                                return false;
                                            })()
                                        }
                                    />
                                    <div className="basis-11/12 ml-2">{answer.name}</div>
                                </div>
                            );
                        })}
                    </>
                );
            }

            case AnswerType.NUMBER: {
                return (
                    <input
                        className="outline-none w-[50%] text-center overflow-y-hidden border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] rounded-xl px-2"
                        type={"number"}
                        name={`quest_${questionInfo.questionNumber}_answer_number`}
                        onChange={updateValue}
                        value={storedValue??""}
                    />
                );
            }

            case AnswerType.TRUEFALSE: {
                var trueButton: any;
                var falseButton: any;

                let clickTrue = (event: React.MouseEvent<HTMLButtonElement, MouseEvent> | undefined): void => {
                    if (!trueButton) {
                        trueButton = event?.target;
                        
                        while (!trueButton.classList.contains("true-button-identifier")) {
                            trueButton = trueButton.parentElement;
                        }
                    }

                    if (storedValue === "true") {
                        if (event) {
                            (event as any).target.value = "";
                            updateValue(event);
                            return;
                        }
                    }
                    
                    trueButton.classList.remove("bg-green-600");
                    trueButton.classList.add("bg-green-400");
                    trueButton.classList.add("outline");

                    if (falseButton) {
                        falseButton.classList.add("bg-red-600");
                        falseButton.classList.remove("bg-red-400");
                        falseButton.classList.remove("outline");
                    }

                    if (event) {
                        (event as any).target.value = "true";
                        updateValue(event);
                    }
                };

                let clickFalse = (event: React.MouseEvent<HTMLButtonElement, MouseEvent> | undefined): void => {
                    if (!falseButton) {
                        falseButton = event?.target;

                        while (!falseButton.classList.contains("false-button-identifier")) {
                            falseButton = falseButton.parentElement;
                        }
                    }
                    
                    if (storedValue === "false") {
                        if (event) {
                            (event as any).target.value = "";
                            updateValue(event);
                            return;
                        }
                    }

                    falseButton.classList.remove("bg-red-600");
                    falseButton.classList.add("bg-red-400");
                    falseButton.classList.add("outline");

                    if (trueButton) {
                        trueButton.classList.add("bg-green-600");
                        trueButton.classList.remove("bg-green-400");
                        trueButton.classList.remove("outline");
                    }

                    if (event) {
                        (event as any).target.value = "false";
                        updateValue(event);
                    }
                };

                return (
                    <div className="flex flex-col md:flex-row">
                        <Button
                            className={classNames(
                                "true-button-identifier bg-green-600 rounded-xl md:rounded-r-none mb-4 md:mb-0 text-white hover:bg-green-400 mr-[0.0625rem]",
                                {
                                    "bg-green-600 hover:bg-green-400": storedValue !== "true",
                                    "bg-green-400 hover:bg-green-400 outline": storedValue === "true"
                                },
                                "outline-2 outline-black"
                            )}
                            styling={{paddedLarge: true}}
                            text={"True"}
                            onclick={clickTrue}
                        />
                        <Button
                            className={classNames(
                                "false-button-identifier bg-red-600 rounded-xl md:rounded-l-none text-white hover:bg-red-400",
                                {
                                    "bg-red-600 hover:bg-red-400": storedValue !== "false",
                                    "bg-red-400 hover:bg-red-400 outline": storedValue === "false"
                                },
                                "outline-2 outline-black"
                            )}
                            styling={{paddedLarge: true}}
                            text={"False"}
                            onclick={clickFalse}
                        />
                    </div>
                );
            }

            case AnswerType.SCALE: {
                return (
                    <div className="flex flex-col md:flex-row justify-around">
                        {questionInfo.possibleAnswers.map((answer, index) => {
                            return (
                                <div className="grid grid-rows-2 grid-cols-1 items-center justify-items-center px-1 first:border-none basis-full border-l border-black" key={index}>
                                    <div className="text-center">{answer.name}</div>
                                    <input
                                        className="w-4 h-4"
                                        type={"radio"}
                                        name={`quest_${questionInfo.questionNumber}_answer_scale`}
                                        value={answer.value}
                                        onChange={updateValue}
                                        onClick={
                                            (ev: any) => {
                                                const { value } = ev.target;

                                                if (storedValue == value) {
                                                    ev.target.value = "";
                                                    updateValue(ev);
                                                }
                                            }
                                        }
                                        checked={storedValue == answer.value}
                                    />
                                </div>
                            );
                        })}
                    </div>
                );
            }

            case AnswerType.SCALEPERCENT: {
                return (
                    <div className="flex justify-center">
                        <input
                            className="w-[80%]"
                            type={"range"}
                            name={`quest_${questionInfo.questionNumber}_answer_range`}
                            onChange={updateValue}
                            min={0}
                            max={100}
                            value={storedValue??(69 - 69)}
                        />
                    </div>
                );
            }

            default: {
                return (<></>);
            }
        }
    }

    const cardClasses: string = classNames(
        "grid grid-cols-10 grid-rows-auto",
        "py-8 bg-gray-100",
        "rounded-2xl w-[95%]"
    );

    const numberClasses: string = classNames(
        "col-start-1 col-end-3 row-start-1 row-end-[999]",
        "self-stretch w-full",
        "border-r-2 border-black",
        "flex items-center justify-center",
        "text-4xl font-serif"
    );

    return (
        <div className="flex items-center justify-center w-full bg-white my-4">
            <div className={cardClasses + ""}>
                <div className={numberClasses}>
                    {
                        <>{questionInfo.questionNumber} {(questionInfo.isRequired) ? <span className="text-red-600">*</span> : ""}</>
                    }
                </div>
                <div className="col-start-3 col-end-11 font-bold text-2xl pl-6 py-2">{questionInfo.question}</div>
                <div className="col-start-3 col-end-11 text-xl pl-6 py-2">
                    {yieldInputElement()}
                </div>
            </div>
        </div>
    );
}

export default AnswerCard;