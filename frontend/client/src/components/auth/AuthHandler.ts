import corsAxios from "../../config/axiosConfig";
import connectionConfig from "../../config/connectionConfig";

class AuthHandler {
    public async isLoggedIn(): Promise<boolean> {
        let resp = await corsAxios.get(connectionConfig.LOGIN_CHECK_ENDPOINT);

        return resp.data;
    }
}

const AUTH: AuthHandler = new AuthHandler();

export default AUTH;