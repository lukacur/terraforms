import { PlusCircleIcon, TrashIcon } from "@heroicons/react/outline";
import React from "react";
import { PossibleAnswerDTO } from "../../util/interfaces/dtos";
import Button from "../universal/form/Button";

type AnswerEditorProps = {
    questionText: string,
    answers?: PossibleAnswerDTO[],
    answersUpdateCallback?: (newArray: PossibleAnswerDTO[]) => void
}

function AnswerEditor({
    questionText,
    answers,
    answersUpdateCallback
}: AnswerEditorProps) {
    const [answersEdit, setAnswersEdit] = React.useState<PossibleAnswerDTO[]>(answers??[]);

    function addBlankAnswer() {
        let tmp: PossibleAnswerDTO = {
            name: "",
            value: `${answersEdit.length}`
        };

        setAnswersEdit([...answersEdit, tmp]);
    }

    function updateAnswer(event: any, index: number): void {
        const { name, value } = event.target

        let answer: PossibleAnswerDTO = {...answersEdit[index], [name]: value};
        let tmp: PossibleAnswerDTO[] = [...answersEdit];
        tmp[index] = answer;

        if (answers && answersUpdateCallback) {
            answers[index] = answer;
            answersUpdateCallback(answers);
        }

        setAnswersEdit([...tmp]);
    }

    function deleteAnswer(index: number) {
        let tmp = [...answersEdit];
        tmp.splice(index, 1);
        setAnswersEdit([...tmp]);

        if (answers && answersUpdateCallback) {
            answersUpdateCallback(tmp);
        }
    }

    return (
        <div>
            <div className="flex flex-col items-center w-full bg-gray-100 pb-8">
                <div className="my-10">
                    <hr className="border-[2px] border-gray-400/25"/>
                    <h1 className="font-bold text-4xl text-center my-5">{questionText}</h1>
                    <hr className="border-[2px] border-gray-400/25"/>
                </div>

                {answersEdit.map((a, i) => (
                    <div className="grid grid-cols-12 mb-2 w-[80%] gap-x-4">
                        <input
                            className="px-2 col-start-1 col-end-6"
                            type={"text"}
                            value={a.name}
                            name={"name"}
                            onChange={(event: any) => updateAnswer(event, i)}
                            placeholder={"Text for answer"}
                        />

                        <input
                            className="px-2 col-start-6 col-end-11"
                            type={"text"}
                            value={a.value}
                            name={"value"}
                            onChange={(event: any) => updateAnswer(event, i)}
                            placeholder={"Value for answer"}
                        />

                        <div className="w-full col-start-11 col-end-13 px-4">
                            <Button
                                text={""}
                                icon={<TrashIcon className="w-6" />}
                                className={"w-full py-2 bg-red-600 hover:bg-red-700 text-white"}
                                styling={{paddedSmall: true, roundedFull: true}}
                                onclick={() => deleteAnswer(i)}
                            />
                        </div>
                    </div>
                ))}
                
                <div className="w-[50%] mt-3">
                    <Button
                        text={"Add new answer"}
                        icon={<PlusCircleIcon className="inline-block w-5" />}
                        className={"bg-green-500 hover:bg-green-300 text-white w-full"}
                        onclick={addBlankAnswer}
                    />
                </div>
            </div>
        </div>
    );
}

export default AnswerEditor;