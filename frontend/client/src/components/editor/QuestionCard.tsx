import React from "react";
import classNames from "classnames";
import { PossibleAnswerDTO, QuestionDTO } from "../../util/interfaces/dtos";
import Button from "../universal/form/Button";
import Toggle from "../universal/form/Toggle";
import ComponentModal from "../universal/modals/ComponentModal";
import AnswerEditor from "./AnswerEditor";
import { AnswerType } from "../../util/constants/Enums";

type QuestionCardProps = {
    classes?: string,
    question?: QuestionDTO,
    questionUpdateCallback?: (newQuestion: QuestionDTO) => void
}

function QuestionCard({
    classes,
    question,
    questionUpdateCallback
}: QuestionCardProps) {
    const [answerModal, setAnswerModal] = React.useState<boolean>(false);
    
    let updateable: any = { question: "", required: false, page: -1, answerType: -1 };

    const cardClasses = classNames(
        "grid grid-cols-4 items-center gap-y-2",
        classes ? classes : "",
        "text-xl"
    );
    

    function updateQuestion(event: any): void {
        const { name, value } = event.target

        updateable = {...updateable, [name]: value};
        if (question && questionUpdateCallback) {
            question.question = updateable.question;
            questionUpdateCallback(question);
        }
    }

    function updateAnswerType(event: any): void {
        const { name, value } = event.target;

        updateable = {...updateable, [name]: parseInt(value)}
        if (question && questionUpdateCallback) {
            question.answerType = updateable.answerType;
            questionUpdateCallback(question);
        }
    }

    function updateRequired(newVal: boolean): void {
        updateable = {...updateable, required: newVal};
        if (question && questionUpdateCallback) {
            question.isRequired = newVal;
            questionUpdateCallback(question);
        }
    }

    function updatePage(event: any) {
        const { name, value } = event.target;

        updateable = {...updateable, [name]: parseInt(value)};
        if (question && questionUpdateCallback) {
            question.questionPage = updateable.page;
            questionUpdateCallback(question);
        }
    }

    function updateAnswers(newAnswers: PossibleAnswerDTO[]) {
        if (question && questionUpdateCallback) {
            question.possibleAnswers = newAnswers;
            questionUpdateCallback(question);
        }
    }

    function isEditable(q: QuestionDTO | undefined) {
        return q?.answerType === AnswerType.SCALE
            || q?.answerType === AnswerType.RADIO
            || q?.answerType === AnswerType.CHECKBOX;
    }

    return (
        <div className={cardClasses + ""}>
            <div className="row-span-1 col-start-1 col-end-5 px-2">
                <input
                    className="w-full px-2 rounded-lg outline-none"
                    name="question"
                    type={"text"}
                    maxLength={128}
                    placeholder="Input question text here"
                    value={question?.question}
                    onChange={updateQuestion}
                />
            </div>

            <div className="row-span-2 col-start-1 col-end-3 lg:col-end-2 pl-2">
                <select className="outline-none w-[80%] lg:w-full" name={"answerType"} value={question?.answerType??-1} onChange={updateAnswerType}>
                    <option value={-1}>-</option>
                    <option value={AnswerType.SHORTTEXT}>Short text</option>
                    <option value={AnswerType.LONGTEXT}>Long text</option>
                    <option value={AnswerType.NUMBER}>Number</option>
                    <option value={AnswerType.SCALE}>Scale</option>
                    <option value={AnswerType.RADIO}>Multichoice - single</option>
                    <option value={AnswerType.CHECKBOX}>Multichoice - many</option>
                    <option value={AnswerType.TRUEFALSE}>True/False</option>
                    <option value={AnswerType.SCALEPERCENT}>Scale percent</option>
                </select>
            </div>

            <div className="relative row-span-2 col-start-3 col-end-5 lg:col-start-2 lg:col-end-3 place-self-center">
                <label className="absolute -top-2 text-xs bg-white rounded-full px-2">Page</label>
                <input className="pl-2 outline-none" type={"number"} name="page" value={question?.questionPage} min={1} max={20} onChange={updatePage} />
            </div>

            <div className="row-span-2 col-start-1 col-end-5 lg:col-start-3 lg:col-end-4">
                <Toggle
                    toggled={question?.isRequired}
                    labelBefore="Required?"
                    colorActive="[#91C4F2]"
                    styling={{small:false, medium: true, large: false}}
                    onChange={(from, to) => updateRequired(to)}
                />
            </div>

            <div className="row-span-2 col-start-1 col-end-5 lg:col-start-4 lg:col-end-5 pr-2">
                <Button
                    text={`Edit answers (${question?.possibleAnswers.length??0})`}
                    onclick={() => setAnswerModal(true)}
                    enabled={isEditable(question)}
                    styling={{paddedSmall: true, roundedFull: true}}
                    className={
                        classNames(
                            "text-black bg-[#91C4F2] hover:bg-questionEditAnswersHovered w-full",
                            {
                                "invisible": !isEditable(question)
                            }
                        )
                    }
                />
            </div>
            
            {(isEditable(question) && question && question.possibleAnswers.length > 0) ?
                (<div className="row-span-3 col-start-1 col-end-5 px-8 py-4 border-2 border-solid border-black/25 relative">
                    <span className="text-black text-xs bg-gray-300 absolute -top-2.5">Set answers</span>
                    {question?.possibleAnswers.map((pa, i) => {
                        return (
                            <>
                            <div className="md:flex flex-row justify-between items-center px-4 hidden">
                                <p className={classNames(
                                    "basis-10/12 border-t-[1px] border-t-black border-r-[1px] border-r-black",
                                    "px-2 py-1 overflow-scroll overflow-y-hidden scroll-hidden",
                                    {
                                        "border-b-[1px] border-b-black": i === question?.possibleAnswers.length - 1
                                    }
                                )}>{pa.name}</p>
                                <p className={classNames(
                                    "basis-2/12 text-center w-full border-t-[1px] border-t-black",
                                    "py-1 overflow-scroll overflow-y-hidden scroll-hidden",
                                    {
                                        "border-b-[1px] border-b-black": i === question?.possibleAnswers.length - 1
                                    }
                                )}>{pa.value}</p>
                            </div>

                            <div className="flex flex-col md:hidden text-lg">
                                <div>{pa.name}:</div>
                                <div className="self-center">{pa.value}</div>
                                {(i !== question?.possibleAnswers.length - 1) ? <hr className="border-gray-400/75 my-2" /> : ""}
                            </div>
                            </>
                        );
                    })}
                </div>)
                : ""}

            {(answerModal) ?
                <ComponentModal
                    modalContent={<AnswerEditor answers={question?.possibleAnswers} questionText={question?.question??""} answersUpdateCallback={updateAnswers} />}
                    modalCloseFunction={
                        () => {
                            if (question && questionUpdateCallback) {
                                question.possibleAnswers = question.possibleAnswers.filter(pa => pa.name !== "");
                                questionUpdateCallback(question);
                            }
                            setAnswerModal(false);
                        }
                    }
                />
                : ""
            }
        </div>
    );
}

export default QuestionCard;