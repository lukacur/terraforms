import classNames from "classnames";
import "./LoadingAnimation.css";

type LoadingCircleProps =  {
    className?: string,
    strokeColor?: string
}

function LoadingCircle({ className, strokeColor = "stroke-slate-500/50" }: LoadingCircleProps) {
    return (
        <div className={className}>
            <svg className={classNames("loader-container w-[75%]", strokeColor)}>
                <circle className="loading-circle" cx="50%" cy="50%" r="10%" fill="none" strokeWidth="7" />
            </svg>
        </div>
    );
}

export default LoadingCircle;