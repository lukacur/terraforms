import classNames from "classnames";

type InfoCardProps = {
    className: string,
    text: string
}

function InfoCard({
    className,
    text
}: InfoCardProps) {
    return (
        <div className={classNames(className, "info-card flex items-center justify-center mb-4 mr-2")}>
            {text}
        </div>
    );
}

export default InfoCard;