import classNames from 'classnames';
import React from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import DATA_SERVER from '../../../util/data/DataFetch';
import { UserInfoDTO } from '../../../util/interfaces/dtos';
import AUTH from '../../auth/AuthHandler';
import SideMenu from '../dropdown/SideMenu';
import UserCard from './UserCard';

type HeaderProps = {
    size?: string,
    bgColor?: string,
    textColor?: string,
    children?: unknown
}

function Header({
    size = 'lg',
    bgColor = 'emerald-400',
    textColor = 'white'
}: HeaderProps) {
    const location = useLocation();
    const navigate = useNavigate();

    const [loggedIn, setLoggedIn] = React.useState<boolean>(false);
    const [rendered, setRendered] = React.useState<boolean>(false);
    const [isSmall, setIsSmall] = React.useState<boolean>(false);
    const [redirectAll, setRedirectAll] = React.useState<boolean>(false);

    const [userInfo, setUserInfo] = React.useState<UserInfoDTO>();

    const classes = classNames(
        `bg-${bgColor} text-${textColor} font-bold`,
        "px-4 h-16",
        "flex flex-nowrap flex-row items-center justify-between",
        "text-lg sm:text-xl",
        {
            "text-xs": size === 'sm',
            "text-xl": size === 'lg'
        }
    );

    const authDivClasses = classNames(
        "h-full even:rounded-r-2xl odd:rounded-l-2xl hover:bg-emerald-500 bg-emerald-400",
        "basis-3/6 text-center cursor-pointer",
        "flex flex-row flex-shrink-0 flex-grow-0 items-center justify-center"
    );

    const logoutContainerClasses = classNames(
        "flex flex-row items-center justify-center basis-2/6 mx-2"
    );

    const linksClasses = classNames(
        "h-4/5 first:border-r-2 first:border-r first:border-r-emerald-500 first:pr-2 last:pl-2",
        "hover:bg-emerald-500 first:pl-4 last:pr-4 text-2xl first:rounded-l-xl last:rounded-r-xl",
        "basis-1/2 text-center py-2"
    );

    React.useEffect(() => {
        if (redirectAll && location.pathname !== "/setupUsername") {
            navigate("/setupUsername");
        }

        let shouldRender: boolean = !location.pathname.endsWith("/register") &&
                                    !location.pathname.endsWith("/login") &&
                                    !location.pathname.endsWith("/logout") &&
                                    !location.pathname.startsWith("/setupUsername");
        setRendered(shouldRender);
        setIsSmall(matchMedia("(min-width: 640px)").matches);

        if (shouldRender) {
            AUTH.isLoggedIn()
            .then(loginStatus => {
                if (loginStatus) {
                    setLoggedIn(loginStatus);

                    DATA_SERVER.fetchUserInfo()
                    .then(userData => {
                        setUserInfo(userData);
                    });
                }
            });
        } else if (location.pathname.endsWith("/setupUsername")) {
            document.body.hidden = true;
            AUTH.isLoggedIn()
            .then(loginStatus => {
                if (loginStatus) {
                    DATA_SERVER.fetchUserInfo()
                    .then(userData => {
                        if (userData?.username) {
                            setRedirectAll(false);
                            navigate("/");
                        } else {
                            setRedirectAll(true);
                        }
                    });
                } else {
                    navigate("/");
                }
                document.body.hidden = false;
            });
        } else if (location.pathname.endsWith("/login") || location.pathname.endsWith("/register")) {
            AUTH.isLoggedIn()
            .then(loginStatus => {
                if (loginStatus) {
                    navigate("/");
                }
            });
        }

        let listener = () => {
            setIsSmall(matchMedia("(min-width: 640px)").matches);
        };

        window.addEventListener("resize", listener);

        return () => {
            window.addEventListener("resize", listener);
        };
    }, [location]);

    return (
        <>
            {(rendered) ? (
                <nav className={classes + ""}>
                    <div className="text-sm md:text-xl basis-3/6">
                        <Link to={"/"}>TerraForms - form building tool</Link>
                    </div>
                    {(isSmall || !loggedIn) ? (
                        <>
                        {(loggedIn) ? (
                            <div className={logoutContainerClasses}>
                                <Link to={"/dashboard"} className={linksClasses}>Dashboard</Link>
                                <Link to={"/editor"} className={linksClasses}>Editor</Link>
                            </div>
                            ):""}

                        {(loggedIn) ? (
                                <div className="flex flex-row justify-end flex-shrink-0 flex-grow-0 items-center basis-1/6 h-4/5">
                                    <UserCard
                                        userInfo={userInfo}
                                    />
                                </div>
                            ) : (
                                <div className="basis-3/6 sm:basis-2/6 flex flex-row items-center justify-end h-4/5 text-sm xs:text-lg md:text-xl">
                                    <Link to={"/login"} className={authDivClasses}><div>Log in</div></Link>
                                    <Link to={"/register"} className={authDivClasses}>Register</Link>
                                </div>
                            )
                        }
                        </>
                    ) : (
                        <SideMenu
                            flexElementContent={
                                [
                                    <div className="w-4/5 text-xl py-2 text-center">{userInfo?.username}</div>,
                                    <hr className="border-gray-400 w-4/5 mt-2 mb-4" />,
                                    <Link to={"/dashboard"} className="w-4/5 text-2xl py-2">Dashboard</Link>,
                                    <Link to={"/editor"} className="w-4/5 text-2xl py-2">Editor</Link>,
                                    <hr className="border-gray-400 w-4/5 my-4" />,
                                    <Link to={"/profile"} className="w-4/5 text-2xl py-2">My Profile</Link>,
                                    <Link to={"/logout"} className="w-4/5 text-2xl text-red-500 py-2">Logout</Link>
                                ]
                            }
                        />
                    )}
                </nav>
            ):""}
        </>
    );
}

export default Header;