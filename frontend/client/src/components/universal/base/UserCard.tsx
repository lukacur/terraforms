import { ChevronDownIcon, ChevronUpIcon, UserIcon } from "@heroicons/react/outline";
import classNames from "classnames";
import React, { MouseEvent } from "react";
import { useNavigate } from "react-router-dom";
import { UserInfoDTO } from "../../../util/interfaces/dtos";
import Menu from "../dropdown/Menu";

type UserCardProps = {
    userInfo?: UserInfoDTO
}

function UserCard({
    userInfo
}: UserCardProps) {
    const navigate = useNavigate();
    const [expanded, setExpanded] = React.useState<boolean>(false);
    const cardRef = React.createRef<HTMLDivElement>();

    function toggleMenu(ev: MouseEvent) {
        ev.stopPropagation();

        if (userInfo) {
            setExpanded(!expanded);
        }
    }

    const cardClasses: string = classNames(
        "flex flex-row items-center text-white",
        {
            "cursor-pointer": userInfo,
            "cursor-not-allowed": !userInfo
        }
    );

    React.useEffect(() => {
        let listener = (ev: any) => {
            if (cardRef.current && !cardRef.current.contains(ev.target)) {
                setExpanded(false);
            }
        };

        document.addEventListener("mousedown", listener);
        document.addEventListener("touchstart", listener);

        return () => {
            document.removeEventListener("mousedown", listener);
            document.removeEventListener("touchstart", listener);
        }
    }, [cardRef]);

    return (
        <div className={classNames("text-black relative", {"animate-pulse": !userInfo})} ref={cardRef}>
            <div className={cardClasses} onClick={toggleMenu}>
                <UserIcon className="w-6 h-6"/>
                <div className="flex items-center justify-center ml-1 mr-3"><div>{userInfo?.username || "..."}</div></div>
                {(expanded) ? <ChevronUpIcon className="w-6 h-6" /> : <ChevronDownIcon className="w-6 h-6"/>}
            </div>
            <Menu
                hidden={!expanded}
                horizontalAlignment={{ custom: "right-0" }}
                verticalAlignment={{ custom: "top-2"}}
                menuCloseCallback={() => setExpanded(false)}
                renderButton={false}
                isAnimated={true}
                menuOptions={
                    [
                        ["My profile", () => navigate("/profile"), undefined, true],
                        ["Logout", () => navigate("/logout"), "hover:bg-red-500 hover:text-white"]
                    ]
                }
            />
        </div>
    );
}

export default UserCard;