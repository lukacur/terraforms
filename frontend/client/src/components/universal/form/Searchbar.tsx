import React, { ChangeEvent } from 'react';
import classNames from 'classnames';
import Button from './Button';
import { SearchIcon } from '@heroicons/react/solid';
import { useNavigate } from 'react-router-dom';
import DATA_SERVER from '../../../util/data/DataFetch';
import STORE from '../../../util/store/Store';
import { FormFullLocal } from '../../../util/interfaces/StoreData';

type SearchBarProps = {
    placeholder?: string,
    maxLength?: number,
    isStatisticsFetch?: boolean
}

function Searchbar({
    placeholder,
    maxLength = 32,
    isStatisticsFetch
}: SearchBarProps) {
    const navigate = useNavigate();
    const [formLink, setFormLink] = React.useState<string>("");

    const [linkInvalid, setLinkInvalid] = React.useState<boolean>(false);

    const searchBoxClasses = classNames(
        "text-3xl w-full h-auto text-center",
        "outline-none",
        "border-2 focus:border-blue-400 hover:border-blue-400 border-solid border-blue-300 rounded-xl",
        "px-4"
    );

    function updateLink(event: ChangeEvent) {
        const { value } = event.target as HTMLInputElement;

        setFormLink(value);
    }

    function toAnsweringPage() {
        if (!isStatisticsFetch) {
            DATA_SERVER.fetchForAnswering(formLink)
            .then(form => {
                let localInfo: FormFullLocal = {
                    localFormId: form.formData.formId?.toString() || (() => { throw new Error("This should never happen") })(),
                    form
                }

                STORE.setCurrAnswerForm(localInfo)
                .then(() => {
                    navigate("/participate");
                });
            })
            .catch(() => {
                setLinkInvalid(true);
                setTimeout(() => setLinkInvalid(false), 2000);
            });
        } else {
            DATA_SERVER.fetchStatisticsByLink(formLink)
            .then(answers => {
                STORE.setCurrStatistics(answers)
                .then(() => navigate("/statistics"));
            })
            .catch(error => {
                setLinkInvalid(true);
                setTimeout(() => setLinkInvalid(false), 2000);
            });
        }
    }

    return (
        <div className="flex flex-col items-center justify-center w-full">
            <div className="w-full">
                <input
                    className={searchBoxClasses}
                    type="text"
                    maxLength={maxLength}
                    placeholder={placeholder}
                    onChange={updateLink}
                />
                {(linkInvalid) ? <span className="text-red-600 text-sm">Check the form link and try again</span> :""}
            </div>
            <div className="mt-4 w-4/5 md:w-3/5">
                <Button
                    styling={{ paddedMedium: true, roundedPartial: true }}
                    className={"bg-blue-500 hover:bg-blue-400 text-white w-full"}
                    icon={<SearchIcon className="inline-block w-6" />}
                    text="Search"
                    onclick={toAnsweringPage}
                />
            </div>
        </div>
    );
}

export default Searchbar;