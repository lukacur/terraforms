import React, { ReactElement } from 'react';
import classNames from 'classnames';

type StylingProps = {
    roundedFull?: boolean,
    roundedPartial?: boolean,
    paddedSmall?: boolean,
    paddedMedium?: boolean,
    paddedLarge?: boolean
}

type ButtonProps = {
    text?: string,
    icon?: ReactElement,
    enabled?: boolean,
    toolTip?: string,
    width?: string,
    className?: string,
    styling?: StylingProps,
    onclick?: (clickEvent: React.MouseEvent<HTMLButtonElement> | undefined) => void
}

function Button({
    text = "Submit",
    width = "full",
    icon,
    enabled = true,
    toolTip,
    className,
    styling = {
        roundedFull: false,
        roundedPartial: true,
        paddedSmall: false,
        paddedMedium: true,
        paddedLarge: false
    },
    onclick
}: ButtonProps) {
    const btnClasses = classNames(
        (className !== undefined) ? className :
        ["hover:bg-blue-500 bg-blue-700 text-white",
        `w-${width} text-xl`,
        "outline-none"],
        {
            "rounded-full": styling.roundedFull,
            "rounded-2xl": styling.roundedPartial,
            "px-4 py-1": styling.paddedSmall,
            "px-8 py-1": styling.paddedMedium,
            "px-12 py-1": styling.paddedLarge,
            "disabled:hover:bg-gray-400 disabled:bg-gray-400": (text && icon) || text,
            "disabled:hover:text-gray-400 disabled:text-gray-400": icon && !text
        },
    );

    const textClasses = classNames(
        {
            "ml-2": icon,
            "hidden": !text
        }
    );
    
    return (
        <button className={btnClasses} onClick={onclick} disabled={!enabled} title={toolTip??""}>
            <div className="flex flex-row justify-center items-center w-full">
                {icon??""} <div className={textClasses}>{text}</div>
            </div>
        </button>
    );
}

export default Button;