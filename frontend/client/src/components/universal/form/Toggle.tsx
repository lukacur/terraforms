import classNames from "classnames";
import React from "react";

type StylingProps = {
    small?: boolean,
    medium?: boolean,
    large?: boolean
}

type ToggleProps = {
    labelBefore?: string,
    labelAfter?: string,
    colorActive?: string,
    colorInactive?: string,
    styling?: StylingProps,
    onChange?: (from: boolean, to: boolean) => void,
    toggled?: boolean
}

var i: number = 0;

function Toggle({
    labelBefore,
    labelAfter,
    colorActive = "blue-400",
    colorInactive = "gray-400",
    styling = {
        small: true,
        medium: false,
        large: false
    },
    onChange,
    toggled
}: ToggleProps) {
    let myIdx: number = i++;
    let off: boolean = !toggled;

    let backgroundActive: string = `bg-${colorActive}`;
    let backgroundInactive: string = `bg-${colorInactive}`;

    function changeState() {
        let parent: HTMLElement | null = document.getElementById(`toggle_parent_comp_${myIdx}`);

        if (parent) {
            let peg: HTMLElement | null = parent.querySelector("div");

            if (off) {
                parent.classList.remove(backgroundActive);
                parent.classList.add(backgroundInactive);

                if (peg) {
                    peg.classList.add("float-right");
                }
            } else if (!off) {
                parent.classList.add(backgroundInactive);
                parent.classList.remove(backgroundActive);

                if (peg)
                    peg.classList.remove("float-right");
            }

            off = !off;
            if (onChange) {
                onChange(off, !off);
            }
        }
    }

    const surrounderClasses: string  = classNames(
        "border-solid border-2 rounded-full",
        "cursor-pointer",
        ((off) ? backgroundInactive : backgroundActive),
        {
            "w-9 h-6": styling.small,
            "w-12 h-8": styling.medium,
            "w-15 h-10": styling.large
        }
    );

    const pegClasses: string = classNames(
        "border-solid border-2 border-white bg-white rounded-full",
        {
            "float-right": !off,
            "w-5 h-5": styling.small,
            "w-7 h-7": styling.medium,
            "w-9 h-9": styling.large
        }
    );

    return (
        <div className="flex flex-row justify-around items-center">
            {labelAfter ? "" : labelBefore??""} <div id={`toggle_parent_comp_${myIdx}`} onClick={changeState} className={surrounderClasses}>
                <div className={pegClasses}></div>
            </div> {labelBefore ? "" : labelAfter??""}
        </div>
    );
}

export default Toggle;