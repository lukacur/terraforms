import { XIcon } from "@heroicons/react/outline";
import classNames from "classnames";
import React, { ReactElement } from "react";
import Button from "../form/Button";
import "./Modals.css";

/**
 * For @var modalOptions insert elements left to right
 */
type ComponentModalProps = {
    style?: string,
    modalCloseFunction: () => void,
    modalContent: ReactElement
}

function ComponentModal({
    style,
    modalCloseFunction,
    modalContent
}: ComponentModalProps) {
    const modalBackgroundClasses: string = classNames(
        "fixed top-0 left-0 w-screen h-screen z-10 py-6",
        "bg-gray-700 bg-opacity-75"
    );

    const modalContentClasses: string = classNames(
        "z-20",
        "bg-white text-black",
        "col-start-2 col-end-12",
        "scrollable overflow-scroll overlay-scrollbar overflow-x-hidden w-full h-full",
        "rounded-l-2xl rounded-r-md",
        "flex flex-col items-center"
    );

    function modalForegroundClick(event: any): void {
        event.stopPropagation();
        event.preventDefault();
    }

    function modalScroll(event: any): void {
        event.stopPropagation();
        event.preventDefault();
    }

    return (
        <div className={modalBackgroundClasses} onClick={modalCloseFunction} onScroll={modalScroll}>
            <div className="grid grid-cols-12 w-full h-full">
                <div className={modalContentClasses} onClick={modalForegroundClick}>
                    <div className="sticky top-0 w-full bg-white self-end">
                        <Button
                            className={"w-8 h-8 float-right mr-2"}
                            text=""
                            styling={{}}
                            onclick={modalCloseFunction}
                            icon={<XIcon className="w-full h-full" />}
                        />
                    </div>

                    <div className="scrollable w-full h-full">{modalContent}</div>
                </div>
            </div>
        </div>
    );
}

export default ComponentModal;