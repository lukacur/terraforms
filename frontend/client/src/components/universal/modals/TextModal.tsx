import { InformationCircleIcon, XIcon } from "@heroicons/react/outline";
import classNames from "classnames";
import { ReactElement } from "react";
import Button from "../form/Button";
import "./Modals.css";

/**
 * For @var modalOptions insert elements left to right
 */
type TextModalProps = {
    modalTitle: string,
    modalText: string,
    modalOptions: ReactElement<typeof Button>[],
    modalCloseFunction: () => void,
    modalIcon?: ReactElement
}

function TextModal({
    modalTitle,
    modalText,
    modalOptions,
    modalCloseFunction,
    modalIcon = <InformationCircleIcon className="text-blue-300" />
}: TextModalProps) {
    const modalBackgroundClasses: string = classNames(
        "fixed top-0 left-0 w-screen h-screen z-30",
        "bg-gray-700 bg-opacity-75"
    );

    const modalContentClasses: string = classNames(
        "z-40",
        "bg-white text-black",
        "rounded-2xl",
        "fixed top-[50vh] left-[50vw] -translate-x-1/2 -translate-y-1/2 w-[90vw] sm:w-[80vw] max-w-[95vw] max-h-[90vh]",
        "flex flex-col lg:flex-row items-center",
        "px-4 py-8"
    );

    const iconClasses: string = classNames(
        "basis-5/6 lg:basis-1/12 z-50",
        "flex items-center justify-center",
        "row-start-3 row-end-5 w-1/4 md:w-1/6"
    );

    function modalForegroundClick(event: any): void {
        event.stopPropagation();
        event.preventDefault();
    }

    return (
        <div className={modalBackgroundClasses} onClick={modalCloseFunction}>
            <div className={modalContentClasses} onClick={modalForegroundClick}>
                {modalIcon ?
                (<div className={iconClasses} onClick={modalForegroundClick}>
                    {modalIcon}
                </div>):""}

                <div className="flex flex-col items-center justify-center basis-1/6 lg:basis-11/12 max-h-[70vh] w-[90vw] sm:w-[80vw] max-w-[90vw]">
                    <div className="w-full flex flex-row mb-4 items-center justify-center">
                        <h1 className="text-center font-bold text-3xl basis-full">{modalTitle}</h1>
                        <Button
                            className={"basis-10 relative bottom-20 lg:bottom-5 right-5 z-50"}
                            text=""
                            styling={{}}
                            onclick={modalCloseFunction}
                            icon={<XIcon className="w-full" />}
                        />
                    </div>
                    
                    <hr className="border-2 rounded-full w-[85%] mb-4"/>
                    
                    <div className="scrollable h-[85%] w-[85%] text-justify text-xl overflow-scroll overlay-scrollbar overflow-x-hidden">{modalText}</div>

                    <hr className="border-2 rounded-full w-[85%] my-4"/>
                    
                    <div className="flex flex-col md:flex-row w-[85%] justify-around">
                        {modalOptions.map((touple, index) => <div key={index} className="w-full flex flex-row items-center justify-center mb-2 lg:mb-0">{touple}</div>)}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default TextModal;