import { ChevronDownIcon, ChevronUpIcon } from "@heroicons/react/outline";
import classNames from "classnames";
import React, { ReactElement } from "react";
import Button from "../form/Button";

type ElementDropdownProps = {
    dropdownText: string,
    elementToShow: ReactElement,
    initiallyExpanded?: boolean
}

function ElementDropdown({
    dropdownText,
    elementToShow,
    initiallyExpanded = false
}: ElementDropdownProps) {
    const [expanded, setExpanded] = React.useState<boolean>(initiallyExpanded);

    const containerClasses: string = classNames(
        "flex flex-col items-center justify-center w-[95%]",
        "border-gray-400 border-2 border-solid rounded-xl",
        "px-4 my-5"
    );

    return (
        <div className={containerClasses}>
            <div className="flex flex-row items-center justify-between w-full cursor-pointer" onClick={() => setExpanded(!expanded)}>
                <div className="basis-11/12 font-bold py-2">{dropdownText}</div>
                {(expanded) ? (<Button
                    text=""
                    className={"outline-none basis-1/12"}
                    styling={{}}
                    icon={<ChevronUpIcon className="w-6" />}
                />) : 
                (<Button
                    text=""
                    className={"outline-none basis-1/12"}
                    styling={{}}
                    icon={<ChevronDownIcon className="w-6" />}
                />)}
            </div>

            {(expanded) ? elementToShow : ""}
        </div>
    );
}

export default ElementDropdown;