import { DotsVerticalIcon } from "@heroicons/react/solid";
import classNames from "classnames";
import React from "react";
import Button from "../form/Button";

type MenuAlignment = {
    alignStart?: boolean,
    alignCenter?: boolean,
    alignEnd?: boolean,
    custom?: string
}

type MenuProps = {
    menuOptions: [optionText: string, optionClickCallback: () => void, optionStyle?: string, dividerAfter?: boolean][],
    horizontalAlignment?: MenuAlignment,
    verticalAlignment?: MenuAlignment,
    hidden: boolean,
    renderButton?: boolean,
    isAnimated?: boolean,
    menuOpenCallback?: () => any,
    menuCloseCallback?: () => any
}

function Menu({
    menuOptions,
    horizontalAlignment = {
        alignStart: false,
        alignCenter: false,
        alignEnd: true
    },
    verticalAlignment = {
        alignStart: true,
        alignCenter: false,
        alignEnd: false
    },
    hidden,
    renderButton = true,
    isAnimated = false,
    menuOpenCallback,
    menuCloseCallback
}: MenuProps) {
    const menuClasses: string = classNames(
        "w-64 bg-white z-50",
        "flex flex-col items-start justify-start rounded-xl shadow-xl shadow-gray-600 overflow-hidden",
        (isAnimated) ? "animate-dropdown-extend" : "",
        "absolute",
        (hidden) ? "hidden" : "block",
        (horizontalAlignment.custom) ? horizontalAlignment.custom :
        {
            "right-2": horizontalAlignment.alignStart,
            "-right-44": horizontalAlignment.alignCenter,
            "left-2": horizontalAlignment.alignEnd
        },
        (verticalAlignment.custom) ? verticalAlignment.custom :
        {
            "top-8": verticalAlignment.alignStart,
            "bottom-0": verticalAlignment.alignCenter,
            "-bottom-0": verticalAlignment.alignEnd
        }
    );

    React.useEffect(() => {}, [hidden]);

    return (
        <div className="relative flex flex-col">
            {(renderButton) ? (
                <Button
                    text=""
                    className={"outline-none"}
                    styling={{}}
                    onclick={
                        (ev: any) => {
                            ev.stopPropagation();

                            if (hidden && menuOpenCallback) {
                                menuOpenCallback();
                            } else if (menuCloseCallback) {
                                menuCloseCallback();
                            }
                        }
                    }
                    icon={<DotsVerticalIcon className="w-6" />}
                />
            ):""}

            <div className={menuClasses}>
                {menuOptions.map((option, index) => {
                    return (
                        <div className="w-full" key={`${option[0]}__${index}`}>
                            <Button
                                text={`${option[0]}`}
                                className={
                                    classNames(
                                        "w-full py-2 text-xl",
                                        option[2] || "hover:bg-gray-300"
                                    )
                                }
                                styling={{}}
                                onclick={
                                    () => {
                                        if (menuCloseCallback) {
                                            menuCloseCallback();
                                        }
                                        
                                        option[1]();
                                    }
                                }
                                key={option[0]}
                            />

                            {(index !== menuOptions.length - 1 && option[3]) ? (<hr className="w-full" />) : ""}
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default Menu;