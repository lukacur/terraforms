import { MenuIcon, XIcon } from "@heroicons/react/outline";
import classNames from "classnames";
import React, { ReactElement } from "react";
import Button from "../form/Button";

type SideMenuProps = {
    flexElementContent?: ReactElement[];
}

function SideMenu({ flexElementContent }: SideMenuProps) {
    const menuContentRef = React.useRef<HTMLDivElement>(null);
    const [menuActive, setMenuActive] = React.useState<boolean>(false);

    const backgroundClasses: string = classNames(
        "fixed top-0 left-0 w-screen h-screen z-40 py-6",
        "bg-gray-700 bg-opacity-75",
        "flex items-center justify-end"
    );

    function hideMenu() {
        if (menuContentRef.current) {
            menuContentRef.current.classList.remove("sidemenu-content");
            menuContentRef.current.classList.add("sidemenu-content-hide");
        }

        setTimeout(() => {
            setMenuActive(false);
        }, 500);
    }

    React.useEffect(() => {
        let listener = (event: MouseEvent) => {
            if (menuContentRef.current) {
                if (menuActive && !menuContentRef.current.contains(event.target as Element)) {
                    hideMenu();
                }
            }
        };

        document.addEventListener("click", listener);

        return () => {
            document.removeEventListener("click", listener);
        };
    }, [menuActive])

    return (
        <div className="relative text-black">
            <Button
                styling={{}}
                className="active:bg-emerald-500 rounded-xl p-2"
                text=""
                icon={<MenuIcon className="w-8 text-white" />}
                onclick={
                    (event: React.MouseEvent<HTMLButtonElement, MouseEvent> | undefined) => {
                        setMenuActive(true);
                        event?.preventDefault();
                        event?.stopPropagation();
                    }
                }
            />

            {menuActive ? (
                <div className={backgroundClasses}>
                    <div className="relative basis-2/3 md:basis-1/2 h-screen bg-white sidemenu-content rounded-l-3xl" hidden={!menuActive} ref={menuContentRef}>
                        <div className="w-full flex flex-col items-center justify-end">
                            <Button
                                styling={{}}
                                className="bg-none self-end mt-2 mr-2"
                                text=""
                                icon={<XIcon className="w-8" />}
                                onclick={() => hideMenu()}
                            />

                            {flexElementContent}
                        </div>
                    </div>
                </div>
            ):""}
        </div>
    );
}

export default SideMenu;