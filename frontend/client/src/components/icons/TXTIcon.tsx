import { SVGProps } from "react"

function TXTIcon(props: SVGProps<SVGSVGElement>) {
    return (
        <svg
            width={32}
            height={32}
            viewBox="0 0 8.467 8.467"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <defs>
            <linearGradient id="txti-linearg-b">
                <stop
                style={{
                    stopColor: "#0004f0",
                    stopOpacity: 1,
                }}
                offset={0}
                />
                <stop
                style={{
                    stopColor: "#cbccff",
                    stopOpacity: 0,
                }}
                offset={1}
                />
            </linearGradient>
            <linearGradient id="txti-linearg-a">
                <stop
                style={{
                    stopColor: "#0003f0",
                    stopOpacity: 1,
                }}
                offset={0}
                />
                <stop
                style={{
                    stopColor: "#7577ff",
                    stopOpacity: 0.56766915,
                }}
                offset={1}
                />
            </linearGradient>
            <linearGradient
                xlinkHref="#txti-linearg-a"
                id="txti-linearg-c"
                x1={1.161}
                y1={4.247}
                x2={7.697}
                y2={4.247}
                gradientUnits="userSpaceOnUse"
                spreadMethod="pad"
            />
            </defs>
            <path
            style={{
                fill: "url(#txti-linearg-c)",
                fillOpacity: 1,
                stroke: "url(#txti-linearg-a)",
                strokeWidth: 0.13229167,
                strokeLinecap: "round",
                strokeLinejoin: "round",
                strokeDasharray: "none",
                strokeOpacity: 1,
                opacity: 1,
            }}
            d="M1.274.129h5.611l.699.914v7.323H1.282Z"
            />
            <text
            xmlSpace="preserve"
            style={{
                fontSize: "3.175px",
                fill: "#ff7575",
                fillOpacity: 1,
                stroke: "#fff",
                strokeWidth: 0.264999,
                strokeLinecap: "square",
                strokeLinejoin: "miter",
                strokeDasharray: "none",
                strokeOpacity: 1,
            }}
            x={1.881}
            y={4.139}
            />
            <text
            xmlSpace="preserve"
            style={{
                fontStyle: "normal",
                fontVariant: "normal",
                fontWeight: 400,
                fontStretch: "normal",
                fontSize: "3.175px",
                fontFamily: "Arial",
                fontVariantLigatures: "normal",
                fontVariantCaps: "normal",
                fontVariantNumeric: "normal",
                fontVariantEastAsian: "normal",
                fill: "#fff",
                fillOpacity: 1,
                stroke: "#fff",
                strokeWidth: 0.264999,
                strokeLinecap: "square",
                strokeLinejoin: "miter",
                strokeDasharray: "none",
                strokeOpacity: 1,
            }}
            x={2.085}
            y={5.496}
            >
            <tspan
                style={{
                fontStyle: "normal",
                fontVariant: "normal",
                fontWeight: 400,
                fontStretch: "normal",
                fontSize: "3.175px",
                fontFamily: "Arial",
                fontVariantLigatures: "normal",
                fontVariantCaps: "normal",
                fontVariantNumeric: "normal",
                fontVariantEastAsian: "normal",
                fill: "#fff",
                fillOpacity: 1,
                strokeWidth: 0.265,
                }}
                x={2.085}
                y={5.496}
            >
                {".txt"}
            </tspan>
            </text>
        </svg>
    );
}

export default TXTIcon;