import corsAxios from "../../config/axiosConfig";
import AUTH from "../../components/auth/AuthHandler";
import remote from "../../config/storeConfig";
import { FormDTO, FormFullDTO, FormStatisticsDTO, QuestionAnswerDTO } from "../interfaces/dtos";
import { FormFullLocal } from "../interfaces/StoreData";

const storeCommonKeys = {
    TEMP_ANSWERS: "ANSW_TEMP",
    TEMP_FORMS: "FRMS_TEMP",
    CURR_FORM_E: "CURR_FRM_EDIT",
    CURR_FORM_A: "CURR_FRM_ANSW",
    CURR_FORM_S: "CURR_FRM_STAT"
};

class Store {
    private remoteStore: RemoteStore;
    private newFormsIdx: number = 0;

    private tempAnswersMap: Map<number, QuestionAnswerDTO[]> = new Map();
    private localFormsMap: Map<string, FormFullDTO> = new Map();
    private currEditingForm: FormFullLocal | null = null;
    private currAnsweringForm: FormFullLocal | null = null;
    private currStats: FormStatisticsDTO | null = null;

    constructor() {
        this.remoteStore = new RemoteStore();

        this.loadRemote();

        setTimeout(() => this.commit(), 1000 * 60 * 5);
    }

    public async loadRemote() {
        if (!(await AUTH.isLoggedIn())) {
            return;
        }

        this.forceRemoteGet<[number, QuestionAnswerDTO[]][]>(storeCommonKeys.TEMP_ANSWERS)
        .then(res => {
            if (res) {
                this.tempAnswersMap = new Map(res);
            } else {
                this.setItem<[number, QuestionAnswerDTO[]][]>(storeCommonKeys.TEMP_ANSWERS, Array.from(this.tempAnswersMap));
            }
        });

        this.forceRemoteGet<[string, FormFullDTO][]>(storeCommonKeys.TEMP_FORMS)
        .then(res => {
            if (res) {
                this.localFormsMap = new Map(res);
            } else {
                this.setItem<[string, FormFullDTO][]>(storeCommonKeys.TEMP_FORMS, Array.from(this.localFormsMap));
            }
        })
        .then(() => {
            while (this.localFormsMap.has(`lo_${this.newFormsIdx}`)) {
                this.newFormsIdx++;
            }
        });

        this.forceRemoteGet<FormFullLocal>(storeCommonKeys.CURR_FORM_E)
        .then(res => {
            if (res) {
                this.currEditingForm = res;
            } else {
                this.setItem<FormFullLocal | null>(storeCommonKeys.CURR_FORM_E, this.currEditingForm);
            }
        });

        this.forceRemoteGet<FormFullLocal>(storeCommonKeys.CURR_FORM_A)
        .then(res => {
            if (res) {
                this.currAnsweringForm = res;
            } else {
                this.setItem<FormFullLocal | null>(storeCommonKeys.CURR_FORM_E, this.currAnsweringForm);
            }
        });
    }

    public async cloneForm(form: FormFullDTO, isNew: boolean): Promise<FormFullLocal> {
        let newForm: FormFullDTO = {
            bodyData: form.bodyData,
            formData: form.formData
        };

        let formWrapper: FormFullLocal = {
            form,
            localFormId: (isNew) ? `lo_${this.newFormsIdx++}` : newForm.formData.formId?.toString() || (() => { throw new Error("This shouldn't happen"); })()
        };

        delete formWrapper.form.formData.formId;
        delete formWrapper.form.bodyData.parentFormId;

        for (let qst of formWrapper.form.bodyData.questions) {
            delete qst.questionId;
        }
        
        this.localFormsMap.set(formWrapper.localFormId, formWrapper.form);
        await this.saveLocalFormsMap();
        await this.setCurrEditForm(formWrapper);
        return formWrapper;
    }

    public async createNewForm(formData: FormDTO) {
        let newForm: FormFullDTO = {
            bodyData: {
                parentFormId: -1,
                questions: []
            },
            formData
        };

        return this.cloneForm(newForm, true);
    }

    public async saveLocalFormsMap(): Promise<void> {
        await this.setItem(storeCommonKeys.TEMP_FORMS, Array.from(this.localFormsMap));
    }

    public async getCurrentEditForm(): Promise<FormFullLocal | null> {
        return this.currEditingForm;
    }
    
    public async setCurrEditForm(form: FormFullLocal | null): Promise<void> {
        this.currEditingForm = form;

        await this.saveCurrEditForm();
    }

    public async saveCurrEditForm(): Promise<void> {
        await this.setItem(storeCommonKeys.CURR_FORM_E, this.currEditingForm);
    }

    public async getCurrAnswerForm(): Promise<FormFullLocal | null> {
        return this.currAnsweringForm;
    }

    public async setCurrAnswerForm(formInfo: FormFullLocal | null): Promise<void> {
        this.currAnsweringForm = formInfo;
        this.saveCurrAnswerForm();
    }

    public async saveCurrAnswerForm() {
        await this.setItem(storeCommonKeys.CURR_FORM_A, this.currAnsweringForm);
    }

    public async getTempAnswersFor(formInfo: FormFullLocal): Promise<QuestionAnswerDTO[] | undefined> {
        if (formInfo.form.formData.formId)
            return this.tempAnswersMap.get(formInfo.form.formData.formId);
        
        return undefined;
    }

    public async setTempAnswersFor(formInfo: FormFullLocal, answers: QuestionAnswerDTO[]): Promise<void> {
        if (formInfo.form.formData.formId) {
            this.tempAnswersMap.set(formInfo.form.formData.formId, answers);
            await this.saveTempAnswersMap();
        }
    }
    
    public async removeCurrentEditFormCond(formWrapper: FormFullLocal): Promise<void> {
        if (this.currEditingForm?.localFormId === formWrapper.localFormId) {
            this.setCurrEditForm(null);
            this.saveCurrEditForm();
        }
    }

    public async getEditingForms(): Promise<FormFullLocal[] | undefined> {
        let retVal: FormFullLocal[] = [];

        this.localFormsMap.forEach((value, key) => {
            retVal.push({ localFormId: key, form: value});
        });

        return retVal;
    }

    public async getFromEditingForms(localFormId: string): Promise<FormFullLocal | undefined> {
        let formInfo = this.localFormsMap.get(localFormId);

        if (formInfo) {
            return { localFormId, form: formInfo };
        }

        return undefined;
    }

    public async setEditingForm(form: FormFullLocal): Promise<void> {
        this.localFormsMap.set(form.localFormId, form.form);
        this.saveLocalFormsMap();
    }

    public async removeEditingForm(form: FormFullLocal): Promise<void> {
        this.localFormsMap.delete(form.localFormId);
        this.saveLocalFormsMap();
    }

    public async saveTempAnswersMap() {
        await this.setItem(storeCommonKeys.TEMP_ANSWERS, Array.from(this.tempAnswersMap));
    }

    public async getTemporaryAnswers(formId: number): Promise<QuestionAnswerDTO[] | undefined> {
        return this.tempAnswersMap.get(formId);
    }

    public async setTemporaryAnswer(formId: number, answer: QuestionAnswerDTO): Promise<void> {
        let answers = this.tempAnswersMap.get(formId)??[];

        answers = answers.filter(answ => answ.questionId !== answer.questionId);

        answers.push(answer);
        this.tempAnswersMap.set(formId, answers);
        
        this.saveTempAnswersMap();
    }

    public async removeTemporaryAnswer(formId: number, answer: QuestionAnswerDTO): Promise<void> {
        let answers = this.tempAnswersMap.get(formId);

        if (answers) {
            this.tempAnswersMap.set(formId, answers.filter(el => el.questionId !== answer.questionId));
            
            this.saveTempAnswersMap();
        }
    }

    public async getCurrStatistics(): Promise<FormStatisticsDTO | null> {
        return this.currStats;
    }

    public async setCurrStatistics(statsInfo: FormStatisticsDTO | null): Promise<void> {
        this.currStats = statsInfo;
        this.saveCurrStatisticsForm();
    }

    public async saveCurrStatisticsForm() {
        await this.setItem(storeCommonKeys.CURR_FORM_S, this.currStats);
    }

    private async forceRemoteGet<T>(itemKey: string): Promise<T | undefined> {
        let response = await this.remoteStore.getItem(itemKey);

        if (response) {
            let value: string | undefined = response.itemValue;

            if (value === "null" || value === undefined)
                return undefined;
            else {
                this.setItemLocally<string>(itemKey, value);
                return JSON.parse(value) as T;
            }
        } else {
            return undefined;
        }
    }

    public async getItem<T>(itemKey: string): Promise<T | undefined> {
        let localValue = localStorage.getItem(itemKey);

        if (localValue === null) {
            let response = await this.remoteStore.getItem(itemKey);

            if (response) {
                let value: string | undefined = response.itemValue;

                if (value === "null" || value === undefined)
                    return undefined;
                else {
                    this.setItemLocally<string>(itemKey, value);
                    return JSON.parse(value) as T;
                }
            } else {
                return undefined;
            }
        } else {
            return JSON.parse(localValue) as T;
        }
    }

    public async setItem<T>(itemKey: string, itemValue: T): Promise<void> {
        this.setItemLocally<T>(itemKey, itemValue);
        this.remoteStore.setItem<T>(itemKey, itemValue);
    }
    
    private async setItemLocally<T>(itemKey: string, itemValue: T): Promise<void> {
        localStorage.setItem(itemKey, JSON.stringify(itemValue));
    }

    public async commit(): Promise<void> {
        this.remoteStore.commit();
    }
}

type RemoteTransferType = {
    itemKey: string,
    itemValue?: string
}

class RemoteStore {
    static url: string = remote;

    async getItem(itemKey: string): Promise<RemoteTransferType | undefined> {
        if (!(await AUTH.isLoggedIn())) {
            return undefined;
        }

        let postData: RemoteTransferType = { itemKey };

        let response =
        await corsAxios.post(RemoteStore.url, postData)
        .then(resp => {
            return resp.data;
        })
        .catch(error => { console.log(error); return undefined; });

        return response;
    }

    async setItem<T>(itemKey: string, itemValue: T): Promise<void> {
        if (!(await AUTH.isLoggedIn())) {
            return;
        }

        let postData: RemoteTransferType = { itemKey, itemValue: JSON.stringify(itemValue) };

        corsAxios.patch(RemoteStore.url, postData)
        .catch(error => console.log(error));
    }

    async commit() {
        if (!(await AUTH.isLoggedIn())) {
            return;
        }

        let multiSet: RemoteTransferType[] = [];

        for (let entry of Object.entries(localStorage)) {
            multiSet.push({ itemKey: entry[0], itemValue: entry[1]})
        }

        corsAxios.put(RemoteStore.url, multiSet)
        .catch(error => console.log(error));
    }
}

let STORE: Store = new Store();

export default STORE;