interface FormDTO {
    formId?: number;
    title: string;
    description: string;
    restrictedTo: number;
    formLink: string | null;
    statsLink: string | null;
}

interface PossibleAnswerDTO {
    name: string;
    value: string;
}

interface QuestionDTO {
    questionId?: number;
    previousVersion?: number;
    questionNumber: number;
    questionPage: number;
    isRequired: boolean;
    answerType: number;
    question: string;
    possibleAnswers: Array<PossibleAnswerDTO>;
}

interface QuestionAnswerDTO {
    questionId: number;
    value: string;
}

interface FormBodyDTO {
    parentFormId?: number;
    questions: Array<QuestionDTO>;
}

interface FormFullDTO {
    formData: FormDTO;
    bodyData: FormBodyDTO;
}

interface AccessRequestDTO {
    entryId?: number;
    formId: number;
    usernameRequested: string;
    accessType: number;
}

interface AccessDataDTO {
    formId: number;
    requests: AccessRequestDTO[];
}

interface UserRegisterDTO {
    username: string;
    email: string;
    password: string;
    repeatedPassword: string;
}

interface UserLoginDTO {
    username: string;
    password: string;
}

interface PermissionRequestDTO {
    formLink: string;
    accessType: number;
}

interface AnswerPartDTO {
    questionId: number;
    answer: string;
}

interface AnswerDTO {
    formId: number;
    answers: AnswerPartDTO[];
}

interface SetUsernameDTO {
    username: string;
}

interface UserInfoDTO {
    username: string;
    email: string;
    isLocalUser: boolean;
}

interface UserInfoUpdateDTO {
    email?: string;
    password?: string;
    repeatedPassword?: string;
}

interface AnswerStatisticsDTO {
    answerCount: number;
    answerPercentage: number;
}

interface StatisticsEntryDTO {
    question: string;
    answerType: number;
    totalAnswers: number;
    answered: number;
    answers: string[] | null;
    answerToPartMap: Map<string, AnswerStatisticsDTO> | null;
    original: any;
}

interface FormStatisticsDTO {
    formTitle: string;
    formDescription: string;
    statistics: StatisticsEntryDTO[];
}

export type {
    FormDTO,
    PossibleAnswerDTO,
    QuestionDTO,
    QuestionAnswerDTO,
    FormBodyDTO,
    FormFullDTO,
    AccessRequestDTO,
    AccessDataDTO,
    UserRegisterDTO,
    UserLoginDTO,
    PermissionRequestDTO,
    AnswerPartDTO,
    AnswerDTO,
    SetUsernameDTO,
    UserInfoDTO,
    UserInfoUpdateDTO,
    AnswerStatisticsDTO,
    StatisticsEntryDTO,
    FormStatisticsDTO
}