import { FormFullDTO, QuestionAnswerDTO } from "./dtos";

interface TemporaryAnswerMap {
    questionId: number,
    answers: QuestionAnswerDTO[]
}

interface FormFullLocal {
    localFormId: string,
    form: FormFullDTO
}

export type { TemporaryAnswerMap, FormFullLocal }