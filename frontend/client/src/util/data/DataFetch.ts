import { AxiosResponse } from "axios";
import corsAxios from "../../config/axiosConfig";
import connectionConfig from "../../config/connectionConfig";
import { AccessDataDTO, AccessRequestDTO, AnswerDTO, FormBodyDTO, FormDTO, FormFullDTO, FormStatisticsDTO, PermissionRequestDTO, SetUsernameDTO, UserInfoDTO, UserInfoUpdateDTO } from "../interfaces/dtos";
import { FormFullLocal } from "../interfaces/StoreData";

class DataFetch {
    public async fetchOwnedForms(): Promise<FormFullDTO[]> {
        let response = await corsAxios.get(connectionConfig.API_FORM_OWNED_FETCH, { data: null });
        return response.data;
    }

    public async fetchSharedForms(): Promise<FormFullDTO[]> {
        let response = await corsAxios.get(connectionConfig.API_FORM_SHARED_FETCH, { data: null });
        return response.data;
    }

    public async getUsernamesLike(usernamePattern: string): Promise<string[]> {
        let response = await corsAxios.get(connectionConfig.USERNAMES_FETCH_ENDPOINT + usernamePattern);
        return response.data;
    }

    public async fetchFormStatistics(formInfo: FormDTO): Promise<FormStatisticsDTO> {
        let response = await corsAxios.post<FormStatisticsDTO>(connectionConfig.API_FORM_STATS_FETCH, formInfo);
        let data = response.data;
        data.statistics.forEach(en => en.original = en.answerToPartMap);
        
        for (let statEntry of data.statistics) {
            if (statEntry.answerToPartMap) {
                statEntry.answerToPartMap = new Map(Object.entries(statEntry.answerToPartMap));
            }
        }

        return data;
    }

    public async fetchStatisticsByLink(formStatsLink: string): Promise<FormStatisticsDTO> {
        let response = await corsAxios.get<FormStatisticsDTO>(connectionConfig.API_FORM_STATS_FETCH_LINK + formStatsLink);
        let data = response.data;
        data.statistics.forEach(en => en.original = en.answerToPartMap);
        
        for (let statEntry of data.statistics) {
            if (statEntry.answerToPartMap) {
                statEntry.answerToPartMap = new Map(Object.entries(statEntry.answerToPartMap));
            }
        }

        return data;
    }

    public async createNewForm(formMetadata: FormDTO): Promise<FormDTO> {
        let response = await corsAxios.post(connectionConfig.API_FORM_CREATE, formMetadata);
        return response.data;
    }

    public async updateFormMeta(formMetadata: FormDTO): Promise<void> {
        await corsAxios.post(connectionConfig.API_FORM_UPDATE, formMetadata);
    }

    public async deleteForm(localForm: FormFullLocal): Promise<void> {
        await corsAxios.delete(connectionConfig.API_FORM_DELETE, { data: localForm.form });
    }

    public async saveOver(localForm: FormFullLocal): Promise<FormBodyDTO> {
        let response = await corsAxios.put(connectionConfig.API_FORM_SAVE_OVER, localForm.form.bodyData)
        return response.data;
    }

    public async saveInto(localForm: FormFullLocal): Promise<void> {
        await corsAxios.patch(connectionConfig.API_FORM_SAVE_INTO, localForm.form.bodyData);
    }

    public async getPendingAccessRequests(localForm: FormFullLocal): Promise<AccessRequestDTO[]> {
        let response = await corsAxios.post(connectionConfig.API_FORM_PERMISSION_FETCH, localForm.form.formData);
        return response.data;
    }

    public async updatePermissions(data: AccessDataDTO): Promise<void> {
        await corsAxios.post(connectionConfig.API_FORM_PERMISSION_UPDATE, data);
    }

    public async grantPermission(request: AccessRequestDTO): Promise<void> {
        await corsAxios.post(connectionConfig.API_FORM_PERMISSION_GRANT, request);
    }

    public async denyPermission(request: AccessRequestDTO): Promise<void> {
        await corsAxios.post(connectionConfig.API_FORM_PERMISSION_DENY, request);
    }

    public async getActivePermissions(localForm: FormFullLocal): Promise<AccessRequestDTO[]> {
        let response = await corsAxios.post(connectionConfig.API_FORM_PERMISSION_ACTIVE_FETCH, localForm.form.formData);
        return response.data;
    }

    public async requestPermission(permissionRequest: PermissionRequestDTO): Promise<AxiosResponse<any, any>> {
        return corsAxios.post(connectionConfig.API_FORM_PERMISSION_REQUEST, permissionRequest);
    }

    public async fetchForAnswering(formLink: string): Promise<FormFullDTO> {
        let response = await corsAxios.get(connectionConfig.API_FORM_DATA_FETCH + formLink);
        return response.data;
    }

    public async sendResponse(response: AnswerDTO): Promise<void> {
        await corsAxios.put(connectionConfig.API_FORM_ANSWER, response);
    }

    public async stopParticipation(formInfo: FormDTO): Promise<void> {
        await corsAxios.delete(connectionConfig.API_FORM_PERMISSION_CANCEL, { data: formInfo });
    }

    public async fetchUserInfo(): Promise<UserInfoDTO> {
        let response = await corsAxios.get(connectionConfig.USER_INFO_ENDPOINT);
        return response.data;
    }

    public async updateUserInfo(newUserInfo: UserInfoUpdateDTO): Promise<void> {
        await corsAxios.post(connectionConfig.USER_INFO_CHANGE_ENDPOINT, newUserInfo);
    }

    public async setUsername(username: SetUsernameDTO): Promise<AxiosResponse<any, any>> {
        return await corsAxios.post(connectionConfig.SETUSERNAME_ENDPOINT, username);
    }

    public async downloadStatisticsPDF(statsInfo: FormStatisticsDTO): Promise<void> {
        let statsFinal = statsInfo;
        statsFinal.statistics.forEach(en => en.answerToPartMap = en.original);

        await corsAxios.post(connectionConfig.FORM_STATS_PDF, statsFinal, { responseType: "blob" })
        .then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            
            const link: HTMLAnchorElement = document.createElement('a');
            let filename: string = "statistics.pdf"; // response.headers["Content-Disposition"].split("; filename=")[1].trim().replaceAll("\"", "");
            link.href = url;
            link.setAttribute('download', filename);
            document.body.appendChild(link);
            link.click();
        })
        .catch(err => console.log(err));
    }

    public async downloadStatisticsCSV(statsInfo: FormStatisticsDTO): Promise<void> {
        let statsFinal = statsInfo;
        statsFinal.statistics.forEach(en => en.answerToPartMap = en.original);

        await corsAxios.post(connectionConfig.FORM_STATS_CSV, statsFinal, { responseType: "blob" })
        .then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            
            const link: HTMLAnchorElement = document.createElement('a');
            let filename: string = "statistics.csv"; // response.headers["Content-Disposition"].split("; filename=")[1].trim().replaceAll("\"", "");
            link.href = url;
            link.setAttribute('download', filename);
            document.body.appendChild(link);
            link.click();
        })
        .catch(err => console.log(err));
    }

    public async downloadStatisticsTXT(statsInfo: FormStatisticsDTO): Promise<void> {
        let statsFinal = statsInfo;
        statsFinal.statistics.forEach(en => en.answerToPartMap = en.original);

        await corsAxios.post(connectionConfig.FORM_STATS_TXT, statsFinal, { responseType: "blob" })
        .then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            
            const link: HTMLAnchorElement = document.createElement('a');
            let filename: string = "statistics.txt"; // response.headers["Content-Disposition"].split("; filename=")[1].trim().replaceAll("\"", "");
            link.href = url;
            link.setAttribute('download', filename);
            document.body.appendChild(link);
            link.click();
        })
        .catch(err => console.log(err));
    }
}

const DATA_SERVER = new DataFetch();

export default DATA_SERVER;