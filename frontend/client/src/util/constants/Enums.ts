export enum LoginProvider {
    LOCAL = 0,
    GOOGLE = 1,
    FACEBOOK = 2
}

export enum UserStatus {
    ENABLED = 0,
    ACTIVATION_PENDING = 1,
    DISABLED = 2,
    DELETION_PENDING = 3,
    DELETED = 4
}

export enum FormRestrictionType {
    NONE = 0,
    LOGGED_IN = 1,
    SELECTED = 2
}

export enum FormAccessType {
    VIEW = 0,
    EDIT = 1,
    STAT = 2
}

export enum AnswerType {
    SHORTTEXT = 0,
    LONGTEXT = 1,
    NUMBER = 2,
    SCALE = 3,
    RADIO = 4,
    CHECKBOX = 5,
    TRUEFALSE = 6,
    SCALEPERCENT = 7
}