import { ArrowLeftIcon } from "@heroicons/react/outline";
import corsAxios from "../../config/axiosConfig";
import classNames from "classnames";
import React from "react";
import { useNavigate } from "react-router-dom";
import Button from "../../components/universal/form/Button";
import connectionConfig from "../../config/connectionConfig";
import { HttpStatus } from "../../util/constants/HttpStatus";
import { UserLoginDTO } from "../../util/interfaces/dtos";
import STORE from "../../util/store/Store";

function Login() {
    const navigate = useNavigate();

    const [loginInfo, setLoginInfo] = React.useState<UserLoginDTO>({username: "", password: ""});

    const [loginError, setLoginError] = React.useState<boolean>(false);

    const inputClasses = classNames(
        "mt-2 block rounded-lg p-2 border-2 border-gray-400 focus:border-gray-500/90 outline-none w-full"
    );

    function updateLoginInfo(ev: any) {
        const { name, value } = ev.target;

        setLoginInfo({ ...loginInfo, [name]: value });
    }

    function performLogin() {
        corsAxios.post(connectionConfig.LOGIN_ENDPOINT, loginInfo)
        .then((resp) => {
            if (resp.status === HttpStatus.OK) {
                STORE.loadRemote()
                .then(() => {
                    navigate("/");
                })
                .catch(() => console.log("Error loading remote store"));
            } else {
                setLoginError(true);
            }
        })
        .catch(error => {
            console.log("Login failed: ");
            console.log(error);

            setLoginError(true);
        });
    }

    React.useEffect(() => {
        let listener = (event: KeyboardEvent) => {
            if (event.key == "Enter") {
                performLogin();
            }
        };

        document.addEventListener("keypress", listener);

        return () => {
            document.removeEventListener("keypress", listener);
        };
    }, [loginInfo]);

    return (
        <div className="flex flex-col items-center justify-center text-2xl bg-gray-300 pt-2 pb-4 w-full min-h-screen max-h-[auto]" style={{ height: "min(inherit, 100vh)" }}>
            <div className="flex flex-row sticky bg-gray-300 md:bg-opacity-0 md:fixed top-0 items-center justify-start cursor-pointer text-3xl w-[95%] self-start" onClick={() => navigate("/")}>
                <div className="ml-4 mt-4" title="To homepage">
                    <ArrowLeftIcon
                        className="w-8 mr-4"
                    />
                </div>
            </div>
            <div className="flex flex-col items-center w-full mt-2">
                <h1 className="text-3xl md:text-4xl font-bold w-[90%] md:w-[70%]">Terraforms login</h1>

                <div className="flex flex-col items-center w-[90%] md:w-[70%] my-10">
                    <input
                        className={inputClasses}
                        type="text"
                        name="username"
                        value={loginInfo.username}
                        placeholder="E-mail"
                        onChange={updateLoginInfo}
                    />

                    <input
                        className={inputClasses}
                        type="password"
                        name="password"
                        value={loginInfo.password}
                        placeholder="Password"
                        onChange={updateLoginInfo}
                    />

                    <div className="flex flex-row items-center justify-around w-full mt-4">
                        <Button
                            className={"bg-blue-700 hover:bg-blue-600 text-white py-1 basis-4/5 md:basis-3/5"}
                            styling={{ roundedPartial: true}}
                            text="Log in"
                            onclick={performLogin}
                        />
                    </div>
                    {(loginError) ? <span className="text-red-600 text-lg mt-2">Email or password invalid</span> :""}
                </div>

                <hr className="w-[80%] border-gray-600/70 mb-10" />

                <div className="flex flex-col items-center justify-center w-[90%] text-center text-xl">
                    <div className="flex flex-col md:flex-row items-center justify-center w-2/3 mb-3">
                        <div>Don't have an account?</div>
                        <Button
                            className={"bg-blue-700 hover:bg-blue-600 basis-1/12 text-white ml-4 mt-4 md:mt-0"}
                            styling={{ paddedMedium: true, roundedPartial: true}}
                            text="Register"
                            onclick={() => navigate("/register")}
                        />
                    </div>

                    <div className="flex flex-col md:flex-row items-center justify-center w-2/3">
                        <div>Or log in with:</div>
                        <Button
                            className={"bg-orange-700 hover:bg-orange-700/80 border-none text-white outline-none ml-4 mt-4 md:mt-0"}
                            styling={{ paddedMedium: true, roundedPartial: true }}
                            text="Google"
                            onclick={() => window.location.href = connectionConfig.OAUTH2_GOOGLE}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;