import corsAxios from "../../config/axiosConfig";
import classNames from "classnames";
import React from "react";
import { HttpStatus } from "../../util/constants/HttpStatus";
import connectionConfig from "../../config/connectionConfig";
import STORE from "../../util/store/Store";
import AUTH from "../../components/auth/AuthHandler";

function Logout() {
    const logoutDivClasses = classNames(
        "fixed top-0 left-0 w-screen h-screen",
        "flex flex-col items-center justify-center",
        "font-serif font-bold text-3xl text-cyan-800"
    );

    React.useEffect(() => {
        AUTH.isLoggedIn()
        .then(loggedIn => {
            if (loggedIn) {
                setTimeout(() => {
                    STORE.setCurrStatistics(null);
                    corsAxios.post(connectionConfig.LOGOUT_ENDPOINT)
                    .then(resp => {
                        if (resp.status === HttpStatus.OK) {
                            window.location.replace("/");
                        } else {
                            console.log("Error while logging out...");
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    });
                }, 1000);
            }
        });
    }, []);

    return (
        <div className={logoutDivClasses}>
            <h1 className="text-3xl">Logout</h1>
            <hr className="border-gray-700/80 w-4/5 my-10" />
            <p className="w-4/5 text-center">You are being logged out. Until next time, farewell my friend.</p>
        </div>
    )
}

export default Logout;