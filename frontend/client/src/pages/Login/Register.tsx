import { ArrowLeftIcon } from "@heroicons/react/outline";
import corsAxios from "../../config/axiosConfig";
import classNames from "classnames";
import React from "react";
import { useNavigate } from "react-router-dom";
import Button from "../../components/universal/form/Button";
import connectionConfig from "../../config/connectionConfig";
import { HttpStatus } from "../../util/constants/HttpStatus";
import { UserRegisterDTO } from "../../util/interfaces/dtos";

function Register() {
    const navigate = useNavigate();

    const usernameRef = React.useRef<HTMLInputElement>(null);
    const [usernameInvalid, setUsernameInvalid] = React.useState<boolean>(false);

    const emailRef = React.useRef<HTMLInputElement>(null);
    const [emailInvalid, setEmailInvalid] = React.useState<boolean>(false);

    const passwordRef = React.useRef<HTMLInputElement>(null);
    const [passwordInvalid, setPasswordInvalid] = React.useState<boolean>(false);

    const repeatRef = React.useRef<HTMLInputElement>(null);
    const [repeatInvalid, setRepeatInvalid] = React.useState<boolean>(false);

    const [registerError, setRegisterError] = React.useState<boolean>(false);

    const [registerInfo, setRegisterInfo] = React.useState<UserRegisterDTO>({username: "", email: "", password: "", repeatedPassword: ""});
    const inputClasses = classNames(
        "mt-2 block rounded-lg p-2 border-2 border-gray-400 focus:border-gray-500/90 outline-none w-full",
        "invalid:border-red-600 invalid:focus:border-red-600  invalid:focus:shadow-md invalid:focus:shadow-red-300"
    );

    function updateInfo(ev: any) {
        const { name, value } = ev.target;

        setRegisterInfo({ ...registerInfo, [name]: value});
    }

    function runCheck(event: React.FocusEvent<HTMLInputElement>) {
        if (event.target !== repeatRef.current) {
            event.target.required = true;
        }

        if (event.target === usernameRef.current) {
            setUsernameInvalid(registerInfo.username.length < 5);

            if (registerInfo.username.length < 5) {
                usernameRef.current.setCustomValidity("Username must be at least 5 characters long");
            } else {
                usernameRef.current.setCustomValidity("");
            }
        }

        if (event.target === passwordRef.current) {
            setPasswordInvalid(registerInfo.password.length < 8);

            if (registerInfo.password.length < 8) {
                passwordRef.current.setCustomValidity("Password must be at least 8 characters long");
            } else if (registerInfo.password !== registerInfo.repeatedPassword) {
                repeatRef.current?.setCustomValidity("Passwords don't match");
                passwordRef.current.setCustomValidity("Passwords don't match");
                setRepeatInvalid(true);
            } else {
                setRepeatInvalid(false);
                repeatRef.current?.setCustomValidity("");
                passwordRef.current.setCustomValidity("");
            }
        }

        if (event.target === repeatRef.current) {
            setRepeatInvalid(registerInfo.password !== registerInfo.repeatedPassword);
            
            if (registerInfo.password !== registerInfo.repeatedPassword) {
                repeatRef.current.setCustomValidity("Passwords don't match");
                passwordRef.current?.setCustomValidity("Passwords don't match");
            } else {
                repeatRef.current.setCustomValidity("");
                passwordRef.current?.setCustomValidity("");
            }
        }

        if (event.target === emailRef.current) {
            emailRef.current.setCustomValidity("");
            setEmailInvalid(!emailRef.current.validity.valid);

            if (!emailRef.current.validity.valid) {
                emailRef.current.setCustomValidity("Email not in valid format");
            }
        }
    }

    function registerUser() {
        let invalid: boolean = false;

        invalid ||= usernameInvalid;
        invalid ||= passwordInvalid;
        invalid ||= repeatInvalid;
        invalid ||= emailInvalid;

        if (!invalid) {
            corsAxios.post(connectionConfig.REGISTER_ENDPOINT, registerInfo)
            .then(resp => {
                if (resp.status === HttpStatus.OK) {
                    window.location.replace("/");
                } else {
                    setRegisterError(true);
                }
            })
            .catch(error => {
                console.log(error);
                setRegisterError(true);
            });
        }
    }

    React.useEffect(() => {
        let listener = (event: KeyboardEvent) => {
            if (event.key == "Enter") {
                registerUser();
            }
        };

        document.addEventListener("keypress", listener);

        return () => {
            document.removeEventListener("keypress", listener);
        };
    }, [registerInfo]);

    return (
        <div className="flex flex-col items-center justify-center text-2xl bg-gray-300 pt-2 pb-4 w-full min-h-screen max-h-[auto]">
            <div className="flex flex-row sticky bg-gray-300 md:bg-opacity-0 md:fixed top-0 items-center justify-start cursor-pointer text-3xl w-[95%] self-start" onClick={() => navigate("/")}>
                <div className="ml-4 mt-4" title="To homepage">
                    <ArrowLeftIcon
                        className="w-8 mr-4"
                    />
                </div>
            </div>
            <div className="flex flex-col items-center w-full mt-2">
                <h1 className="text-3xl md:text-4xl font-bold w-[90%] md:w-[70%]">Terraforms register</h1>

                <div className="flex flex-col items-center w-[90%] md:w-[70%] my-10">
                    <div className="w-full">
                        <input
                            ref={usernameRef}
                            className={inputClasses}
                            type="text"
                            name="username"
                            value={registerInfo.username}
                            minLength={5}
                            placeholder="Username"
                            onChange={updateInfo}
                            onBlur={runCheck}
                        />
                        {(usernameInvalid) ? <span className="text-red-600 text-sm w-4/5">{usernameRef.current?.validationMessage}</span>:""}
                    </div>

                    <div className="w-full">
                        <input
                            ref={emailRef}
                            className={inputClasses}
                            type="email"
                            name="email"
                            value={registerInfo.email}
                            placeholder="E-mail"
                            onChange={updateInfo}
                            onBlur={runCheck}
                        />
                        {(emailInvalid) ? <span className="text-red-600 text-sm w-4/5">{emailRef.current?.validationMessage}</span>:""}
                    </div>

                    <div className="w-full">
                        <input
                            ref={passwordRef}
                            className={inputClasses}
                            type="password"
                            name="password"
                            value={registerInfo.password}
                            minLength={8}
                            placeholder="Password"
                            onChange={updateInfo}
                            onBlur={runCheck}
                        />
                        {(repeatInvalid || passwordInvalid) ? <span className="text-red-600 text-sm w-4/5">{passwordRef.current?.validationMessage}</span>:""}
                    </div>

                    <div className="w-full">
                        <input
                            ref={repeatRef}
                            className={inputClasses}
                            type="password"
                            name="repeatedPassword"
                            value={registerInfo.repeatedPassword}
                            placeholder="Repeat password"
                            onChange={updateInfo}
                            onBlur={runCheck}
                        />
                        {(repeatInvalid || passwordInvalid) ? <span className="text-red-600 text-sm w-4/5">Passwords don't match</span>:""}
                    </div>

                    {(registerError) ? <span className="text-red-600 text-lg mt-2 w-4/5">Username or email already taken</span> :""}

                    <div className="flex flex-row items-center justify-around w-full mt-4">
                        <Button
                            className={"bg-blue-700 hover:bg-blue-600 text-white py-1 basis-4/5 md:basis-3/5"}
                            styling={{ roundedPartial: true}}
                            text="Register"
                            onclick={registerUser}
                        />
                    </div>
                </div>

                <hr className="w-[80%] border-gray-600/70 mb-10" />

                <div className="flex flex-col items-center justify-center w-[90%] text-center text-xl">
                    <div className="flex flex-col md:flex-row items-center justify-center w-2/3 mb-3">
                        <div>Already have an account?</div>
                        <Button
                            className={"bg-blue-700 hover:bg-blue-600 basis-1/12 text-white ml-4 mt-4 md:mt-0"}
                            styling={{ paddedMedium: true, roundedPartial: true}}
                            text="Login"
                            onclick={() => navigate("/login")}
                        />
                    </div>

                    <div className="flex flex-col md:flex-row items-center justify-center w-2/3">
                        <div>Or log in with:</div>
                        <Button
                            className={"bg-orange-700 hover:bg-orange-700/80 border-none text-white outline-none ml-4 mt-4 md:mt-0"}
                            styling={{ paddedMedium: true, roundedPartial: true }}
                            text="Google"
                            onclick={() => window.location.href = connectionConfig.OAUTH2_GOOGLE}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;