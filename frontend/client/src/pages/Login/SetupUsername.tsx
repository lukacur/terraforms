import classNames from "classnames";
import React from "react";
import { useNavigate } from "react-router-dom";
import Button from "../../components/universal/form/Button";
import DATA_SERVER from "../../util/data/DataFetch";
import { SetUsernameDTO } from "../../util/interfaces/dtos";

function SetupUsername() {
    const navigate = useNavigate();

    const usernameRef = React.useRef<HTMLInputElement>(null);
    const [usernameInvalid, setUsernameInvalid] = React.useState<boolean>(false);

    const [username, setUsername] = React.useState("");
    const inputClasses = classNames(
        "mt-2 block rounded-lg p-2 border-2 border-gray-400 focus:border-gray-500/90 outline-none w-full",
        "invalid:border-red-600 invalid:focus:border-red-600  invalid:focus:shadow-md invalid:focus:shadow-red-300"
    );

    function updateUsername(ev: any) {
        const { value } = ev.target;

        setUsername(value);
    }

    function commitUsername() {
        let dto: SetUsernameDTO = { username };

        let invalid: boolean = false;

        invalid ||= usernameInvalid;

        if (!invalid) {
            DATA_SERVER.setUsername(dto)
            .then(() => navigate("/"));
        }
    }

    function runCheck(event: React.FocusEvent<HTMLInputElement>) {
        if (event.target === usernameRef.current) {
            setUsernameInvalid(username.length < 5);

            if (username.length < 5) {
                usernameRef.current.setCustomValidity("Username must be at least 5 characters long");
            } else {
                usernameRef.current.setCustomValidity("");
            }
        }
    }

    React.useEffect(() => {
        let listener = (event: KeyboardEvent) => {
            if (event.key == "Enter") {
                commitUsername();
            }
        };

        document.addEventListener("keypress", listener);

        return () => {
            document.removeEventListener("keypress", listener);
        };
    }, [username]);

    return (
        <div className="flex items-center justify-center bg-gray-300 pt-2 pb-4 w-screen min-h-screen max-h-[auto] text-lg md:text-2xl">
            <div className="flex flex-col items-center w-full">
                <h1 className="w-4/5 text-3xl md:text-4xl font-bold">New user - username setup</h1>
                <hr className="w-4/5 border-gray-600/70 my-10" />
                <p className="w-4/5 text-justify">
                    Your username uniquely identifies you and cannot be changed. Choose wisely, your future is in your hands!
                </p>

                <div className="flex flex-col items-center w-[90%] md:w-[70%] my-10">
                    <input
                        ref={usernameRef}
                        className={inputClasses}
                        type="text"
                        name="username"
                        value={username}
                        minLength={5}
                        placeholder="Username"
                        onChange={updateUsername}
                        onBlur={runCheck}
                    />
                    {(usernameInvalid) ? <span className="text-red-600 text-sm">{usernameRef.current?.validationMessage}</span>:""}

                    <div className="flex flex-row items-center justify-around w-full mt-6">
                        <Button
                            className={"bg-blue-700 hover:bg-blue-600 basis-2/5 text-white py-1 basis-4/5 md:basis-3/5"}
                            styling={{ roundedPartial: true }}
                            text="Set username"
                            onclick={commitUsername}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SetupUsername;