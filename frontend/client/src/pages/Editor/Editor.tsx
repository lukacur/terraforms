import React from "react";
import { ArrowCircleUpIcon, ArrowCircleDownIcon, PlusCircleIcon, TrashIcon } from "@heroicons/react/outline";
import QuestionCard from "../../components/editor/QuestionCard";
import Button from "../../components/universal/form/Button";
import { QuestionDTO } from "../../util/interfaces/dtos";
import { MenuAlt2Icon } from "@heroicons/react/solid";
import STORE from "../../util/store/Store";
import { FormFullLocal } from "../../util/interfaces/StoreData";
import DATA_SERVER from "../../util/data/DataFetch";
import { Link, useNavigate } from "react-router-dom";
import LoadingCircle from "../../components/universal/base/LoadingCircle";
import Menu from "../../components/universal/dropdown/Menu";
import InfoCard from "../../components/universal/base/InfoCard";

function Editor() {
    const navigate = useNavigate();
    const menuRef = React.useRef<HTMLDivElement>(null);

    const [softReloadState, softReload] = React.useState<boolean>(false);

    const [formMetaSent, setFromMetaSent] = React.useState<boolean>(false);

    const [loading, setLoading] = React.useState<boolean>(true);
    const [saveMenuActive, setSaveMenuActive] = React.useState<boolean>(false);

    const [infoCardShown, setInfoCardShown] = React.useState<boolean>(false);
    const [infoCardText, setInfoCardText] = React.useState<string>("");
    const [infoCardClassName, setInfoCardClassName] = React.useState<string>("");

    const [questions, setQuestions] = React.useState<QuestionDTO[]>([]);
    const [originalQuestionsLength, setOriginalQuestionsLength] = React.useState<number>(0);
    const [formInfo, setFormInfo] = React.useState<FormFullLocal>(
        {
            localFormId: "lo_-1",
            form: {
                bodyData: { parentFormId: -1, questions: [] },
                formData: { description: "", formId: -1, restrictedTo: -1, title: "", formLink: null, statsLink: null }
            }
        }
    );

    function addBlankQuestion(): void {
        let tmpq: QuestionDTO = {
            answerType: -1,
            isRequired: false,
            questionNumber: questions.length + 1,
            questionPage: 1,
            question: "",
            possibleAnswers: []
        };

        setQuestions([...questions, tmpq]);
    }

    function arrowClick(index: number, increase: boolean): void {
        if (index < 0) {
            return;
        }
        
        let question: QuestionDTO = questions[index];
        let otherIdx: number | undefined = undefined;
        let other: QuestionDTO | undefined = undefined;

        if (increase && index > 0) {
            otherIdx = index - 1;
            other = questions[otherIdx];

            questions[index] = other;
            questions[otherIdx] = question;
        } else if (!increase && index < questions.length - 1) {
            otherIdx = index + 1;
            other = questions[otherIdx];

            questions[index] = other;
            questions[otherIdx] = question;
        }

        if (other !== undefined && otherIdx !== undefined) {
            other.questionNumber = index + 1;
            question.questionNumber = otherIdx + 1;
        }

        setQuestions([...questions]);
    }

    function deleteQuestion(index: number) {
        questions.splice(index, 1);

        for (let i: number = index; i < questions.length; i++) {
            questions[i].questionNumber = i + 1;
        }
        setQuestions([...questions]);
    }

    function getMenuOptions() {
        let options: [optionText: string, optionClickCallback: () => void, optionStyle?: string, dividerAfter?: boolean][] = [
            [(formInfo.localFormId.startsWith("lo_")) ? "Save new" : "Save changes over", saveOver]
        ];

        if (!formInfo.localFormId.startsWith("lo_") && originalQuestionsLength === questions.length
                && questions.filter(q => q.questionId === undefined).length === 0) {
            options.push(["Insert changes", insertInto, "", true]);
        } else {
            let option = options[0];
            option[2] = "";
            option[3] = true;
        }

        options.push(["Save locally", saveLocally]);

        return options;
    }

    function sendFormMeta() {
        if (!formMetaSent) {
            setFromMetaSent(true);
            DATA_SERVER.updateFormMeta(formInfo.form.formData);
        }
    }

    function saveOver() {
        let newForm: FormFullLocal = {
            localFormId: formInfo.localFormId,
            form: formInfo.form
        };
        newForm.form.bodyData.questions = questions;

        let saveOverFn = (formData: FormFullLocal) => {
            sendFormMeta();
            DATA_SERVER.saveOver(formData)
            .then((servResp) => {
                formData.form.bodyData = servResp;

                STORE.removeEditingForm(formData);

                STORE.setCurrEditForm(formData)
                .then(() => setFormInfo(formData));
                setQuestions(formData.form.bodyData.questions);
            })
            .then(() => {
                setInfoCardText("Changes saved successfully!");
                setInfoCardClassName("bg-green-600 border border-2 border-green-500/75");
                
                if (!infoCardShown) {
                    setInfoCardShown(true);
                    setTimeout(() => setInfoCardShown(false), 4000);
                }
            });
        };

        if (formInfo.localFormId.startsWith("lo_")) {
            DATA_SERVER.createNewForm(formInfo.form.formData)
            .then((headerData) => {
                newForm.form.formData = headerData;
                newForm.localFormId = headerData.formId?.toString() || (() => { throw new Error("This should never happen") })();
                newForm.form.bodyData.parentFormId = headerData.formId;
                STORE.removeEditingForm(formInfo);
                saveOverFn(newForm);
            });
        } else {
            saveOverFn(newForm);
        }

        softReload(!softReloadState);
    }

    function insertInto() {
        let newForm: FormFullLocal = {
            localFormId: formInfo.localFormId,
            form: formInfo.form
        };

        newForm.form.bodyData.questions = questions;

        sendFormMeta();
        DATA_SERVER.saveInto(newForm)
        .then(() => {
            STORE.removeEditingForm(newForm);
            STORE.setCurrEditForm(newForm);
            setFormInfo(newForm);
        })
        .then(() => {
            setInfoCardText("Changes inserted successfully!");
            setInfoCardClassName("bg-green-600 border border-2 border-green-500/75");
            
            if (!infoCardShown) {
                setInfoCardShown(true);
                setTimeout(() => setInfoCardShown(false), 4000);
            }
        });

        softReload(!softReloadState);
    }

    function saveLocally() {
        let newForm: FormFullLocal = {
            localFormId: formInfo.localFormId,
            form: { ...formInfo.form }
        };
        
        newForm.form.bodyData.questions = [ ...questions ];
        STORE.setEditingForm(newForm);
        STORE.setCurrEditForm(newForm)
        .then(() => {
            setInfoCardText("Changes saved locally");
            setInfoCardClassName("bg-green-600 border border-2 border-green-500/75");
            
            if (!infoCardShown) {
                setInfoCardShown(true);
                setTimeout(() => setInfoCardShown(false), 4000);
            }
        });
        setFormInfo(newForm);

        softReload(!softReloadState);
    }

    const [intervalInfo, setIntervalInfo] = React.useState<NodeJS.Timer>();
    const [secondsLeft, setSecondsLeft] = React.useState<number>(2);

    React.useEffect(() => {
        STORE.getCurrentEditForm()
        .then(form => {
            if (form) {
                setFormInfo(form);

                let qsts = form.form.bodyData.questions;
                qsts.sort((q1, q2) => q1.questionNumber - q2.questionNumber);

                for (let q of qsts) {
                    q.possibleAnswers.sort(
                        (a1, a2) => {
                            if (a1.value > a2.value) {
                                return 1;
                            } else if (a1.value == a2.value) {
                                return 0;
                            } else {
                                return -1;
                            }
                        }
                    );
                }

                setQuestions(qsts);
                setOriginalQuestionsLength(qsts.length);
                setLoading(false);
            } else if (intervalInfo === undefined) {
                let tmp = 2;
                let ii = setInterval(() => {
                    if (tmp > 0) {
                        setSecondsLeft(--tmp);
                        return;
                    }

                    navigate("/dashboard");
                }, 1000);

                setTimeout(() => clearInterval(ii), 3000);

                setIntervalInfo(ii);
            }
        });

        let listener = (ev: any) => {
            if (menuRef.current && !menuRef.current.contains(ev.target)) {
                setSaveMenuActive(false);
            }
        };

        document.addEventListener("mousedown", listener);
        document.addEventListener("touchstart", listener);

        return () => {
            document.removeEventListener("mousedown", listener);
            document.removeEventListener("touchstart", listener);
        }
    }, [softReloadState, menuRef]);

    return (
        <div>
            {(!loading) ? (
                <>
                <div className="flex flex-col items-center w-full bg-gray-100 pb-8">
                    <div className="bg-gray-300 px-8 pt-4 pb-8 mb-10 w-full">
                        <h1 className="font-bold text-4xl text-center w-full">{formInfo?.form.formData.title}</h1>
                        <hr className="border-[2px] border-gray-400/25 my-5 w-full"/>
                        <div className="flex items-center justify-center w-full">
                            <h2 className="text-2xl text-justify">{formInfo?.form.formData.description}</h2>
                        </div>
                    </div>

                    {questions.map((q, i) => (
                        <div className="grid grid-cols-12 grid-rows-2 bg-gray-300 rounded-xl py-4 w-[85%] mb-5" key={i}>
                            <QuestionCard
                                classes={"row-start-1 row-end-3 col-start-3 col-end-11 md:col-end-12"}
                                question={q}
                                questionUpdateCallback={(newQst) => {
                                    let newq: QuestionDTO[] = [...questions];
                                    newq[i] = newQst;
                                    setQuestions(newq);
                                }}
                            />

                            <div className="grid grid-cols-1 lg:grid-cols-2 grid-rows-3 lg:grid-rows-2 row-start-1 row-end-3 col-start-1 col-end-3">
                                <div className="flex justify-center items-center row-start-2 row-end-3 lg:row-start-1 lg:row-end-3 font-serif text-center text-2xl">
                                        {q.questionNumber}
                                </div>

                                <div className="w-full px-1 row-start-1 row-end-2 lg:col-start-2 lg:col-end-3 self-end justify-self-center">
                                    <Button
                                        text={""}
                                        icon={<ArrowCircleUpIcon className="w-5" />}
                                        className={"w-full py-[80%] xs:py-1 bg-[#006BA6] disabled:hover:bg-[#006BA6] hover:bg-editorArrowHovered text-white"}
                                        styling={{roundedFull: true}}
                                        enabled={i !== 0}
                                        onclick={() => arrowClick(i, true)}
                                    />
                                </div>

                                <div className="w-full px-1 row-start-3 row-end-4 lg:row-start-2 lg:row-end-3 lg:col-start-2 lg:col-end-3 self-start justify-self-center">
                                    <Button
                                        text={""}
                                        icon={<ArrowCircleDownIcon className="w-5" />}
                                        className={"w-full py-[80%] xs:py-1 bg-[#006BA6] disabled:hover:bg-[#006BA6] hover:bg-editorArrowHovered text-white"}
                                        styling={{roundedFull: true}}
                                        enabled={i !== questions.length - 1}
                                        onclick={() => arrowClick(i, false)}
                                    />
                                </div>
                            </div>

                            <div className="w-full py-1 px-2 row-start-1 row-end-3 col-start-11 md:col-start-12 col-end-13 self-center justify-self-center">
                                <Button
                                    text={""}
                                    icon={<TrashIcon className="w-5" />}
                                    className={"w-full py-[80%] xs:py-2 bg-[#C3423F] hover:bg-red-700 text-white"}
                                    styling={{roundedFull: true}}
                                    onclick={() => deleteQuestion(i)}
                                />
                            </div>
                        </div>
                    ))}
                    
                    <div className="w-[50%]">
                        <Button
                            text={"Add new question"}
                            icon={<PlusCircleIcon className="inline-block w-5" />}
                            className="bg-green-500 hover:bg-green-300 text-white w-full"
                            onclick={addBlankQuestion}
                        />
                    </div>
                </div>

                <div className="fixed bottom-0 right-0 mb-6 mr-6" ref={menuRef}>
                    <Menu
                        hidden={!saveMenuActive}
                        menuOptions={getMenuOptions()}
                        isAnimated={true}
                        renderButton={false}
                        menuCloseCallback={() => setSaveMenuActive(false)}
                        horizontalAlignment={{alignStart: true}}
                        verticalAlignment={{alignCenter: true}}
                    />

                    <Button
                        className="rounded-full bg-[#F18F01] hover:bg-orange-700 text-white w-16 h-16"
                        styling={{roundedFull: true, paddedSmall: true}}
                        text=""
                        toolTip="Save changes"
                        onclick={
                            (ev: any) => {
                                ev.stopPropagation();
                                setSaveMenuActive(!saveMenuActive);
                            }
                        }
                        icon={<MenuAlt2Icon />}
                    />
                </div>
                </>
            ) : (intervalInfo !== undefined) ? (
                <div className="text-lg font-serif">
                    You haven't selected any form to edit. You will be redirected to your dashboard in {secondsLeft} seconds.
                    Click <Link to={"/dashboard"} onClick={() => clearInterval(intervalInfo)} className="underline text-blue-500">here</Link> if you
                     are not redirected.
                </div>
            ) : (
                <LoadingCircle className="w-full flex justify-center" />
            )}

            {(infoCardShown) ? (
                <InfoCard
                    text={infoCardText}
                    className={infoCardClassName}
                />
            ):""}
        </div>
    );
}

export default Editor;