import Searchbar from '../../components/universal/form/Searchbar';

function Homepage() {
    return (
        <div className="flex flex-col items-center bg-teal-200 pt-[24vh] pb-[36vh] mt-4">
            <h1 className="text-center font-bold text-4xl pb-10 justify-self-start">Find form by link</h1>
            <div className="flex flex-col self-center w-10/12">
            <Searchbar
                maxLength={96}
                placeholder="Enter form link here"
            />
            <hr className="border-black my-10" />
            <h1 className="text-center font-bold text-4xl pb-10 justify-self-start">Open form statistics by link</h1>
            <Searchbar
                maxLength={96}
                placeholder="Enter form statistics link here"
                isStatisticsFetch={true}
            />
            </div>
        </div>
    );
}

export default Homepage;
