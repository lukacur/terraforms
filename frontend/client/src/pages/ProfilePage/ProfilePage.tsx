import classNames from "classnames";
import React, { ChangeEvent } from "react";
import InfoCard from "../../components/universal/base/InfoCard";
import Button from "../../components/universal/form/Button";
import DATA_SERVER from "../../util/data/DataFetch";
import { UserInfoDTO, UserInfoUpdateDTO } from "../../util/interfaces/dtos";

function ProfilePage() {
    const emailRef = React.useRef<HTMLInputElement>(null);
    const [emailInvalid, setEmailInvalid] = React.useState<boolean>(false);

    const passwordRef = React.useRef<HTMLInputElement>(null);
    const [passwordInvalid, setPasswordInvalid] = React.useState<boolean>(false);

    const repeatRef = React.useRef<HTMLInputElement>(null);
    const [repeatInvalid, setRepeatInvalid] = React.useState<boolean>(false);

    const [profileInfo, setProfileInfo] = React.useState<UserInfoDTO>();
    const [updatedInfo, setUpdatedInfo] = React.useState<UserInfoUpdateDTO>({ email: "", password: "", repeatedPassword: ""});

    const [emailDisabled, setEmailDisabled] = React.useState<boolean>(true);
    const [pwdDisabled, setPwdDisabled] = React.useState<boolean>(true);

    const [infoCardText, setInfoCardText] = React.useState<string>("");
    const [infoCardClassName, setInfoCardClassName] = React.useState<string>("");
    const [infoCardShown, setInfoCardShown] = React.useState<boolean>(false);

    function updateInfo(event: ChangeEvent<HTMLInputElement>) {
        const { name, value } = event.target;

        setUpdatedInfo({ ...updatedInfo, [name]: value });
    }

    const infoCardClasses: string = classNames(
        "flex flex-col items-center justify-center",
        "rounded-xl",
        "py-10",
        "bg-white shadow-2xl shadow-black text-2xl"
    );

    function saveChanges() {
        let newInfo = { ...updatedInfo };

        let invalid: boolean = false;

        invalid ||= passwordInvalid;
        invalid ||= repeatInvalid;
        invalid ||= emailInvalid;
        
        if (emailDisabled) {
            newInfo.email = undefined;
        }

        if (pwdDisabled) {
            newInfo.password = undefined;
            newInfo.repeatedPassword = undefined;
        }

        if (!invalid && (!emailDisabled || !pwdDisabled)) {
            DATA_SERVER.updateUserInfo(newInfo)
            .then(() => {
                setInfoCardClassName("bg-green-400 border-2 border-green-500");
                setInfoCardText("Information successfully updated!");

                if (!infoCardShown) {
                    setInfoCardShown(true);
                    setTimeout(() => setInfoCardShown(false), 4000);
                }

                setUpdatedInfo({ ...updatedInfo, password: "", repeatedPassword: "" })
                setEmailDisabled(true);
                setPwdDisabled(true);
            });
        }
    }

    function discardChanges() {
        setUpdatedInfo({ email: profileInfo?.email || (() => { throw new Error("This should never happen.") })(), password: "", repeatedPassword: "" });
        setInfoCardClassName("bg-red-400 border-2 border-red-500");
        setInfoCardText("Changes discarded");

        if (!infoCardShown) {
            setInfoCardShown(true);
            setTimeout(() => setInfoCardShown(false), 4000);
        }

        setEmailDisabled(true);
        setPwdDisabled(true);
    }

    function runCheck(event: React.FocusEvent<HTMLInputElement>) {
        if (event.target !== repeatRef.current) {
            event.target.required = true;
        }

        if (event.target === passwordRef.current) {
            setPasswordInvalid(!pwdDisabled && ((updatedInfo.password) ? updatedInfo.password.length < 8 : false));

            if (!pwdDisabled && (updatedInfo.password !== undefined)) {
                if (updatedInfo.password.length < 8) {
                    passwordRef.current.setCustomValidity("Password must be at least 8 characters long");
                } else if (updatedInfo.password !== updatedInfo.repeatedPassword) {
                    repeatRef.current?.setCustomValidity("Passwords don't match");
                    passwordRef.current.setCustomValidity("Passwords don't match");
                    setRepeatInvalid(true);
                } else {
                    setRepeatInvalid(false);
                    repeatRef.current?.setCustomValidity("");
                    passwordRef.current.setCustomValidity("");
                }
            } else {
                setRepeatInvalid(false);
                setPasswordInvalid(false);
                repeatRef.current?.setCustomValidity("");
                passwordRef.current?.setCustomValidity("");
            }
        }

        if (event.target === repeatRef.current) {
            setRepeatInvalid(!pwdDisabled && updatedInfo.password !== updatedInfo.repeatedPassword);
            
            if (!pwdDisabled && updatedInfo.password !== updatedInfo.repeatedPassword) {
                repeatRef.current.setCustomValidity("Passwords don't match");
                passwordRef.current?.setCustomValidity("Passwords don't match");
            } else {
                setRepeatInvalid(false);
                setPasswordInvalid(false);
                repeatRef.current.setCustomValidity("");
                passwordRef.current?.setCustomValidity("");
            }
        }

        if (event.target === emailRef.current) {
            emailRef.current.setCustomValidity("");
            setEmailInvalid(!emailDisabled && !emailRef.current.validity.valid);

            if (!emailDisabled && !emailRef.current.validity.valid) {
                emailRef.current.setCustomValidity("Email not in valid format");
            } else if (emailDisabled) {
                setEmailInvalid(false);
                emailRef.current.setCustomValidity("");
            }
        }
    }

    React.useEffect(() => {
        DATA_SERVER.fetchUserInfo()
        .then(userInfo => {
            setUpdatedInfo({ email: userInfo.email, password: "", repeatedPassword: "" });
            setProfileInfo(userInfo);
        });
    }, []);

    const inputClasses: string = classNames(
        "rounded-lg w-full outline-none border-2 border-solid focus:border-[#91C4F2] border-[#006BA6] p-2",
        "disabled:border-gray-300 disabled:bg-gray-200 text-lg md:text-2xl",
        "invalid:border-red-600 invalid:focus:border-red-600  invalid:focus:shadow-md invalid:focus:shadow-red-300"
    );

    const inputContainerClasses: string = classNames(
        "relative w-[90%] px-2 mb-10"
    );

    return (
        <>
            <div className="relative left-[10%] w-4/5 my-8">
                <div className={infoCardClasses}>
                    <h1 className="text-4xl font-bold">{profileInfo?.username}</h1>

                    <hr className="border-gray-500 w-[85%] my-10" />

                    <div className={inputContainerClasses}>
                        <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">E-mail</label>
                        <input
                            ref={emailRef}
                            className={inputClasses}
                            type={"email"}
                            name={"email"}
                            value={updatedInfo.email}
                            placeholder="E-mail"
                            onChange={updateInfo}
                            onBlur={runCheck}
                            disabled={emailDisabled}
                        />
                        {(!emailDisabled && emailInvalid) ? <span className="text-red-600 text-sm">{emailRef.current?.validationMessage}</span>:""}

                        {(profileInfo?.isLocalUser) ? (
                            <input
                                className={"absolute top-4 right-4 md:-right-4 h-4 w-4 rounded-3xl ml-2"}
                                type={"checkbox"}
                                checked={!emailDisabled}
                                onChange={() => setEmailDisabled(!emailDisabled)}
                                title={"Check me to update your email"}
                            />
                        ):""}
                    </div>

                    <div className={inputContainerClasses}>
                        <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">Password</label>
                        <input
                            ref={passwordRef}
                            className={inputClasses}
                            type={"password"}
                            name={"password"}
                            value={updatedInfo.password}
                            placeholder="Password"
                            onChange={updateInfo}
                            onBlur={runCheck}
                            disabled={pwdDisabled}
                        />
                        {(!pwdDisabled && (passwordInvalid || repeatInvalid)) ? <span className="text-red-600 text-sm">{passwordRef.current?.validationMessage}</span>:""}

                        {(profileInfo?.isLocalUser) ? (
                            <input
                                className={"absolute top-4 right-4 md:-right-4 h-4 w-4 rounded-3xl ml-2"}
                                type={"checkbox"}
                                checked={!pwdDisabled}
                                onChange={() => setPwdDisabled(!pwdDisabled)}
                                title={"Check me to update your password"}
                            />
                        ):""}
                    </div>

                    <div className={inputContainerClasses}>
                        <label className="absolute text-center text-sm bg-white text-gray-600 px-3 -top-2.5 left-4 rounded-full">Repeated password</label>
                        <input
                            ref={repeatRef}
                            className={inputClasses}
                            type={"password"}
                            name={"repeatedPassword"}
                            value={updatedInfo.repeatedPassword}
                            placeholder="Repeated password"
                            onChange={updateInfo}
                            onBlur={runCheck}
                            disabled={pwdDisabled}
                        />
                        {(!pwdDisabled && (passwordInvalid || repeatInvalid)) ? <span className="text-red-600 text-sm">{repeatRef.current?.validationMessage}</span>:""}
                    </div>

                    <div className="flex flex-col md:flex-row items-center justify-around w-[90%]">
                        {(profileInfo?.isLocalUser) ? (
                            <>
                                <Button
                                    styling={{ paddedSmall: true, roundedPartial: true }}
                                    className={"basis-2/6 bg-green-500 hover:bg-green-400 text-white mb-2 md:mb-0"}
                                    text="Save changes"
                                    onclick={saveChanges}
                                />

                                <Button
                                    styling={{ paddedSmall: true, roundedPartial: true }}
                                    className={"basis-2/6 bg-red-500 hover:bg-red-400 text-white"}
                                    text="Cancel"
                                    onclick={discardChanges}
                                />
                            </>
                        ):"You can't change your information since you have logged in with an outside authorization provider."}
                    </div>
                </div>
            </div>
            {(infoCardShown) ? (
                    <InfoCard
                        text={infoCardText}
                        className={infoCardClassName}
                    />
                ):""}
        </>
    );
}

export default ProfilePage;