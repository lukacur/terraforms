import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/outline";
import classNames from "classnames";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import AnswerCard from "../../components/answerer/AnswerCard";
import InfoCard from "../../components/universal/base/InfoCard";
import Button from "../../components/universal/form/Button";
import TextModal from "../../components/universal/modals/TextModal";
import DATA_SERVER from "../../util/data/DataFetch";
import { AnswerDTO, QuestionAnswerDTO, QuestionDTO } from "../../util/interfaces/dtos";
import { FormFullLocal } from "../../util/interfaces/StoreData";
import STORE from "../../util/store/Store";

function AnsweringPage() {
    const navigate = useNavigate();

    const [formInfo, setFormInfo] = React.useState<FormFullLocal>();
    const [currentPage, setCurrentPage] = React.useState(1);
    const [questionToAnswerMap, setQuestionToAnswerMap] =
            React.useState(new Map<QuestionDTO, string>());
    const [questionToPageMap, setQuestionToPageMap] = React.useState<Map<number, QuestionDTO[]>>(new Map());
    const [maxPages, setMaxPages] = React.useState(0);
    const [visible, setVisible] = React.useState(false);

    const [infoCardShown, setInfoCardShown] = React.useState<boolean>(false);
    const [infoCardText, setInfoCardText] = React.useState<string>("");
    const [infoCardClassName, setInfoCardClassName] = React.useState<string>("");

    const [sentModal, setSentModal] = React.useState<boolean>(false);
    const [wasSuccess, setWasSuccess] = React.useState<boolean>(false);
    const [sentModalTitle, setSentModalTitle] = React.useState<string>("");
    const [sentModalText, setSentModalText] = React.useState<string>("");

    function sendResponse(): void {
        let invalid: boolean = false;
        let response: AnswerDTO = {
            formId: formInfo?.form.formData.formId || (() => { throw new Error("This should never happen") })(),
            answers: []
        };

        let answeredQuestions = Array.from(questionToAnswerMap).filter(val => val[1] !== "").map(val => val[0].questionId);
        let requiredQuestions = Array.from(formInfo?.form.bodyData.questions).filter(q => q.isRequired).map(q => q.questionId);

        invalid = requiredQuestions.some(rqId => !answeredQuestions.includes(rqId));

        questionToAnswerMap.forEach((answer: string, questionDto: QuestionDTO) => {
            response.answers.push(
                {
                    questionId: questionDto.questionId || (() => { throw new Error("This should never happen") })(),
                    answer
                }
            );
        });

        if (!invalid) {
           DATA_SERVER.sendResponse(response)
            .then(() => {
                setWasSuccess(true);
                setSentModalTitle("Success");
                setSentModalText("Your answers have been successfully recorded. Thank you for participating.");
                setSentModal(true);
            })
            .catch(error => {
                setWasSuccess(false);
                console.log(error);
                setSentModalTitle("Failure");
                setSentModalText("Your answers weren't able to be saved. Check your answers and try again later.");
                setSentModal(true);
            });
        } else {
            setWasSuccess(false);
            setSentModalTitle("Not sent - invalid answers");
            setSentModalText("Check the answers that you have given and make sure that you answered all the required questions.");
            setSentModal(true);
        }
    }

    const [intervalInfo, setIntervalInfo] = React.useState<NodeJS.Timer>();
    const [secondsLeft, setSecondsLeft] = React.useState<number>(2);

    React.useEffect(() => {
        STORE.getCurrAnswerForm()
        .then(form => {
            if (form && !formInfo) {
                let tmpQToPMap: Map<number, QuestionDTO[]> = new Map<number, QuestionDTO[]>();
                form.form.bodyData.questions.sort((q1, q2) => q1.questionPage - q2.questionPage);
                let qnum: number = 1;
                form.form.bodyData.questions.forEach(q => q.questionNumber = qnum++);

                setFormInfo(form);

                let maxPages: number = -1;
                for (let question of form.form.bodyData.questions) {
                    let questionPageNum: number = question.questionPage;
                    let pageQuestions: QuestionDTO[] | undefined = tmpQToPMap.get(questionPageNum);

                    if (questionPageNum > maxPages) {
                        maxPages = questionPageNum;
                    }

                    if (question.possibleAnswers.length !== 0) {
                        question.possibleAnswers.sort(
                            (a1, a2) => {
                                if (a1.value > a2.value) {
                                    return 1;
                                } else if (a1.value === a2.value) {
                                    return 0;
                                } else {
                                    return -1;
                                }
                            }
                        );
                    }
            
                    if (pageQuestions) {
                        pageQuestions.push(question);
                    } else {
                        pageQuestions = [question];
                    }
            
                    tmpQToPMap.set(questionPageNum, pageQuestions);
                }

                setMaxPages(maxPages);
                setQuestionToPageMap(tmpQToPMap);

                STORE.getTempAnswersFor(form)
                .then(answers => {
                    let tmpQToAMap: Map<QuestionDTO, string> = new Map();

                    if (answers && answers.length !== 0) {
                        tmpQToPMap.forEach((questionsList, key) => {
                            for (let question of questionsList) {
                                let filtered = answers.filter(answ => answ.questionId === question.questionId);

                                let answer: QuestionAnswerDTO | undefined = undefined;

                                if (filtered.length !== 0) {
                                    answer = filtered[0];
                                }

                                if (answer) {
                                    tmpQToAMap.set(question, answer.value);
                                }
                            }
                        });

                        setQuestionToAnswerMap(tmpQToAMap);
                    }
                })
                .then(() => setVisible(true));
            } else if (intervalInfo === undefined && !formInfo && !form) {
                let tmp = 2;
                let ii = setInterval(() => {
                    if (tmp > 0) {
                        setSecondsLeft(--tmp);
                        return;
                    }

                    navigate("/");
                }, 1000);

                setTimeout(() => clearInterval(ii), 3000);

                setIntervalInfo(ii);
            }
        });

        let keyListener = (event: KeyboardEvent) => {
            if (event.key == "ArrowLeft" && currentPage > 1) {
                setCurrentPage(currentPage - 1);
            } else if (event.key == "ArrowRight" && currentPage < maxPages) {
                setCurrentPage(currentPage + 1);
            }
        };

        let startedX: number | null = null;
        let endedX: number | null = null;
        let startedY: number | null = null;
        let endedY: number | null = null;
        let lastTouch: TouchEvent;

        let touchStartLst = (event: TouchEvent) => {
            startedX = event.touches[0].clientX;
            startedY = event.touches[0].clientY;
        };

        let touchMoveLst = (event: TouchEvent) => {
            lastTouch = event;
        };

        let touchEndLst = (event: TouchEvent) => {
            endedX = lastTouch?.touches[0].clientX || null;
            endedY = lastTouch?.touches[0].clientY || null;

            if (startedY !== null && endedY !== null && (Math.abs(endedY - startedY) < 10)) {
                if (startedX !== null && endedX !== null) {
                    if (startedX < endedX) {
                        if (currentPage > 1) {
                            setCurrentPage(currentPage - 1);
                        }
                    } else if (startedX > endedX) {
                        if (currentPage < maxPages) {
                            setCurrentPage(currentPage + 1);
                        }
                    }
                }
            }
        };

        document.addEventListener("keydown", keyListener);
        document.addEventListener("touchstart", touchStartLst);
        document.addEventListener("touchmove", touchMoveLst);
        document.addEventListener("touchend", touchEndLst);

        return () => {
            document.removeEventListener("keydown", keyListener);
            document.removeEventListener("touchstart", touchStartLst);
            document.removeEventListener("touchmove", touchMoveLst);
            document.removeEventListener("touchend", touchEndLst);
        };
    }, [currentPage]);

    return (
        <>
            {(visible) ? (
                <div>
                    <div className="bg-gray-300 px-8 pt-4 pb-8">
                        <h1 className="font-bold text-3xl md:text-4xl text-center mb-5">{formInfo?.form.formData.title}</h1>
                        <hr className="border-[2px] border-gray-400/25"/>
                        <h2 className="text-lg md:text-2xl text-justify">{formInfo?.form.formData.description}</h2>
                        <h2 className="text-sm text-justify mt-2">(Tip: you can navigate the form pages with arrow keys or by swiping left or right)</h2>
                    </div>

                    <div className="flex flex-col items-center">
                        {questionToPageMap.get(currentPage)
                            ?.map((question: QuestionDTO, index: number) => {
                                return (
                                    <AnswerCard
                                        questionInfo={question}
                                        valueUpdateCallback={
                                            (newValue: string) => {
                                                let tmpMap = questionToAnswerMap;

                                                tmpMap.set(question, newValue)
                                                
                                                setQuestionToAnswerMap(tmpMap);
                                            }
                                        }
                                        answered={questionToAnswerMap.get(question)}
                                        key={`${question.questionId}__${index}`}
                                    />
                                );
                            })
                        }

                        <div className="flex flex-row w-full justify-around mb-5">
                            <Button
                                className="w-6"
                                styling={{}}
                                text=""
                                toolTip="Previous page"
                                icon={<ChevronLeftIcon className="w-full" />}
                                enabled={currentPage > 1}
                                onclick={
                                    () => {
                                        if (currentPage > 1) {
                                            setCurrentPage(currentPage - 1);
                                        }
                                    }
                                }
                            />
                            <div>{currentPage}</div>
                            <Button
                                className="w-6"
                                styling={{}}
                                text=""
                                toolTip="Next page"
                                icon={<ChevronRightIcon className="w-full" />}
                                enabled={currentPage < maxPages}
                                onclick={
                                    () => {
                                        if (currentPage < maxPages) {
                                            setCurrentPage(currentPage + 1);
                                        }
                                    }
                                }
                            />
                        </div>

                        <div className={classNames("flex flex-row justify-around w-full bg-white sticky bottom-0 py-4", {"hidden": currentPage < maxPages})}>
                            <Button
                                text="Send response"
                                className="text-lg text-white bg-[#34D399] hover:bg-[#44E3A9]"
                                styling={{paddedMedium: true, roundedPartial: true}}
                                onclick={sendResponse}
                            />

                            <Button
                                text="Save progress locally"
                                className="text-lg text-white bg-[#D16F01] hover:bg-[#F18F01]"
                                styling={{paddedMedium: true, roundedPartial: true}}
                                onclick={
                                    () => {
                                        if (formInfo) {
                                            let answers: QuestionAnswerDTO[] = [];

                                            questionToAnswerMap.forEach((value, key) => {
                                                answers.push(
                                                    {
                                                        questionId: key.questionId || (() => { throw new Error("This should not happen"); })(),
                                                        value
                                                    }
                                                );
                                            });

                                            STORE.setTempAnswersFor(formInfo, answers)
                                            .then(() => {
                                                setInfoCardText("Progress saved!");
                                                setInfoCardClassName("bg-green-600");
                                                if (!infoCardShown) {
                                                    setInfoCardShown(true);
                                                    setTimeout(() => setInfoCardShown(false), 4000);
                                                }
                                            });
                                        }
                                    }
                                }
                            />
                        </div>
                    </div>
                </div>
            ):(intervalInfo !== undefined) ? (
                <div className="text-lg font-serif">
                    You haven't selected any form to give answers on. You will be redirected to the homepage in {secondsLeft} seconds.
                    Click <Link to={"/"} onClick={() => clearInterval(intervalInfo)} className="underline text-blue-500">here</Link> if you
                     are not redirected.
                </div>
            ):""}

            {(sentModal) ? (
                <TextModal
                    modalTitle={sentModalTitle}
                    modalText={sentModalText}
                    modalCloseFunction={
                        () => {
                            setSentModal(false);
                            if (wasSuccess) {
                                STORE.setCurrAnswerForm(null);

                                if (formInfo) {
                                    STORE.setTempAnswersFor(formInfo, []);
                                }
                                
                                navigate("/");
                            }
                        }
                    }
                    modalOptions={
                        [
                            <Button
                                text="OK"
                                onclick={
                                    () => {
                                        setSentModal(false);
                                        if (wasSuccess) {
                                            STORE.setCurrAnswerForm(null);
                    
                                            if (formInfo) {
                                                STORE.setTempAnswersFor(formInfo, []);
                                            }
                    
                                            navigate("/");
                                        }
                                    }
                                }
                            />
                        ]
                    }
                />
            ):""}

            {(infoCardShown) ? (
                <InfoCard
                    text={infoCardText}
                    className={infoCardClassName}
                />
            ):""}
        </>
    );
}

export default AnsweringPage;