import { ArrowUpIcon } from "@heroicons/react/solid";
import classNames from "classnames";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import CSVIcon from "../../components/icons/CSVIcon";
import PDFIcon from "../../components/icons/PDFIcon";
import TXTIcon from "../../components/icons/TXTIcon";
import StatisticsCard from "../../components/statistics/StatisticsCard";
import LoadingCircle from "../../components/universal/base/LoadingCircle";
import Button from "../../components/universal/form/Button";
import DATA_SERVER from "../../util/data/DataFetch";
import { FormStatisticsDTO } from "../../util/interfaces/dtos";
import STORE from "../../util/store/Store";

function StatisticsPage() {
    const navigate = useNavigate();
    const scrollToTopDiv = React.useRef<HTMLDivElement>(null);
    const documentDivRef = React.useRef<HTMLDivElement>(null);

    const [loading, setLoading] = React.useState<boolean>(true);
    const [statisticsInfo, setStatisticsInfo] = React.useState<FormStatisticsDTO>();

    const [intervalInfo, setIntervalInfo] = React.useState<NodeJS.Timer>();
    const [secondsLeft, setSecondsLeft] = React.useState<number>(2);

    function downloadFile() {
        document.body.classList.add("cursor-wait");
    }

    React.useEffect(() => {
        STORE.getCurrStatistics()
        .then(stats => {
            if (stats) {
                setStatisticsInfo(stats);
                setLoading(false);
            } else if (intervalInfo === undefined) {
                let tmp = 2;
                let ii = setInterval(() => {
                    if (tmp > 0) {
                        setSecondsLeft(--tmp);
                        return;
                    }

                    navigate("/dashboard");
                }, 1000);

                setTimeout(() => clearInterval(ii), 3000);

                setIntervalInfo(ii);
            }

            let scrollListener = () => {
                if (scrollToTopDiv.current) {
                    let docHeight: number = document.documentElement.scrollHeight - document.documentElement.clientHeight;
                    let scrollPercent: number = Math.ceil((window.scrollY / docHeight) * 100);
                    
                    if (scrollPercent < 10)
                        scrollToTopDiv.current.hidden = true;
                    else
                        scrollToTopDiv.current.hidden = false;
                }
            };

            window.addEventListener("scroll", scrollListener);
            return () => {
                STORE.setCurrStatistics(null);
                window.removeEventListener("scroll", scrollListener);
            };
        });
    }, []);

    const statisticsDocumentsClasses: string = classNames(
        "fixed bottom-0 py-4 bg-white w-full",
        {
            "hover:animate-dropdown-extend animate-dropdown-retract": matchMedia("(any-pointer: fine)").matches
        }
    );

    return (
        <div>
            {(!loading) ? (
                <div>
                    <div className="bg-gray-300 px-8 pt-4 pb-8 mb-10 w-full">
                        <h1 className="font-bold text-4xl text-center w-full">{statisticsInfo?.formTitle}</h1>
                        <hr className="border-[2px] border-gray-400/25 my-5 w-full"/>
                        <div className="flex items-center justify-center w-full">
                            <h2 className="text-2xl text-justify">{statisticsInfo?.formDescription}</h2>
                        </div>
                    </div>

                    {statisticsInfo?.statistics.map(
                        (entry, index) => (
                            <div className="mb-10 flex flex-col items-center justify-center">
                                <StatisticsCard
                                    questionInfo={entry}
                                    questionNumber={index + 1}
                                />
                                <hr className="bg-black/50 my-5 w-[95%]" />
                            </div>
                        )
                    )}

                    <div className={statisticsDocumentsClasses} ref={documentDivRef}>
                        <div className="flex flex-row items-center justify-center col-start-2 col-end-3">
                            <Button
                                styling={{ roundedPartial: true }}
                                text={""}
                                icon={<PDFIcon width={64} height={64} />}
                                toolTip={"Export to PDF"}
                                className={"mr-8"}
                                onclick={
                                    () => {
                                        if (statisticsInfo) {
                                            downloadFile();
                                            DATA_SERVER.downloadStatisticsPDF(statisticsInfo)
                                            .then(() => document.body.classList.remove("cursor-wait"));
                                        }
                                    }
                                }
                            />

                            <Button
                                styling={{ roundedPartial: true }}
                                text={""}
                                icon={<CSVIcon width={64} height={64} />}
                                toolTip={"Export to CSV"}
                                className={"mr-8"}
                                onclick={
                                    () => {
                                        if (statisticsInfo) {
                                            downloadFile();
                                            DATA_SERVER.downloadStatisticsCSV(statisticsInfo)
                                            .then(() => document.body.classList.remove("cursor-wait"));
                                        }
                                    }
                                }
                            />

                            <Button
                                styling={{ roundedPartial: true }}
                                text={""}
                                icon={<TXTIcon width={64} height={64} />}
                                toolTip={"Export to TXT"}
                                className={" "}
                                onclick={
                                    () => {
                                        if (statisticsInfo) {
                                            downloadFile();
                                            DATA_SERVER.downloadStatisticsTXT(statisticsInfo)
                                            .then(() => document.body.classList.remove("cursor-wait"));
                                        }
                                    }
                                }
                            />
                        </div>
                    </div>

                    <div className="fixed bottom-[15vh] md:bottom-0 right-0 mb-4 mr-4" ref={scrollToTopDiv}>
                        <Button
                            styling={{ paddedSmall: true, roundedFull: true }}
                            className={"h-full"}
                            icon={<ArrowUpIcon className="w-6" />}
                            text=""
                            toolTip={"Scroll to top"}
                            onclick={() => window.scrollTo({ top: 0, behavior: "smooth"})}
                        />
                    </div>
                </div>
            ) : (intervalInfo !== undefined) ? (
                <div className="text-lg font-serif">
                    You haven't selected any form to view statistics on. You will be redirected to your dashboard in {secondsLeft} seconds.
                    Click <Link to={"/dashboard"} onClick={() => clearInterval(intervalInfo)} className="underline text-blue-500">here</Link> if you
                     are not redirected.
                </div>
            ) : (
                <LoadingCircle className="w-full h-2/5" />
            )}
        </div>
    );
}

export default StatisticsPage;