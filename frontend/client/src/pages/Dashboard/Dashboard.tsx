import { TrashIcon, XCircleIcon } from "@heroicons/react/outline";
import { ExclamationIcon } from "@heroicons/react/solid";
import { PlusIcon } from "@heroicons/react/solid";
import React from "react";
import { useNavigate } from "react-router-dom";
import CardTable from "../../components/dashboard/CardTable";
import FormAccessRequest from "../../components/dashboard/FormAccessRequest";
import FormInfoEditor from "../../components/dashboard/FormInfoEditor";
import PermissionManager from "../../components/dashboard/PermissionManager";
import { FormMenuCallbacks } from "../../components/dashboard/types/DashboardCommonTypes";
import LoadingCircle from "../../components/universal/base/LoadingCircle";
import ElementDropdown from "../../components/universal/dropdown/ElementDropdown";
import Menu from "../../components/universal/dropdown/Menu";
import Button from "../../components/universal/form/Button";
import ComponentModal from "../../components/universal/modals/ComponentModal";
import TextModal from "../../components/universal/modals/TextModal";
import DATA_SERVER from "../../util/data/DataFetch";
import { FormFullLocal } from "../../util/interfaces/StoreData";
import STORE from "../../util/store/Store";

function Dashboard() {
    const navigate = useNavigate();
    const createMenuRef = React.useRef<HTMLDivElement>(null);

    const [currentlyEditingForms, setCurrentlyEditingForms] = React.useState<FormFullLocal[]>([]);
    const [ownedForms, setOwnedForms] = React.useState<FormFullLocal[]>([]);
    const [sharedWithUserForms, setSharedWithUserForms] = React.useState<FormFullLocal[]>([]);

    const [editingLoaded, setEditingLoaded] = React.useState<boolean>(false);
    const [ownedLoaded, setOwnedLoaded] = React.useState<boolean>(false);
    const [sharedLoaded, setSharedLoaded] = React.useState<boolean>(false);

    const [softReloadState, softReload] = React.useState<boolean>(false);

    const [creationMenuActive, setCreationMenuActive] = React.useState<boolean>(false);
    const [accessRequestActive, setAccessRequestActive] = React.useState<boolean>(false);
    
    const [permissionManagerActive, setPermissionManagerActive] = React.useState<boolean>(false);
    const [createFormActive, setCreateFormActive] = React.useState<boolean>(false);
    const [deleteConfirmModal, setDeleteConfirmModal] = React.useState<boolean>(false);
    const [isTemporary, setIsTemporary] = React.useState<boolean>(false);
    const [isOwner, setIsOwner] = React.useState<boolean>(false);

    const [cloneInfo, setCloneInfo] = React.useState<FormFullLocal | null>(null);
    const [editingInfo, setEditingInfo] = React.useState<FormFullLocal | null>(null);
    const [formToDelete, setFormToDelete] = React.useState<FormFullLocal | null>(null);
    const [formPermissionManaging, setFormPermissionManaging] = React.useState<FormFullLocal | null>(null);

    const [creatingForm, setCreatingForm] =
    React.useState<FormFullLocal>(
        {
            localFormId: "lo_1",
            form: {
                formData: {
                    description: "",
                    formId: -1,
                    restrictedTo: -1,
                    title: "",
                    formLink: null,
                    statsLink: null
                },
                bodyData: {
                    parentFormId: -1,
                    questions: []
                }
            }
        }
    );

    function getDeleteModalTitle(): string {
        if (isTemporary) {
            return "Discard changes";
        } else if (isOwner) {
            return "Delete form";
        } else {
            return "Stop participating";
        }
    }

    function getDeleteModalText(): string {
        if (isTemporary) {
            return "Are you sure you want to discard all changes made to  the selected form? This can't be undone!";
        } else if (isOwner) {
            return "Are you sure you want to delete the selected form? This can't be undone!";
        } else {
            return "Are you sure you want to stop participating on this form? If you do this, you will have to request access again to continue participating.";
        }
    }

    function getFormCardMenuCallbacks(isTemp: boolean, isOwner: boolean): FormMenuCallbacks {
        let formCardMenuCallbacks: FormMenuCallbacks = {
            formSettingsCallback(formInfo) {
                setIsTemporary(isTemp);
                setIsOwner(isOwner);
                STORE.getFromEditingForms(formInfo.localFormId)
                .then(form => {
                    if (form) {
                        setEditingInfo(form);
                    } else {
                        setEditingInfo(formInfo);
                    }
    
                    setCreateFormActive(true);
                });
            },
            managePermissionsCallback(formInfo) {
                setIsTemporary(isTemp);
                setIsOwner(isOwner);
                setFormPermissionManaging(formInfo);
                setPermissionManagerActive(true);
            },
            cloneCallback(formInfo) {
                setIsTemporary(isTemp);
                setIsOwner(isOwner);
                setCloneInfo(formInfo);
                setCreateFormActive(true);
            },
            formDeleteCallback(formInfo) {
                setIsTemporary(isTemp);
                setIsOwner(isOwner);
                setFormToDelete(formInfo);
                setDeleteConfirmModal(true);
            }
        };

        return formCardMenuCallbacks;
    }

    function closeActionPerformed() {
        setCloneInfo(null);
        setEditingInfo(null);
        setCreatingForm({localFormId: "lo_0", form: { formData: {description: "", title: "", formId: -1, restrictedTo: -1, formLink: null, statsLink: null}, bodyData: { parentFormId: -1, questions: []} }});

        setCreateFormActive(false);
    }

    React.useEffect(() => {
        let listener = (ev: any) => {
            if (createMenuRef.current && !createMenuRef.current.contains(ev.target)) {
                setCreationMenuActive(false);
            }
        };

        document.addEventListener("mousedown", listener);
        document.addEventListener("touchstart", listener);
        
        STORE.getEditingForms()
        .then(forms => {
            if (forms) {
                setCurrentlyEditingForms(forms);
            }
            setEditingLoaded(true);
        });

        DATA_SERVER.fetchOwnedForms()
        .then(forms => {
            let localForms: FormFullLocal[] = forms.map(form => {
                return {
                    localFormId: form.formData.formId?.toString() || (() => { throw new Error("This shouldn't happen"); })(),
                    form
                };
            });

            setOwnedForms(localForms);
            setOwnedLoaded(true);
        });
        
        DATA_SERVER.fetchSharedForms()
        .then(forms => {
            let localForms: FormFullLocal[] = forms.map(form => {
                return {
                    localFormId: form.formData.formId?.toString() || (() => { throw new Error("This shouldn't happen"); })(),
                    form
                };
            });

            setSharedWithUserForms(localForms);
            setSharedLoaded(true);
        });

        return () => {
            document.removeEventListener("mousedown", listener);
            document.removeEventListener("touchstart", listener);
        }
    }, [softReloadState]);

    return (
        <>
            <div className="flex flex-col items-center justify-center text-xl">
                <div className="flex flex-col items-center justify-center w-[95%] mb-10">
                    <hr className="border-black/25 w-full mt-2 mb-10" />
                    <h1 className="text-center font-bold text-3xl md:text-4xl">Welcome to the dashboard</h1>
                    <hr className="border-black/25 w-full my-10" />
                    <h2 className="w-[90%] self-start text-lg md:text-xl text-justify">
                        Here you can see all of your owned forms, forms that were shared with you and forms that you are currently working on.<br /><br />
                        The forms that you are currently working on are under the {"'Currently editing'"} section.<br />
                        The forms that you own are under the {"'Owned forms'"} section.<br />
                        The forms that others shared with you are under the {"'Shared with me'"} section.<br />
                    </h2>
                    <hr className="border-black/25 w-full mt-10" />
                </div>

                <ElementDropdown
                    dropdownText="Currently editing"
                    elementToShow={
                        <>
                            <hr className="w-full border-t-gray-400 border-t-2" />
                            {(editingLoaded) ? (
                                <div className="py-2 w-full">
                                    {(currentlyEditingForms.length !== 0) ?
                                        (<CardTable
                                            tempStore={true}
                                            ownsForms={true}
                                            formCardMenuCallbacks={
                                                (() => {
                                                    return getFormCardMenuCallbacks(true, true);
                                                })()
                                            }
                                            forms={currentlyEditingForms}
                                        />)
                                    : (<div className="text-gray-600 text-center">You aren't editing any forms yet. What are you waiting for?</div>)
                                    }
                                </div>
                            ) : (
                                <div className="flex items-center justify-center">
                                    <LoadingCircle />
                                </div>
                            )}
                        </>
                    }
                />

                <ElementDropdown
                    dropdownText="Owned forms"
                    elementToShow={
                        <>
                            <hr className="w-full border-t-gray-400 border-t-2" />
                            {(ownedLoaded) ? (
                                <div className="py-2 w-full">
                                    {(ownedForms.length !== 0) ?
                                        (<CardTable
                                            tempStore={false}
                                            ownsForms={true}
                                            formCardMenuCallbacks={getFormCardMenuCallbacks(false, true)}
                                            forms={ownedForms}
                                        />)
                                    : (<div className="text-gray-600 text-center">You haven't created any forms yet. You are missing on so much fun...</div>)
                                    }
                                </div>
                            ) : (
                                <div className="flex items-center justify-center">
                                    <LoadingCircle />
                                </div>
                            )}
                        </>
                    }
                />

                <ElementDropdown
                    dropdownText="Shared with me"
                    elementToShow={
                        <>
                            <hr className="w-full border-t-gray-400 border-t-2" />
                            {(sharedLoaded) ? (
                                <div className="py-2 w-full">
                                    {(sharedWithUserForms.length !== 0) ?
                                        (<CardTable
                                            tempStore={false}
                                            ownsForms={false}
                                            formCardMenuCallbacks={getFormCardMenuCallbacks(false, false)}
                                            forms={sharedWithUserForms}
                                        />)
                                    : (<div className="text-gray-600 text-center">Noone has shared a form with you yet. So lonely...</div>)
                                    }
                                </div>
                            ) : (
                                <div className="flex items-center justify-center">
                                    <LoadingCircle />
                                </div>
                            )}
                        </>
                    }
                />
            </div>

            <div className="fixed bottom-0 right-0 mb-6 mr-6" ref={createMenuRef}>
                <Menu
                    hidden={!creationMenuActive}
                    menuOptions={
                        [
                            ["Create new form", () => setCreateFormActive(true), "", true],
                            ["Request access...", () => setAccessRequestActive(true)]
                        ]
                    }
                    isAnimated={true}
                    renderButton={false}
                    menuCloseCallback={() => setCreationMenuActive(false)}
                    horizontalAlignment={{alignStart: true}}
                    verticalAlignment={{alignCenter: true}}
                />

                <Button
                    className={"rounded-full bg-[#F18F01] hover:bg-orange-700 text-white w-16 h-16"}
                    styling={{roundedFull: true, paddedSmall: true}}
                    text=""
                    toolTip="Add/create"
                    onclick={
                        (ev: any) => {
                            ev.stopPropagation();
                            setCreationMenuActive(!creationMenuActive);
                        }
                    }
                    icon={<PlusIcon />}
                />
            </div>

            {(createFormActive) ? (
                <ComponentModal
                    modalCloseFunction={
                        () => {
                            setCloneInfo(null);
                            setEditingInfo(null);
                            setCreateFormActive(false);
                        }
                    }
                    modalContent={
                        <FormInfoEditor
                            form={
                                ((cloneInfo) ?
                                    cloneInfo : (
                                        (editingInfo) ? editingInfo : creatingForm
                                    )
                                )
                            }
                            editorType={
                                {
                                    clone: cloneInfo !== null && editingInfo === null,
                                    create: cloneInfo === null && editingInfo === null,
                                    edit: cloneInfo === null && editingInfo !== null
                                }
                            }
                            confirmActionCallback={
                                (finalFormData: FormFullLocal) => {
                                    if (cloneInfo === null && editingInfo === null) {
                                        DATA_SERVER.createNewForm(finalFormData.form.formData)
                                        .then(respDto => {
                                            setOwnedForms([
                                                ...ownedForms,
                                                {
                                                    localFormId: respDto.formId?.toString() || (() => { throw new Error("This shouldn't happen"); })(),
                                                    form: {
                                                        formData: respDto,
                                                        bodyData: {
                                                            parentFormId: respDto.formId || (() => { throw new Error("This shouldn't happen"); })(),
                                                            questions: []
                                                        }
                                                    }
                                                }
                                            ]);
                                        });
                                    } else if (cloneInfo !== null) {
                                        STORE.cloneForm(finalFormData.form, true)
                                            .then(() => navigate("/editor"))
                                            .catch((error) => console.log(error));
                                    } else if (editingInfo !== null) {
                                        if (!finalFormData.localFormId.startsWith("lo_") && !isTemporary) {
                                            DATA_SERVER.updateFormMeta(finalFormData.form.formData)
                                            .then(() => {
                                                STORE.setEditingForm(finalFormData)
                                                .then(() => {
                                                    softReload(!softReloadState);
                                                });
                                            });
                                        } else {
                                            STORE.setEditingForm(finalFormData)
                                            .then(() => {
                                                softReload(!softReloadState);
                                            });
                                        }
                                    }

                                    closeActionPerformed();
                                }
                            }
                            cancelActionCallback={closeActionPerformed}
                        />
                    }
                />
            ):""}

            {(accessRequestActive) ? (
                <ComponentModal
                    modalCloseFunction={() => setAccessRequestActive(false)}
                    modalContent={
                        <FormAccessRequest
                            modalCloseFunction={() => setAccessRequestActive(false)}
                        />
                    }
                />
            ):""}

            {(permissionManagerActive) ? (
                <ComponentModal
                    modalCloseFunction={() => setPermissionManagerActive(false)}
                    modalContent={
                        <PermissionManager
                            formInfo={formPermissionManaging??currentlyEditingForms[0]}
                        />
                    }
                />
            ):""}

            {(deleteConfirmModal) ? (
                <TextModal
                    modalCloseFunction={() => setDeleteConfirmModal(false)}
                    modalOptions={
                        [
                            <Button
                                className={"text-white w-[75%] bg-blue-700 hover:bg-red-600 text-xl outline-none"}
                                styling={{paddedMedium: true, roundedPartial: true}}
                                icon={((isOwner || isTemporary) ? <TrashIcon className="h-6" /> : undefined)}
                                text={(isTemporary) ? "Discard changes" : ((isOwner) ? "Delete" : "Stop participating")}
                                onclick={
                                    () => {
                                        setDeleteConfirmModal(false);

                                        if (formToDelete) {
                                            STORE.removeEditingForm(formToDelete)
                                            .then(() => {
                                                if (!isOwner && !isTemporary) {
                                                    DATA_SERVER.stopParticipation(formToDelete.form.formData)
                                                    .then(() => softReload(!softReloadState));
                                                } else if (!isTemporary) {
                                                    DATA_SERVER.deleteForm(formToDelete)
                                                    .then(() => softReload(!softReloadState));
                                                } else {
                                                    softReload(!softReloadState);
                                                }
                                            });
                                            STORE.removeCurrentEditFormCond(formToDelete)
                                            .then(() => softReload(!softReloadState));
                                        }
                                    }
                                }
                            />,
                            <Button
                                width="[75%]"
                                styling={{paddedMedium: true, roundedPartial: true}}
                                text="Cancel"
                                icon={<XCircleIcon className="h-6" />}
                                onclick={() => setDeleteConfirmModal(false)}
                            />
                        ]
                    }
                    modalTitle={getDeleteModalTitle()}
                    modalText={getDeleteModalText()}
                    modalIcon={<ExclamationIcon className="text-yellow-300" />}
                />
            ):""}
        </>
    );
}

export default Dashboard;