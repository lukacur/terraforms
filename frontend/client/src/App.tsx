import Header from './components/universal/base/Header';
import './App.css';
import Homepage from './pages/Homepage/Homepage';
import Editor from './pages/Editor/Editor';
import AnsweringPage from './pages/AnsweringPage/AnsweringPage';
import Dashboard from './pages/Dashboard/Dashboard';
import { Routes, Route, useNavigate, useLocation } from 'react-router-dom';
import Login from './pages/Login/Login';
import Register from './pages/Login/Register';
import SetupUsername from './pages/Login/SetupUsername';
import Logout from './pages/Login/Logout';
import AUTH from './components/auth/AuthHandler';
import React from 'react';
import ProfilePage from './pages/ProfilePage/ProfilePage';
import StatisticsPage from './pages/StatisticsPage/StatisticsPage';

function App() {
  const navigate = useNavigate();
  const location = useLocation();

  const [isAppHidden, setIsAppHidden] = React.useState<boolean>(true);

  React.useEffect(() => {
    let loc: string = location.pathname;
    if (!(loc === "/" || loc === "" || loc === "/login" || loc === "/register" || loc === "/participate" || loc === "/home" || loc.startsWith("/statistics"))) {
      setIsAppHidden(true);
    } else {
      setIsAppHidden(false);
    }

    AUTH.isLoggedIn()
    .then(loggedIn => {
      let currLoc: string = location.pathname;

      if (!loggedIn && !(currLoc === "/" || currLoc === "" || currLoc === "/login" || currLoc === "/register" || loc === "/participate" || loc === "/home" || loc.startsWith("/statistics"))) {
        navigate("/login");
      }

      setIsAppHidden(false);
    });
  }, [location])

  return (
    <>
      {(!isAppHidden) ? (<>
        <Header />
        <Routes>
          <Route path="/">
            <Route
              index
              element={<Homepage />}
            />
            <Route
              path="home"
              element={<Homepage />}
            />
            <Route
              path="editor"
              element={<Editor />}
            />
            <Route
              path="participate"
              element={<AnsweringPage />}
            />
            <Route
              path="statistics"
              element={<StatisticsPage />}
            />
            <Route
              path="login"
              element={<Login />}
            />
            <Route
              path="register"
              element={<Register />}
            />
            <Route
              path="profile"
              element={<ProfilePage />}
            />
            <Route
              path="setupUsername"
              element={<SetupUsername />}
            />
            <Route
              path="logout"
              element={<Logout />}
            />
            <Route
              path="dashboard"
              element={<Dashboard />}
            />
          </Route>
        </Routes>
      </>):""}
    </>
  );
}

export default App;
