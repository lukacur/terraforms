module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}"
  ],
  theme: {
    extend: {
      transitionProperty: {
        height: 'height'
      },
      colors: {
        'editorArrowHovered': 'rgba(0, 107, 166, 0.75)',
        'questionEditAnswersHovered': 'rgba(145, 196, 242, 0.75)'
      },
      animation: {
        'dropdown-extend': 'dropdown 1s ease-in 0s 1 normal forwards',
        'dropdown-retract': 'dropdownrev 1s ease-out 0s 1 normal forwards'
      },
      keyframes: {
        'dropdown': {
          '0%': { "max-height": '0' },
          '100%': { "max-height": '100vh'}
        },
        'dropdownrev': {
          '0%': { "max-height": '100vh' },
          '100%': { "max-height": '0' }
        }
      },
      screens: {
        'xs': '320px',
        'md': '576px'
      }
    },
  },
  plugins: [],
}
